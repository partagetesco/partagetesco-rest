FROM node:12.5.0

ENV PORT=4096

COPY ./ /partage_tes_co
WORKDIR /partage_tes_co

RUN npm i -P
RUN npm i -g typescript
RUN npm run prepare

EXPOSE ${PORT}

ENTRYPOINT [ "node", "dist/src/index.js" ]