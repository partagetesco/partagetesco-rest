PartageTesCO - Service REST

# Présentation

Ceci est le dépôt du service REST du site PartageTesCO.

# Utilisation

## Préparation

Pour récupérer l'ensemble des dépendances commencer par ouvrir un terminal dans le dossier de l'application et tapez :
```shell
npm i
```

## Prérequis

Une base de données PostgreSQL est requise pour stocker les données de l'application.
Pour générer la structure de la base de données un script SQL est disponible, le fichier
`database_generator_script.sql`.

### Variables d'environnement

Avant de lancer l'application définissez les variables d'environnement suivantes :
 * **HOSTNAME** Le nom d'hôte ou l'IP du serveur.
 * **BASE_URL** L'URL de base de l'API Rest.
 * **PORT** Le port de l'Api Rest.
 * **DATABASE_HOST** Le nom d'hôte ou l'IP du serveur hébergeant la base de donnée PostgreSQL.
 * **DATABASE_PORT** Le port de la base de donnée.
 * **DATABASE_DB** Le nom de la base de donnée dans PostgreSQL.
 * **DATABASE_USER** Le nom de l'utilisateur pour se connecter à la base de donnée.
 * **DATABASE_PASSWORD** Le mot de passe de l'utilisateur.
 * **TOKEN_SECRET** Le secret pour les générer tokens.
 * **MOT_DE_PASSE_SECRET** Le secret pour hasher générer les mots de passe des utilisateurs.
 * **COOKIE_SECRET** Le secret pour générer les cookies.
 * **NOM_COOKIE_TOKEN** Le nom du cookie contenant le token.
 * **ADMINISTRATEUR_FONCTION_ID** L'ID de la fonction administrateur dans la base de donnée.
 * **MODERATEUR_FONCTION_ID** L'ID de la fonction modérateur dans la base de donnée.
 * **PATH_STOCKAGE_FICHIERS** Le chemin vers ou sauvegarder les fichiers reçus.

## Lancement

Pour démarrer le service REST tapez dans le terminal :
```shell
npm run start
```

Le service est désormais accessible à l'adresse suivante http://localhost:3000.

Pour arrêter le service REST utiliser `Ctrl+C`.

## Documentation

La documentation pour le développement est disponible :
 * [Sur Gitlab Pages](https://partagetesco.gitlab.io/partagetesco-rest/documentation)
 * [Sur l'application](http://partagetesco.fr/api/documentation)
 * En générant la doc après avoir récupérer le code source avec la commande suivante, `npm run doc`. La documentation est disponible dans le dossier `apidoc`.

La documentation de l'API Rest est disponible :
 * [Sur Gitlab Pages](https://partagetesco.gitlab.io/partagetesco-rest/api-documentation)
 * [Sur l'application](http://partagetesco.fr/api/api-documentation) (En cours de développement)
 * En générant la doc après avoir récupérer le code source avec la commande suivante, `npm run doc`. La documentation est disponible dans le dossier `docs`.