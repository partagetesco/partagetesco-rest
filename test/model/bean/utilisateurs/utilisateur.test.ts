import { suite, test } from "mocha-typescript";
import chai from "chai";
import { Utilisateur } from "../../../../src/model/bean/utilisateurs/utilisateur";
import { strictEqual } from "assert";
import EmptyStringError from "../../../../src/model/errors/empty.string.error";
import MailInvallideError from "../../../../src/model/errors/mail.invalide.error";

/**
 * Tests de Utilisateur
 * 
 * @author Developpix
 * @version 0.1
 */
@suite
class UtilisateurTest {
    /**
     * Utilisateur pour les tests
     */
    private utilisateur: Utilisateur;

    /**
     * Adresse mail pour les tests
     */
    private readonly MAIL_TEST = "test@gmail.com";

    before() {
        this.utilisateur = new Utilisateur(1, "test", this.MAIL_TEST);
    }

    /*
     * Tests du constructeur
     */

    /**
     * Test constructeur
     */
    @test
    testConstructeurCT1() {
        const utilisateur = new Utilisateur(1, "test", this.MAIL_TEST);

        strictEqual(utilisateur.getId(), 1, "Vérification de l'ID");
        strictEqual(utilisateur.getPseudo(), "test", "Vérification du pseudo");
        strictEqual(utilisateur.getMail(), this.MAIL_TEST, "Vérification de l'adresse mail");
        strictEqual(utilisateur.getBiographie(), "", "Vérification biographie vide");
    }

    /**
     * Test constructeur
     */
    @test
    testConstructeurCT2() {
        const utilisateur = new Utilisateur(1, "test", this.MAIL_TEST);
        const utilisateur2 = new Utilisateur(1, "test", this.MAIL_TEST);

        strictEqual(utilisateur.getId(), 1, "Vérification de l'ID du compte 1");
        strictEqual(utilisateur.getPseudo(), "test", "Vérification du pseudo du compte 1");
        strictEqual(utilisateur.getMail(), this.MAIL_TEST, "Vérification de l'adresse mail du compte 1");
        strictEqual(utilisateur.getBiographie(), "", "Vérification biographie vide du compte 1");

        strictEqual(utilisateur2.getId(), 1, "Vérification de l'ID du compte 2");
        strictEqual(utilisateur2.getPseudo(), "test", "Vérification du pseudo du compte 2");
        strictEqual(utilisateur2.getMail(), this.MAIL_TEST, "Vérification de l'adresse mail du compte 2");
        strictEqual(utilisateur2.getBiographie(), "", "Vérification biographie vide du compte 2");
    }

    /**
     * Test constructeur pseudo vide
     */
    @test
    testConstructeurCT3() {
        chai.expect(() => new Utilisateur(1, "", this.MAIL_TEST)).to.throw(EmptyStringError.MESSAGE_PSEUDO_VIDE);
    }

    /**
     * Test constructeur pseudo null
     */
    @test
    testConstructeurCT4() {
        chai.expect(() => new Utilisateur(1, null, this.MAIL_TEST)).to.throw(EmptyStringError.MESSAGE_PSEUDO_VIDE);
    }

    /**
     * Test constructeur mail vide
     */
    @test
    testConstructeurCT5() {
        chai.expect(() => new Utilisateur(1, "test", "")).to.throw(EmptyStringError.MESSAGE_MAIL_VIDE);
    }

    /**
     * Test constructeur mail null
     */
    @test
    testConstructeurCT6() {
        chai.expect(() => new Utilisateur(1, "test", null)).to.throw(EmptyStringError.MESSAGE_MAIL_VIDE);
    }

    /*
     * Tests de setMail
     */

    /**
     * Test définition d'une adresse mail valide
     */
    @test
    testSetMailCT1() {
        this.utilisateur.setMail("dev@gmail.com");

        strictEqual(this.utilisateur.getMail(), "dev@gmail.com", "Vérification adresse mail changé");
    }

    /**
     * Test définition d'une adresse mail valide avec 2 caractères après le dernier points
     */
    @test
    testSetMailCT2() {
        this.utilisateur.setMail("dev@gmail.fr");

        strictEqual(this.utilisateur.getMail(), "dev@gmail.fr", "Vérification adresse mail changé");
    }

    /**
     * Test définition d'une adresse mail invalide avec 1 caractères après le dernier points
     */
    @test
    testSetMailCT3() {
        chai.expect(() => this.utilisateur.setMail("dev@gmail.a")).to.throw(MailInvallideError.DEFAULT_MESSAGE);
    }

    /**
     * Test définition d'une adresse mail invalide avec aucun point après le @
     */
    @test
    testSetMailCT4() {
        chai.expect(() => this.utilisateur.setMail("dev@gmail")).to.throw(MailInvallideError.DEFAULT_MESSAGE);
    }

    /**
     * Test définition d'une adresse mail valide avec 1 caractère après le dernier points
     */
    @test
    testSetMailCT5() {
        chai.expect(() => this.utilisateur.setMail("dev@gmail.a")).to.throw(MailInvallideError.DEFAULT_MESSAGE);
    }

    /**
     * Test définition d'une adresse mail invalide sans arobase
     */
    @test
    testSetMailCT6() {
        chai.expect(() => this.utilisateur.setMail("devgmail.a")).to.throw(MailInvallideError.DEFAULT_MESSAGE);
    }

    /**
     * Test définition d'une adresse mail invalide avec un point juste après l'arobase 
     */
    @test
    testSetMailCT7() {
        chai.expect(() => this.utilisateur.setMail("dev@.com")).to.throw(MailInvallideError.DEFAULT_MESSAGE);
    }

    /**
     * Test définition d'une adresse mail vide
     */
    @test
    testSetMailCT8() {
        chai.expect(() => this.utilisateur.setMail("")).to.throw(EmptyStringError.MESSAGE_MAIL_VIDE);
    }

    /**
     * Test définition d'une adresse mail null
     */
    @test
    testSetMailCT9() {
        chai.expect(() => this.utilisateur.setMail(null)).to.throw(EmptyStringError.MESSAGE_MAIL_VIDE);
    }

    /*
     * Tests de setPseudo
     */

    /**
     * Test définition d'un pseudo valide
     */
    @test
    testSetPseudoCT1() {
        this.utilisateur.setPseudo("test2");

        strictEqual(this.utilisateur.getPseudo(), "test2", "Vérification pseudo changé");
    }

    /**
     * Test définition d'un pseudo vide
     */
    @test
    testSetPseudoCT2() {
        chai.expect(() => this.utilisateur.setPseudo("")).to.throw(EmptyStringError.MESSAGE_PSEUDO_VIDE);
    }

    /**
     * Test définition d'un pseudo null
     */
    @test
    testSetPseudoCT3() {
        chai.expect(() => this.utilisateur.setPseudo(null)).to.throw(EmptyStringError.MESSAGE_PSEUDO_VIDE);
    }

    /*
     * Tests de setId
     */

    /**
     * Test définition d'un ID
     */
    @test
    testSetIdCT1() {
        strictEqual(this.utilisateur.getId(), 1, "Vérification de l'ID initial");

        // Définition de l'ID 0
        this.utilisateur.setId(0);

        strictEqual(this.utilisateur.getId(), 0, "Vérification ID changé");
    }
}
