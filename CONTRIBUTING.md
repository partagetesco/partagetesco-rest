# Les commits

## Structure des messages
```
<type>(<portée>): <sujet>
<LIGNE VIDE>
<corps>
<LIGNE VIDE>
<pied de page>
```

## Type
Doit-être un (ou plusieurs séprarer par des virgules) des éléments de la liste ci-dessous:
* **build**: Modifications affectant le système de compilation et/ou les dépendances
du projet
* **ci**: Modifications de l'intégration continue
* **docs**: Modifications de la documentation seulement
* **feat**: Nouvelle fonctionnalité
* **fix**: Correction de bug
* **perf**: Modifications permettant de modifier les performances
* **refactor**: Modifications du code ne donnant pas lieu à une nouveauté
ou à une correction de bug
* **style**: Modifications esthétique du code ne modifiant par l'algorithme ou son
implémentation (espace, formattage, point-virgule, etc)
* **test**: Modifications des tests

## Portée
La portée des changements effectué, l'emplacement dans l'application.

## Sujet
Brève description des modifications apporté. <br/>
Conseil :
* Ecrivez vos phrase à l'impératif
* Ne commencez pas par une lettre majuscule
* Ne finissez pas par un "."

Aucune idée de sujet c'est pas grave :
* Commencez par un verbe/nom décrivant votre action sur le projet (Implémentation, Correction, Ajout, Suppression, etc)
* Quoi ? Expliquer en quelques mots qu'est qui a été ajouter, supprimer,
corriger, etc
* Voilà, vous avez fini ;-)

## Corps
Décrivez en détail les modifications si nécessaire.
Vous pouvez commencer par des majuscules et finir par un point,
impératif conseiller mais pas obligatoire.

## Pied de page
Utilisez le pied de page pour les filtrages des commits style, fermer une
discussion, corriger un problème ("fix an issue").

# Les tests

Pour tester l'application nous utilisons les frameworks :
* [Mocha](https://mochajs.org)
* Avec l'outil pour le typescript permettant d'utiliser les annotations [mocha-typescript](https://www.npmjs.com/package/mocha-typescript)
* Pour les Mocks et autres nous utilisons [ts-mockito](https://www.npmjs.com/package/ts-mockito)

## Ecriture de test

Ecrivez vos tests dans le dossier `test` en veillant à créer la même architecture que dans le dossier `src`. Les fichier des tests doivent être rajouter à la fin du fichier de configuration `test/mocha.opts` en utilisant la syntaxe `dist/<test/chemin/vers/le/fichier>.js` (attention à remplacer l'extension `.ts` par l'extension `.js`).

## Execution

Pour exécuter en continue les tests à chaque changement de code utiliser la commande :
```shell
npm run watch
```

Pour lancement les tests une seul fois exécuter plutôt la commande suivante :
```shell
npm test
```

# Développement en général

Lorsque vous développer vous pouvez lancer un watch pour que nodemon relance le serveur à chaque changement grâce à la commande :
```shell
npm run dev
```

Ou bien lancer la compilation, les tests puis démarrer une seul fois le serveur (les changements ne seront pas mise à jour tant que vous n'aurez pas arrêté et relancé le serveur) avec la commande :
```shell
npm start
```