/**
 * Contrôleur contrôlant les accès modérateur de niveau 2
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { RequeteAuthentifiee } from "../model/types/requete.authentifiee";
import Fonction from "../model/bean/utilisateurs/fonction";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../model/errors/reponse.error";

// Création des instances des DAO
const modoRouter: express.Router = express.Router();

// Routes d'administration
modoRouter.all('*', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On recupère la fonction de l'utilisateur
    let fonction: Fonction = req.utilisateur.getFonction();

    // Si l'utilisateur est administrateur ou un modérateur alors on autorise l'accès
    if (fonction && (fonction.getId() == parseInt(process.env.ADMINISTRATEUR_FONCTION_ID)
        || fonction.getId() == parseInt(process.env.MODERATEUR_FONCTION_ID)))
        next();
    else
        // Sinon on refuse l'accès
        res.status(401).send({ erreur: ReponseError.ERREUR_ACCESS_RESTREINS_MODO });
});

export { modoRouter };