/**
 * Contrôleur redirigeant les accès vers les routeurs correspondant
 * 
 * @author Developpix
 * @version 0.1
 */
import { Router } from "express";

import { utilisateursRouter } from "./utilisateurs/utilisateurs";
import { utilisateurRouter } from "./utilisateurs/utilisateur";
import { fonctionsRouter } from "./utilisateurs/fonctions";
import { fonctionRouter } from "./utilisateurs/fonction";
import { typesFichierRouter } from "./fichiers/types.fichiers";
import { typeFichierRouter } from "./fichiers/type.fichiers";
import { formatsFichiersRouter } from "./fichiers/formats.fichiers";
import { formatFichiersRouter } from "./fichiers/format.fichiers";
import { licencesRouter } from "./fichiers/licences";
import { licenceRouter } from "./fichiers/licence";
import { fichiersRouter } from "./fichiers/fichiers";
import { fichierRouter } from "./fichiers/fichier";
import { categoriesRouter } from "./cours/categories";
import { categorieRouter } from "./cours/categorie";
import { coursRecuperationRouter } from "./cours/cours.recuperation";
import { chapitresRouter } from "./cours/chapitres";
import { chapitreRouter } from "./cours/chapitre";
import { sectionsRouter } from "./cours/sections";
import { sectionRouter } from "./cours/section";
import { qcmsRouter } from "./cours/qcm/qcms";
import { qcmRouter } from "./cours/qcm/qcm";
import { questionsRouter } from "./cours/qcm/questions";
import { questionRouter } from "./cours/qcm/question";
import { reponsesRouter } from "./cours/qcm/reponses";
import { reponseRouter } from "./cours/qcm/reponse";
import { exercicesRouter } from "./cours/exercices/exercices";
import { exerciceRouter } from "./cours/exercices/exercice";
import { authRouter } from "./auth.router";
import { coursManipulationRouter } from "./cours/cours.manipulation";

// Création des instances des DAO
const rootRouter: Router = Router();

// On associe les routes des utilisateurs aux routeurs des utilisateurs
rootRouter
    .use("/utilisateurs", utilisateursRouter)
    .use("/utilisateur", utilisateurRouter)
    // On associe les routes des fonctions aux routeurs des fonctions
    .use("/fonctions", fonctionsRouter)
    .use("/fonction", authRouter, fonctionRouter)

    // On associe les routes des types de fichier aux routeurs
    .use("/types-fichiers", typesFichierRouter)
    .use("/type-fichiers", authRouter, typeFichierRouter)
    // On associe les routes des formats de fichier aux routeurs
    .use("/formats-fichiers", formatsFichiersRouter)
    .use("/format-fichiers", authRouter, formatFichiersRouter)
    // On associe les routes des licences aux routeurs
    .use("/licences", licencesRouter)
    .use("/licence", authRouter, licenceRouter)
    // On associe les routes des fichiers aux routeurs
    .use("/fichiers", fichiersRouter)
    .use("/fichier", authRouter, fichierRouter)

    // On associe les routes des catégories aux routeurs
    .use("/categories", categoriesRouter)
    .use("/categorie", authRouter, categorieRouter)
    // On associe les routes des cours aux routeurs
    .use("/cours", coursRecuperationRouter)
    .use("/cours", authRouter, coursManipulationRouter)
    // On associe les routes des chapitres aux routeurs
    .use("/chapitres", chapitresRouter)
    .use("/chapitre", authRouter, chapitreRouter)
    // On associe les routes des sections aux routeurs
    .use("/sections", sectionsRouter)
    .use("/section", authRouter, sectionRouter)

    // On associe les routes des QCM aux routeurs
    .use("/qcms", qcmsRouter)
    .use("/qcm", authRouter, qcmRouter)
    // On associe les routes des questions aux routeurs
    .use("/questions", questionsRouter)
    .use("/question", authRouter, questionRouter)
    // On associe les routes des réponses aux routeurs
    .use("/reponses", reponsesRouter)
    .use("/reponse", authRouter, reponseRouter)

    // On associe les routes des exercices aux routeurs
    .use("/exercices", exercicesRouter)
    .use("/exercice", authRouter, exerciceRouter);

/**
 * @apiDefine ApiErreurs
 * 
 * @apiError (401) Informations-erronees Réponses signalant que les informations d'authentification sont invalides.
 * @apiErrorExample Informations-erronees:
 *    HTTP/1.1 401 Unauthorized
 *     {
 *        "erreur": {
 *          "code": "401000",
 *          "type": "Échec de l'authentification",
 *          "message": "Les informations saisies sont erronées."
 *        }
 *     }
 * 
 * @apiError (401) Token-absent Réponses signalant que le token n'a pas été spécifié.
 * @apiErrorExample Token-absent:
 *    HTTP/1.1 401 Unauthorized
 *     {
 *        "erreur": {
 *          "code": "401001",
 *          "type": "Token absent",
 *          "message": "Impossible de vous identifier. Veuillez définir le paramètre 'token'."
 *        }
 *     }
 * 
 * @apiError (401) Token-invalide Réponses signalant que le token est invalide.
 * @apiErrorExample Token-invalide:
 *    HTTP/1.1 401 Unauthorized
 *     {
 *        "erreur": {
 *          "code": "401002",
 *          "type": "Token invalide",
 *          "message": "Le token envoyé est invalide."
 *        }
 *     }
 * 
 * @apiError (401) Compte-supprime Réponses signalant que le compte n'existe pas ou plus.
 * @apiErrorExample Compte-supprime:
 *    HTTP/1.1 401 Unauthorized
 *     {
 *        "erreur": {
 *          "code": "401003",
 *          "type": "Compte supprimé",
 *          "message": "Le compte a été supprimé. Veuillez contacter l'administrateur."
 *        }
 *     }
 */

export { rootRouter };