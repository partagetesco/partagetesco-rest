/**
 * Routeurs contrôlant l'authentification
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { RequeteAuthentifiee } from "../model/types/requete.authentifiee";
import { Response, Request } from "express-serve-static-core";
import { ReponseError } from "../model/errors/reponse.error";
import { UtilisateursDao } from "../model/dao/utilisateurs/utilisateurs.dao";
import { Utilisateur } from "../model/bean/utilisateurs/utilisateur";
import jwt from "jsonwebtoken";

// On recupère le DAO des tokens
const utilisateursDao: UtilisateursDao = new UtilisateursDao();

// Création des instances des DAO
const authRouter: express.Router = express.Router();

// Routes d'administration
authRouter.all('*', (req: Request, res: Response, next: () => void) => {
    // On récupère le token depuis le body ou les cookies
    let token: string;
    if (req.body.token)
        token = req.body.token;
    else if (req.query.token)
        token = req.query.token;
    else
        token = req.cookies[process.env.NOM_COOKIE_TOKEN]

    try {
        // On décode le token en le vérifiant
        let tokenDecode: any = jwt.verify(token, process.env.TOKEN_SECRET);

        // On récupère l'utilisateur associé
        utilisateursDao.get(tokenDecode.id)
            .then(
                (utilisateur: Utilisateur) => {
                    (<RequeteAuthentifiee>req).utilisateur = utilisateur;

                    // On appel le handler suivant
                    next();
                },
                (err: any) => {
                    console.log(err);

                    // On retourne un message d'erreur pour dire que le compte a été supprimé
                    res.status(401).send({ erreur: ReponseError.ERREUR_COMPTE_SUPPRIME });
                });
    } catch (err) {
        // Si le token est invalide on retourne une message d'erreur
        res.send(ReponseError.ERREUR_TOKEN_INVALIDE);
    }
});

export { authRouter };