/**
 * Routes /utilisateurs/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Request, Response } from "express-serve-static-core";
import jwt from "jsonwebtoken";
import { Utilisateur } from "../../model/bean/utilisateurs/utilisateur";
import { UtilisateursDao } from "../../model/dao/utilisateurs/utilisateurs.dao";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { ParametresRechercheUtilisateurs } from "../../model/types/parametres.recherche";
import EchecAuthentificationError from "../../model/errors/echec.authentification.error";
import { adminRouter } from "../adminRouter";
import Fonction from "../../model/bean/utilisateurs/fonction";
import { FonctionsDao } from "../../model/dao/utilisateurs/fonctions.dao";
import { authRouter } from "../auth.router";

// Création des instances des DAO
const utilisateursDao: UtilisateursDao = new UtilisateursDao();
const fonctionsDao: FonctionsDao = new FonctionsDao();

// Création du routeur
const utilisateursRouter = express.Router();

/**
 * @api {get} /utilisateurs Rechercher des utilisateurs
 * @apiVersion 0.1.0
 * @apiName GetUtilisateurs
 * @apiGroup Utilisateurs
 * 
 * @apiParam {Number} id l'ID de l'utilisateur à récupérer.
 * @apiParam {String} pseudo Un mot clé recherché dans le pseudo des utilisateurs.
 * @apiParam {String} mail Un mot clé recherché dans l'adresse mail des utilisateurs.
 * @apiParam {String} biographie Un mot clé recherché dans la biographie des utilisateurs.
 * @apiParam {Number} fonction l'ID de la fonction des utilisateurs recherchés.
 * 
 * @apiSuccess {Number} id l'ID de l'utilisateur.
 * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
 * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
 * @apiSuccess {String} biographie La biographie de l'utilisateur.
 * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *     [
 *        {
 *          "id": 1,
 *          "pseudo": "developpix",
 *          "mail": "mail@exemple.com",
 *          "biographie": "",
 *          "fonction": {
 *            "id": 1,
 *            "nom": "Utilisateur",
 *            "description": "Un utilisateur."
 *          }
 *        }
 *     ]
 *
 * @apiUse ApiErreurs
 */
utilisateursRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheUtilisateurs = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.pseudo)
    parametres.pseudo = req.query.pseudo;
  if (req.query.mail)
    parametres.mail = req.query.mail;
  if (req.query.biographie)
    parametres.biographie = req.query.biographie;
  if (req.query.fonction)
    parametres.id_fonction = parseInt(req.query.fonction);

  // On fait la recherche dans la base de donnée
  utilisateursDao.getAll(parametres).then(
    (utilisateurs: Utilisateur[]) => {
      res.send(utilisateurs);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /utilisateurs/:id Récupérer un utilisateur
   * @apiVersion 0.1.0
   * @apiName GetUtilisateur
   * @apiGroup Utilisateurs
   * 
   * @apiParam {Number} id l'ID de l'utilisateur à récupérer.
   * 
   * @apiSuccess {Number} id l'ID de l'utilisateur.
   * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
   * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
   * @apiSuccess {String} biographie La biographie de l'utilisateur.
   * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
   *
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *        {
   *          "id": 1,
   *          "pseudo": "developpix",
   *          "mail": "mail@exemple.com",
   *          "biographie": "",
   *          "fonction": {
   *            "id": 1,
   *            "nom": "Utilisateur",
   *            "description": parametres"Un utilisateur."
   *          }
   *        }
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    utilisateursDao.get(parseInt(req.params.id)).then(
      (utilisateur: Utilisateur) => {
        res.send(utilisateur);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  })

  /**
   * @api {put} /utilisateurs/promouvoir Changer la fonction de plusieurs utilisateurs
   * @apiVersion 0.1.0
   * @apiName Promouvoir
   * @apiGroup Utilisateurs
   * 
   * @apiParam {json} body Les données sur la fonction et les utilisateurs.
   * @apiParam {Number} fonction l'ID de la fonction des utilisateurs recherchés.
   * @apiParam {Number[]} utilisateurs Les ID des utilisateurs.
   * 
   * @apiParamExample {json} Exemple:
   *    HTTP/1.1 200 OK
   *        {
   *          "fonction": 1,
   *          "utilisateurs": [ 4, 2 ]
   *        }
   * 
   * @apiSuccess {Number} id l'ID de l'utilisateur.
   * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
   * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
   * @apiSuccess {String} biographie La biographie de l'utilisateur.
   * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
   *
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *     [
   *        {
   *          "id": 1,
   *          "pseudo": "developpix",
   *          "mail": "mail@exemple.com",
   *          "biographie": "",
   *          "fonction": {
   *            "id": 1,
   *            "nom": "Utilisateur",
   *            "description": "Un utilisateur."
   *          }
   *        }
   *     ]
   *
   * @apiUse ApiErreurs
   */
  .put('/promouvoir', authRouter, adminRouter, async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // Création de la liste des utilisateurs promus
    let utilisateursPromus: Utilisateur[] = [];

    try {
      // Recupération de la fonction associé
      const fonction: Fonction = await fonctionsDao.get(parseInt(req.body.fonction));

      // On promus la liste des utilisateurs
      let utilisateurModifie: Utilisateur;
      for (let i = 0; i < req.body.utilisateurs.length; i++) {
        utilisateurModifie = await utilisateursDao.get(parseInt(req.body.utilisateurs[i]));
        utilisateursDao.updateFonction(utilisateurModifie, fonction);
      }

      // On retourne l'utilisateur modifié
      res.send(utilisateursPromus);
    } catch (err) {
      // TODO
      console.log(err);

      // On renvoi le message d'erreur inattendue
      res.send({ erreur: ReponseError.ERREUR_INATTENDUE, promus: utilisateursPromus });
    }
  });

/**
 * Fonction permettant de générer le token de l'utilisateur
 * 
 * @param utilisateur l'utilisateur
 * @returns le token de l'utilisateur 
 */
function genererToken(utilisateur: Utilisateur): string {
  return jwt.sign({ id: utilisateur.getId() }, process.env.TOKEN_SECRET,
    {
      expiresIn: '7d'
    });
}

export { utilisateursRouter };