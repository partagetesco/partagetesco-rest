/**
 * Routes /fonctions/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { ParametresRechercheFonctions } from "../../model/types/parametres.recherche";
import { FonctionsDao } from "../../model/dao/utilisateurs/fonctions.dao";
import Fonction from "../../model/bean/utilisateurs/fonction";

// Création des instances des DAO
const fonctionsDao: FonctionsDao = new FonctionsDao();

// Création du routeur
const fonctionsRouter = express.Router();

/**
 * @api {get} /fonctions Rechercher des fonctions
 * @apiVersion 0.1.0
 * @apiName GetFonctions
 * @apiGroup Fonctions
 * 
 * @apiParam {Number} id L'ID de la fonction.
 * @apiParam {String} nom Un mot clé recherché dans le nom de la fonction
 * @apiParam {String} description Un mot clé recherché dans la description de la fonction
 * 
 * @apiSuccess {Number} id l'ID de la fonction.
 * @apiSuccess {String} nom Le nom de la fonction
 * @apiSuccess {String} description La description de la fonction
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 0,
 *        "nom": "Utilisateur",
 *        "description": "Un utilisateur de l'application"
 *      }
 *    ]
 *
 * @apiUse ApiErreurs
 */
fonctionsRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheFonctions = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;

  // On fait la recherche dans la base de donnée
  fonctionsDao.getAll(parametres).then(
    (fonctions: Fonction[]) => {
      res.send(fonctions);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /fonctions/:id Recupérer une fonction
   * @apiVersion 0.1.0
   * @apiName GetFonction
   * @apiGroup Fonctions
   * 
   * @apiParam {Number} id L'ID de la fonction.
   * 
   * @apiSuccess {Number} id l'ID de la fonction.
   * @apiSuccess {String} nom Le nom de la fonction
   * @apiSuccess {String} description La description de la fonction
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 0,
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    fonctionsDao.get(parseInt(req.params.id)).then(
      (fonction: Fonction) => {
        res.send(fonction);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { fonctionsRouter };