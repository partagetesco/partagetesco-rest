/**
 * Routes /utilisateur/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Request, Response } from "express-serve-static-core";
import jwt from "jsonwebtoken";
import { Utilisateur } from "../../model/bean/utilisateurs/utilisateur";
import { UtilisateursDao } from "../../model/dao/utilisateurs/utilisateurs.dao";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import EchecAuthentificationError from "../../model/errors/echec.authentification.error";
import { authRouter } from "../auth.router";

// Création des instances des DAO
const utilisateursDao: UtilisateursDao = new UtilisateursDao();

// Création du routeur
const utilisateurRouter = express.Router();

/**
 * @api {post} /utilisateur/inscription Inscrire un nouvel utilisateur
 * @apiVersion 0.1.0
 * @apiName Inscription
 * @apiGroup Utilisateurs
 * 
 * @apiParam {json} body Les données de l'utilisateur au format JSON.
 * @apiParam {String} pseudo Le pseudo de l'utilisateur.
 * @apiParam {String} motDePasse Le mot de passe de l'utilisateur.
 * @apiParam {String} mail L'adresse mail de l'utilisateur.
 * @apiParam {String} biographie La biographie de l'utilisateur (optionnel).
 * 
 * @apiParamExample {json} Exemple:
 *      {
 *        "pseudo": "developpix",
 *        "motDePasse": "mtp",
 *        "mail": "mail@exemple.com",
 *        "biographie": ""
 *      }
 * 
 * @apiSuccess {String} message Un message d'informations.
 * 
 * @apiSuccess {Utilisateur} compte Le compte de l'utilisateur.
 * @apiSuccess {Number} id L'ID de l'utilisateur.
 * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
 * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
 * @apiSuccess {String} biographie La biographie de l'utilisateur.
 * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
 * 
 * @apiSuccess {String} token Le token de l'utilisateur.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "message": "Inscription réussie",
 *      "compte": {
 *        "id": 1,
 *        "pseudo": "developpix",
 *        "mail": "mail@exemple.com",
 *        "biographie": "",
 *        "fonction": {
 *          "id": 1,
 *          "nom": "Utilisateur",
 *          "description": "Un utilisateur."
 *        }
 *      },
 *      "token": "xxx.xxx.xxx"
 *    }
 *
 * @apiUse ApiErreurs
 */
utilisateurRouter.post('/inscription', (req: Request, res: Response, next: NextFunction) => {
  // On créer l'utilisateur
  let utilisateur: Utilisateur = new Utilisateur(-1, req.body.pseudo, req.body.mail, req.body.biographie);

  // On enregistre l'utilisateur
  utilisateursDao.insert(utilisateur, req.body.motDePasse)
    .then((utilisateurCree: Utilisateur) => {
      // On génère le token
      let token: string = genererToken(utilisateur);

      // On défini un cookie avec une durée de vie de 7j et signé
      res.cookie(process.env.NOM_COOKIE_TOKEN, token,
        {
          signed: true,
          secure: true,
          httpOnly: true,
          maxAge: 604800,
          domain: process.env.HOSTNAME
        });

      // On lui retourne un message pour l'informer du succès et lui donner son token
      res.send({
        message: 'Inscription réussie',
        compte: utilisateurCree,
        token: token
      });
    }, (err: any) => {
      // TODO
      console.log(err);

      // Si le pseudo est déjà présent dans la base de donnée
      if (err.constraint && err.constraint === 'utilisateurs_pseudo_key')
        res.send({ erreur: ReponseError.ERREUR_PSEUDO_PRIS });
      // Sinon si l'adresse mail est déjà présent dans la base de donnée
      else if (err.constraint && err.constraint === 'utilisateurs_mail_key')
        res.send({ erreur: ReponseError.ERREUR_MAIL_PRIS });
      else
        // Sinon on renvoi le message d'erreur inattendue
        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {post} /utilisateur/connexion Récupérer le token de l'utilisateur
   * @apiVersion 0.1.0
   * @apiName Connexion
   * @apiGroup Utilisateurs
   * @apiPermission utilisateurs
   * 
   * @apiParam {String} pseudo Le pseudo de l'utilisateur.
   * @apiParam {String} motDePasse Le mot de passe de l'utilisateur.
   * 
   * @apiSuccess {String} message Un message d'informations.
   * 
   * @apiSuccess {Utilisateur} compte Le compte de l'utilisateur.
   * @apiSuccess {Number} id L'ID de l'utilisateur.
   * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
   * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
   * @apiSuccess {String} biographie La biographie de l'utilisateur.
   * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
   * 
   * @apiSuccess {String} token Le token de l'utilisateur.
   *
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *    {
   *      "message": "Inscription réussie",
   *      "compte": {
   *        "id": 1,
   *        "pseudo": "developpix",
   *        "mail": "mail@exemple.com",
   *        "biographie": "",
   *        "fonction": {
   *          "id": 1,
   *          "nom": "Utilisateur",
   *          "description": "Un utilisateur."
   *        }
   *      },
   *      "token": "xxx.xxx.xxx"
   *    }
   *
   * @apiUse ApiErreurs
   */
  .post('/connexion', (req: Request, res: Response, next: NextFunction) => {
    utilisateursDao.authentifier(req.body.pseudo, req.body.motDePasse)
      .then((utilisateurConnecte: Utilisateur) => {
        // On génère le token
        let token: string = genererToken(utilisateurConnecte);

        // On défini un cookie avec une durée de vie de 7j et signé
        res.cookie(process.env.NOM_COOKIE_TOKEN, token,
          {
            signed: true,
            secure: true,
            httpOnly: true,
            maxAge: 604800,
            domain: process.env.HOSTNAME
          });

        // On lui retourne un message de succès, son compte et son token
        res.send({
          message: 'Connexion réussie',
          compte: utilisateurConnecte,
          token: token
        });
      }, (err: any) => {
        // TODO
        console.log(err);

        // Si on a une échec d'authentification on renvoie un message à l'utilisateur
        if (err instanceof EchecAuthentificationError)
          res.status(401).send({ erreur: ReponseError.ERREUR_ECHEC_AUTHENTIFICATION });
        else
          // Sinon on renvoie un message d'erreur innatendue
          res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  })

  /**
   * @api {put} /utilisateur Mettre à jour l'utilisateur
   * @apiVersion 0.1.0
   * @apiName PutUtilisateur
   * @apiGroup Utilisateurs
   * @apiPermission proprietaire
   * 
   * @apiParam {String} pseudo Le pseudo de l'utilisateur (optionnel).
   * @apiParam {String} motDePasse Le mot de passe de l'utilisateur (optionnel).
   * @apiParam {String} mail L'adresse mail de l'utilisateur (optionnel).
   * @apiParam {String} biographie La biographie de l'utilisateur (optionnel).
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "pseudo": "developpix",
   *        "motDePasse": "mtp",
   *        "mail": "mail@exemple.com",
   *        "biographie": ""
   *      }
   * 
   * @apiSuccess {Number} id L'ID de l'utilisateur.
   * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
   * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
   * @apiSuccess {String} biographie La biographie de l'utilisateur.
   * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "pseudo": "developpix",
   *        "mail": "mail@exemple.com",
   *        "biographie": "",
   *        "fonction": {
   *          "id": 1,
   *          "nom": "Utilisateur",
   *          "description": "Un utilisateur."
   *        }
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', authRouter, async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // Création de l'utilisateur modifiée
    let utilisateurModifie: Utilisateur = req.utilisateur;

    try {
      // Si un pseudo est contenu dans le corps de la requête on met à jour le pseudo
      if (req.body.pseudo)
        utilisateurModifie = await utilisateursDao.updatePseudo(req.utilisateur, req.body.pseudo);

      // Si une adresse mail est contenu dans le corps de la requête on la met à jour
      if (req.body.mail)
        utilisateurModifie = await utilisateursDao.updateMail(req.utilisateur, req.body.mail);

      // Si un mot de passe est contenu dans le corps de la requête on le met à jour
      if (req.body.motDePasse)
        utilisateurModifie = await utilisateursDao.updateMotDePasse(req.utilisateur, req.body.motDePasse);

      // Si une biographie est contenu dans le corps de la requête on la met à jour
      if (req.body.biographie)
        utilisateurModifie = await utilisateursDao.updateBiographie(req.utilisateur, req.body.biographie);

      // On retourne l'utilisateur modifié
      res.send(utilisateurModifie);
    } catch (err) {
      // TODO
      console.log(err);

      // Si le pseudo est déjà présent dans la base de donnée
      if (err.constraint && err.constraint === 'utilisateurs_pseudo_key')
        res.send({ erreur: ReponseError.ERREUR_PSEUDO_PRIS, utilisateur: utilisateurModifie });
      // Sinon si l'adresse mail est déjà présent dans la base de donnée
      else if (err.constraint && err.constraint === 'utilisateurs_mail_key')
        res.send({ erreur: ReponseError.ERREUR_MAIL_PRIS, utilisateur: utilisateurModifie });
      else
        // Sinon on renvoi le message d'erreur inattendue
        res.send({ erreur: ReponseError.ERREUR_INATTENDUE, utilisateur: utilisateurModifie });
    }
  })


  /**
   * @api {delete} /utilisateur/desinscription Désinscrire l'utilisateur authentifié avec le token
   * @apiVersion 0.1.0
   * @apiName Desinscription
   * @apiGroup Utilisateurs
   * @apiPermission proprietaire
   * 
   * @apiSuccess {Number} id L'ID de l'utilisateur.
   * @apiSuccess {String} pseudo Le pseudo de l'utilisateur.
   * @apiSuccess {String} mail L'adresse mail de l'utilisateur.
   * @apiSuccess {String} biographie La biographie de l'utilisateur.
   * @apiSuccess {Fonction} fonction La fonction de l'utilisateur.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *    {
   *      "message": "Désinscription réussie"
   *    }
   *
   * @apiUse ApiErreurs
   */
  .delete('/desinscription', authRouter, (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le token
    utilisateursDao.delete(req.utilisateur).then(
      () => {
        res.send({ message: 'Désinscription réussie' });
      }, (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

/**
 * Fonction permettant de générer le token de l'utilisateur
 * 
 * @param utilisateur l'utilisateur
 * @returns le token de l'utilisateur 
 */
function genererToken(utilisateur: Utilisateur): string {
  return jwt.sign({ id: utilisateur.getId() }, process.env.TOKEN_SECRET,
    {
      expiresIn: '7d'
    });
}

export { utilisateurRouter };