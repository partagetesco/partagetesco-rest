/**
 * Routes /fonction/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { FonctionsDao } from "../../model/dao/utilisateurs/fonctions.dao";
import Fonction from "../../model/bean/utilisateurs/fonction";
import { adminRouter } from "../adminRouter";

// Création des instances des DAO
const fonctionsDao: FonctionsDao = new FonctionsDao();

// Création du routeur
const fonctionRouter = express.Router();

  /**
   * @api {post} /fonction Insérer une nouvelle fonction
   * @apiVersion 0.1.0
   * @apiName PostFonction
   * @apiGroup Fonctions
   * @apiPermission admin
   * 
   * @apiParam {json} body Les données de la fonction au format JSON
   * @apiParam {String} nom Le nom de la fonction
   * @apiParam {String} description La description de la fonction
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la fonction.
   * @apiSuccess {String} nom Le nom de la fonction
   * @apiSuccess {String} description La description de la fonction
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 0,
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   *
   * @apiUse ApiErreurs
   */
fonctionRouter.post('/', adminRouter, (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la fonction
    let fonction: Fonction = new Fonction(-1, req.body.nom, req.body.description);

    // On enregistre la fonction dans la base de donnée et on la retourne à l'utilisateur
    fonctionsDao.insert(fonction).then(
      (fonctionCree: Fonction) => {
        res.send(fonctionCree);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {put} /fonction Mettre à jour une fonction
   * @apiVersion 0.1.0
   * @apiName PutFonction
   * @apiGroup Fonctions
   * @apiPermission admin
   * 
   * @apiParam {json} body Les données de la fonction au format JSON
   * @apiParam {Number} id L'ID de la fonction
   * @apiParam {String} nom Le nom de la fonction
   * @apiParam {String} description La description de la fonction
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "id": 0,
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la fonction.
   * @apiSuccess {String} nom Le nom de la fonction
   * @apiSuccess {String} description La description de la fonction
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 0,
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', adminRouter, (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la fonction
    let fonction: Fonction = new Fonction(parseInt(req.body.id), req.body.nom, req.body.description);

    // On met à jour la fonction dans la base de donnée et on la retourne à l'utilisateur
    fonctionsDao.update(fonction).then(
      (fonctionModifiee: Fonction) => {
        res.send(fonctionModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {delete} /fonction Supprimer une fonction
   * @apiVersion 0.1.0
   * @apiName DeleteFonction
   * @apiGroup Fonctions
   * @apiPermission admin
   * 
   * @apiParam {Number} id L'ID de la fonction
   * 
   * @apiSuccess {Number} id l'ID de la fonction.
   * @apiSuccess {String} nom Le nom de la fonction
   * @apiSuccess {String} description La description de la fonction
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 0,
   *        "nom": "Utilisateur",
   *        "description": "Un utilisateur de l'application"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', adminRouter, (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime la fonction dans la base de donnée et on la retourne à l'utilisateur
    fonctionsDao.delete(parseInt(req.query.id)).then(
      (fonctionSupprimee: Fonction) => {
        res.send(fonctionSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { fonctionRouter };