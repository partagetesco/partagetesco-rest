/**
 * Routes /categories/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { ParametresRechercheCategories } from "../../model/types/parametres.recherche";
import { CategoriesDao } from "../../model/dao/cours/categories.dao";
import { Categorie } from "../../model/bean/cours/categorie";

// Création des instances des DAO
const categoriesDao: CategoriesDao = new CategoriesDao();

// Création du routeur
const categoriesRouter = express.Router();

/**
 * @api {get} /categories Rechercher des catégories
 * @apiVersion 0.1.0
 * @apiName GetCategories
 * @apiGroup Categories
 * 
 * @apiParam {Number} id L'ID de la catégorie
 * @apiParam {String} slug Le slug de la catégorie
 * @apiParam {String} nom Le nom de la catégorie
 * @apiParam {String} description La description de la catégorie
 * @apiParam {Number} parent La catégorie parente de la catégorie (optionnel)
 * 
 * @apiSuccess {Number} id l'ID de la catégorie.
 * @apiSuccess {String} slug Le slug de la catégorie
 * @apiSuccess {String} nom Le nom de la catégorie
 * @apiSuccess {String} description La description de la catégorie
 * @apiSuccess {Categorie} parent La catégorie parente de la catégorie (optionnel)
 * 
 * @apiSuccessExample Success-Response-1:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "slug": "informatique",
 *        "nom": "Informatique",
 *        "description": "Catégorie parente pour le domaine de l'informatique",
 *      }
 *    ]
 * 
 * @apiSuccessExample Success-Response-1:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 2,
 *        "slug": "reseau",
 *        "nom": "Réseau",
 *        "description": "Catégorie le réseau",
 *        "parent": {
 *          "id": 1,
 *          "slug": "informatique",
 *          "nom": "Informatique",
 *          "description": "Catégorie parente pour le domaine de l'informatique",
 *          "parent": null
 *        }
 *      }
 *    ]
 *
 * @apiUse ApiErreurs
 */
categoriesRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheCategories = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.slug)
    parametres.slug = req.query.slug;
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.parent)
    parametres.id_parent = parseInt(req.query.parent);

  // On fait la recherche dans la base de donnée
  categoriesDao.getAll(parametres).then(
    (categories: Categorie[]) => {
      res.send(categories);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /categories/:id Recupérer une catégorie
   * @apiVersion 0.1.0
   * @apiName GetCategorie
   * @apiGroup Categories
   * 
   * @apiParam {Number} id L'ID de la catégorie
   * 
   * @apiSuccess {Number} id l'ID de la catégorie.
   * @apiSuccess {String} slug Le slug de la catégorie
   * @apiSuccess {String} nom Le nom de la catégorie
   * @apiSuccess {String} description La description de la catégorie
   * @apiSuccess {Categorie} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "slug": "informatique",
   *        "nom": "Informatique",
   *        "description": "Catégorie parente pour le domaine de l'informatique",
   *      }
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau",
   *        "nom": "Réseau",
   *        "description": "Catégorie le réseau",
   *        "parent": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    categoriesDao.get(parseInt(req.params.id)).then(
      (categorie: Categorie) => {
        res.send(categorie);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { categoriesRouter };