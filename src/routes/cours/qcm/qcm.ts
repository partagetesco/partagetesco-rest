/**
 * Routes /qcm/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../../model/types/requete.authentifiee";
import { QCMsDao } from "../../../model/dao/cours/qcm/qcms.dao";
import { ChapitresDao } from "../../../model/dao/cours/chapitres.dao";
import { QCM } from "../../../model/bean/cours/qcm/qcm";

// Création des instances des DAO
const qcmsDao: QCMsDao = new QCMsDao();
const chapitresDao: ChapitresDao = new ChapitresDao();

// Création du routeur
const qcmRouter = express.Router();

/**
 * @api {post} /qcm Insérer un nouveau QCM
 * @apiVersion 0.1.0
 * @apiName PostQCM
 * @apiGroup QCM
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données du QCM au format JSON.
 * @apiParam {String} slug Le slug du QCM.
 * @apiParam {String} nom Le nom du QCM.
 * @apiParam {Number} chapitre L'ID du chapitre du QCM.
 * 
 * @apiParamExample {json} Exemple:
 *  {
 *    "slug": "test",
 *    "nom": "Test",*
 *    "chapitre": 2
 *  }
 * 
 * @apiSuccess {Number} id l'ID du QCM.
 * @apiSuccess {String} slug Le slug du QCM.
 * @apiSuccess {String} nom Le nom du QCM.
 * @apiSuccess {Chapitre} chapitre Le chapitre du QCM.
 *
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *        {
 *          "id": 1,
 *          "slug": "test",
 *          "nom": "Test",
 *          "chapitre": {
 *            "id": 2,
 *            "numero": 2,
 *            "nom": "Chap. 2",
 *            "description": "Chapitre 2 du cours 2",
 *            "cours": {
 *              "id": 2,
 *              "slug": "reseau2",
 *              "nom": "Réseaux2",
 *              "description": "Cours contenant tous ce qui est en lien avec les communications",
 *              "categorie": {
 *                "id": 1,
 *                "slug": "informatique",
 *                "nom": "Informatique",
 *                "description": "Catégorie parente pour le domaine de l'informatique",
 *                "parent": null
 *              }
 *            }
 *          }
 *        }
 *
 * @apiUse ApiErreurs
 */
qcmRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer le qcm
  let qcm: QCM = new QCM(-1, req.body.slug, req.body.nom,
    await chapitresDao.get(parseInt(req.body.chapitre)));

  // On enregistre le qcm dans la base de donnée et on la retourne à l'utilisateur
  qcmsDao.insert(qcm).then(
    (qcmCree: QCM) => {
      res.send(qcmCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /qcm Mettre à jour un QCM
   * @apiVersion 0.1.0
   * @apiName PutQCM
   * @apiGroup QCM
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données du QCM au format JSON.
   * @apiParam {Number} id l'ID du QCM.
   * @apiParam {String} slug Le slug du QCM.
   * @apiParam {String} nom Le nom du QCM.
   * @apiParam {Number} chapitre L'ID du chapitre du QCM.
   * 
   * @apiParamExample {json} Exemple:
   *  {
   *    "id": 1,
   *    "slug": "test",
   *    "nom": "Test",*
   *    "chapitre": 2
   *  }
   * 
   * @apiSuccess {Number} id l'ID du QCM.
   * @apiSuccess {String} slug Le slug du QCM.
   * @apiSuccess {String} nom Le nom du QCM.
   * @apiSuccess {Chapitre} chapitre Le chapitre du QCM.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *        {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        }
   *
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer le qcm
    let qcm: QCM = new QCM(-1, req.body.slug, req.body.nom,
      await chapitresDao.get(parseInt(req.body.chapitre)));

    // On met à jour le qcm dans la base de donnée et on la retourne à l'utilisateur
    qcmsDao.update(qcm).then(
      (qcmModifie: QCM) => {
        res.send(qcmModifie);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })


  /**
   * @api {delete} /qcm Supprimer un QCM
   * @apiVersion 0.1.0
   * @apiName DeleteQCM
   * @apiGroup QCM
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id L'ID du QCM.
   * 
   * @apiSuccess {Number} id l'ID du QCM.
   * @apiSuccess {String} slug Le slug du QCM.
   * @apiSuccess {String} nom Le nom du QCM.
   * @apiSuccess {Chapitre} chapitre Le chapitre du QCM.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *        {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le qcm dans la base de donnée et on la retourne à l'utilisateur
    qcmsDao.delete(parseInt(req.query.id)).then(
      (qcmSupprime: QCM) => {
        res.send(qcmSupprime);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { qcmRouter };