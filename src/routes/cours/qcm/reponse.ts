/**
 * Routes /reponse/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { QuestionsDao } from "../../../model/dao/cours/qcm/questions.dao";
import { ReponsesDao } from "../../../model/dao/cours/qcm/reponses.dao";
import { RequeteAuthentifiee } from "../../../model/types/requete.authentifiee";
import { Reponse } from "../../../model/bean/cours/qcm/reponse";
import { ReponseError } from "../../../model/errors/reponse.error";

// Création des instances des DAO
const reponsesDao: ReponsesDao = new ReponsesDao();
const questionsDao: QuestionsDao = new QuestionsDao();

// Création du routeur
const reponseRouter = express.Router();

/**
 * @api {post} /reponse Insérer une nouvelle réponse
 * @apiVersion 0.1.0
 * @apiName PostReponse
 * @apiGroup Reponses
 * @apiPermission utilisateurs
 * 
 * @apiParam {String} reponse Le contenu de la réponse.
 * @apiParam {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
 * @apiParam {Number} question L'ID de la question associé.
 * 
 * @apiSuccess {Number} id l'ID de la réponse.
 * @apiSuccess {String} reponse Le contenu de la réponse.
 * @apiSuccess {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
 * @apiSuccess {Question} question La question associé.
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 1,
 *        "valide": false,
 *        "question": {
 *          "id": 1,
 *          "qcm": {
 *            "id": 1,
 *            "slug": "test",
 *            "nom": "Test",
 *            "chapitre": {
 *              "id": 2,
 *              "numero": 2,
 *              "nom": "Chap. 2",
 *              "description": "Chapitre 2 du cours 2",
 *              "cours": {
 *                "id": 2,
 *                "slug": "reseau2",
 *                "nom": "Réseaux2",
 *                "description": "Cours contenant tous ce qui est en lien avec les communications",
 *                "categorie": {
 *                  "id": 1,
 *                  "slug": "informatique",
 *                  "nom": "Informatique",
 *                  "description": "Catégorie parente pour le domaine de l'informatique",
 *                  "parent": null
 *                }
 *              }
 *            }
 *          },
 *          "question": "test ?"
 *        },
 *        "reponse": "42"
 *      }
 *
 * @apiUse ApiErreurs
 */
reponseRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer la réponse
  let reponse: Reponse = new Reponse(-1, req.body.reponse, req.body.valide,
    await questionsDao.get(parseInt(req.body.question)));

  // On enregistre la réponse dans la base de donnée et on la retourne à l'utilisateur
  reponsesDao.insert(reponse).then(
    (reponseCree: Reponse) => {
      res.send(reponseCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /reponse Mettre à jour une réponse
   * @apiVersion 0.1.0
   * @apiName PutReponse
   * @apiGroup Reponses
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID de la réponse.
   * @apiParam {String} reponse Le contenu de la réponse.
   * @apiParam {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
   * @apiParam {Number} question L'ID de la question associé.
   * 
   * @apiSuccess {Number} id l'ID de la réponse.
   * @apiSuccess {String} reponse Le contenu de la réponse.
   * @apiSuccess {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
   * @apiSuccess {Question} question La question associé.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "valide": false,
   *        "question": {
   *          "id": 1,
   *          "qcm": {
   *            "id": 1,
   *            "slug": "test",
   *            "nom": "Test",
   *            "chapitre": {
   *              "id": 2,
   *              "numero": 2,
   *              "nom": "Chap. 2",
   *              "description": "Chapitre 2 du cours 2",
   *              "cours": {
   *                "id": 2,
   *                "slug": "reseau2",
   *                "nom": "Réseaux2",
   *                "description": "Cours contenant tous ce qui est en lien avec les communications",
   *                "categorie": {
   *                  "id": 1,
   *                  "slug": "informatique",
   *                  "nom": "Informatique",
   *                  "description": "Catégorie parente pour le domaine de l'informatique",
   *                  "parent": null
   *                }
   *              }
   *            }
   *          },
   *          "question": "test ?"
   *        },
   *        "reponse": "42"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la réponse
    let reponse: Reponse = new Reponse(-1, req.body.reponse, req.body.valide,
      await questionsDao.get(parseInt(req.body.question)));

    // On met à jour la réponse dans la base de donnée et on la retourne à l'utilisateur
    reponsesDao.update(reponse).then(
      (reponseModifiee: Reponse) => {
        res.send(reponseModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {delete} /reponse Supprimer une réponse
   * @apiVersion 0.1.0
   * @apiName DeleteReponse
   * @apiGroup Reponses
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID de la réponse.
   * 
   * @apiSuccess {Number} id l'ID de la réponse.
   * @apiSuccess {String} reponse Le contenu de la réponse.
   * @apiSuccess {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
   * @apiSuccess {Question} question La question associé.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "valide": false,
   *        "question": {
   *          "id": 1,
   *          "qcm": {
   *            "id": 1,
   *            "slug": "test",
   *            "nom": "Test",
   *            "chapitre": {
   *              "id": 2,
   *              "numero": 2,
   *              "nom": "Chap. 2",
   *              "description": "Chapitre 2 du cours 2",
   *              "cours": {
   *                "id": 2,
   *                "slug": "reseau2",
   *                "nom": "Réseaux2",
   *                "description": "Cours contenant tous ce qui est en lien avec les communications",
   *                "categorie": {
   *                  "id": 1,
   *                  "slug": "informatique",
   *                  "nom": "Informatique",
   *                  "description": "Catégorie parente pour le domaine de l'informatique",
   *                  "parent": null
   *                }
   *              }
   *            }
   *          },
   *          "question": "test ?"
   *        },
   *        "reponse": "42"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime la réponse dans la base de donnée et on la retourne à l'utilisateur
    reponsesDao.delete(parseInt(req.query.id)).then(
      (reponseSupprimee: Reponse) => {
        res.send(reponseSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { reponseRouter };