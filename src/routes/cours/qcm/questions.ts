/**
 * Routes /questions/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../../model/types/requete.authentifiee";
import { QuestionsDao } from "../../../model/dao/cours/qcm/questions.dao";
import { Question } from "../../../model/bean/cours/qcm/question";
import { ParametresRechercheQuestions } from "../../../model/types/parametres.recherche";

// Création des instances des DAO
const questionsDao: QuestionsDao = new QuestionsDao();

// Création du routeur
const questionsRouter = express.Router();

/**
 * @api {get} /questions Rechercher des questions
 * @apiVersion 0.1.0
 * @apiName GetQuestions
 * @apiGroup Questions
 * 
 * @apiParam {Number} id l'ID de la question.
 * @apiParam {String} question Un mot clé recherché dans le contenu de la question.
 * @apiParam {QCM} qcm L'ID du QCM associé.
 * 
 * @apiSuccess {Number} id l'ID de la question.
 * @apiSuccess {String} question Le contenu de la question.
 * @apiSuccess {QCM} qcm Le QCM.
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *     [
 *      {
 *        "id": 1,
 *        "qcm": {
 *          "id": 1,
 *          "slug": "test",
 *          "nom": "Test",
 *          "chapitre": {
 *            "id": 2,
 *            "numero": 2,
 *            "nom": "Chap. 2",
 *            "description": "Chapitre 2 du cours 2",
 *            "cours": {
 *              "id": 2,
 *              "slug": "reseau2",
 *              "nom": "Réseaux2",
 *              "description": "Cours contenant tous ce qui est en lien avec les communications",
 *              "categorie": {
 *                "id": 1,
 *                "slug": "informatique",
 *                "nom": "Informatique",
 *                "description": "Catégorie parente pour le domaine de l'informatique",
 *                "parent": null
 *              }
 *            }
 *          }
 *        },
 *        "question": "test ?"
 *      }
 *     ]
 *
 * @apiUse ApiErreurs
 */
questionsRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheQuestions = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.question)
    parametres.question = req.query.question;
  if (req.query.qcm)
    parametres.id_qcm = parseInt(req.query.qcm);

  // On fait la recherche dans la base de donnée
  questionsDao.getAll(parametres).then(
    (questions: Question[]) => {
      res.send(questions);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /questions/:id Recupérer une question
   * @apiVersion 0.1.0
   * @apiName GetQuestion
   * @apiGroup Questions
   * 
   * @apiParam {Number} id l'ID de la question.
   * 
   * @apiSuccess {Number} id l'ID de la question.
   * @apiSuccess {String} question Le contenu de la question.
   * @apiSuccess {QCM} qcm Le QCM.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "qcm": {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        },
   *        "question": "test ?"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    questionsDao.get(parseInt(req.params.id)).then(
      (question: Question) => {
        res.send(question);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { questionsRouter };