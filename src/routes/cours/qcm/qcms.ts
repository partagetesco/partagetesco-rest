/**
 * Routes /qcms/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response, Request } from "express-serve-static-core";
import { ReponseError } from "../../../model/errors/reponse.error";
import { QCMsDao } from "../../../model/dao/cours/qcm/qcms.dao";
import { ParametresRechercheQCM } from "../../../model/types/parametres.recherche";
import { QCM } from "../../../model/bean/cours/qcm/qcm";

// Création des instances des DAO
const qcmsDao: QCMsDao = new QCMsDao();

// Création du routeur
const qcmsRouter = express.Router();

/**
 * @api {get} /qcms Rechercher des QCM
 * @apiVersion 0.1.0
 * @apiName GetQCMs
 * @apiGroup QCM
 * 
 * @apiParam {Number} id l'ID du QCM.
 * @apiParam {String} slug Un mot clé recherché dans le slug du QCM.
 * @apiParam {String} nom Un mot clé recherché dans le nom du QCM.
 * @apiParam {Number} chapitre L'ID du chapitre des QCM.
 * 
 * @apiSuccess {Number} id l'ID du QCM.
 * @apiSuccess {String} slug Le slug du QCM.
 * @apiSuccess {String} nom Le nom du QCM.
 * @apiSuccess {Chapitre} chapitre Le chapitre du QCM.
 *
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      [
 *        {
 *          "id": 1,
 *          "slug": "test",
 *          "nom": "Test",
 *          "chapitre": {
 *            "id": 2,
 *            "numero": 2,
 *            "nom": "Chap. 2",
 *            "description": "Chapitre 2 du cours 2",
 *            "cours": {
 *              "id": 2,
 *              "slug": "reseau2",
 *              "nom": "Réseaux2",
 *              "description": "Cours contenant tous ce qui est en lien avec les communications",
 *              "categorie": {
 *                "id": 1,
 *                "slug": "informatique",
 *                "nom": "Informatique",
 *                "description": "Catégorie parente pour le domaine de l'informatique",
 *                "parent": null
 *              }
 *            }
 *          }
 *        }
 *      ]
 *
 * @apiUse ApiErreurs
 */
qcmsRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheQCM = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.slug)
    parametres.slug = req.query.slug;
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.chapitre)
    parametres.id_chapitre = parseInt(req.query.chapitre);

  // On fait la recherche dans la base de donnée
  qcmsDao.getAll(parametres).then(
    (qcms: QCM[]) => {
      res.send(qcms);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /qcms/:id Recupérer un QCM
   * @apiVersion 0.1.0
   * @apiName GetQCM
   * @apiGroup QCM
   * 
   * @apiParam {Number} id l'ID du QCM.
   * 
   * @apiSuccess {Number} id l'ID du QCM.
   * @apiSuccess {String} slug Le slug du QCM.
   * @apiSuccess {String} nom Le nom du QCM.
   * @apiSuccess {Chapitre} chapitre Le chapitre du QCM.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *        {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        }
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: Request, res: Response, next: NextFunction) => {
    qcmsDao.get(parseInt(req.params.id)).then(
      (qcm: QCM) => {
        res.send(qcm);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { qcmsRouter };