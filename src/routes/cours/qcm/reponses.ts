/**
 * Routes /reponses/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ParametresRechercheReponses } from "../../../model/types/parametres.recherche";
import { ReponsesDao } from "../../../model/dao/cours/qcm/reponses.dao";
import { RequeteAuthentifiee } from "../../../model/types/requete.authentifiee";
import { Reponse } from "../../../model/bean/cours/qcm/reponse";
import { ReponseError } from "../../../model/errors/reponse.error";

// Création des instances des DAO
const reponsesDao: ReponsesDao = new ReponsesDao();

// Création du routeur
const reponsesRouter = express.Router();

/**
 * @api {get} /reponses Rechercher des réponses
 * @apiVersion 0.1.0
 * @apiName GetReponses
 * @apiGroup Reponses
 * 
 * @apiParam {Number} id l'ID de la réponse.
 * @apiParam {String} reponse Un mot clé recherché dans le contenu de la réponse.
 * @apiParam {Boolean} valide Un booléen indiquant si les réponse sont valide ou non.
 * @apiParam {Number} question L'ID de la question associé.
 * 
 * @apiSuccess {Number} id l'ID de la réponse.
 * @apiSuccess {String} reponse Le contenu de la réponse.
 * @apiSuccess {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
 * @apiSuccess {Question} question La question associé.
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "valide": false,
 *        "question": {
 *          "id": 1,
 *          "qcm": {
 *            "id": 1,
 *            "slug": "test",
 *            "nom": "Test",
 *            "chapitre": {
 *              "id": 2,
 *              "numero": 2,
 *              "nom": "Chap. 2",
 *              "description": "Chapitre 2 du cours 2",
 *              "cours": {
 *                "id": 2,
 *                "slug": "reseau2",
 *                "nom": "Réseaux2",
 *                "description": "Cours contenant tous ce qui est en lien avec les communications",
 *                "categorie": {
 *                  "id": 1,
 *                  "slug": "informatique",
 *                  "nom": "Informatique",
 *                  "description": "Catégorie parente pour le domaine de l'informatique",
 *                  "parent": null
 *                }
 *              }
 *            }
 *          },
 *          "question": "test ?"
 *        },
 *        "reponse": "42"
 *      }
 *    ]
 *
 * @apiUse ApiErreurs
 */
reponsesRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheReponses = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.reponse)
    parametres.reponse = req.query.reponse;
  if (req.query.valide != null)
    parametres.valide = req.query.valide == 'true';
  if (req.query.question)
    parametres.id_question = parseInt(req.query.question);

  // On fait la recherche dans la base de donnée
  reponsesDao.getAll(parametres).then(
    (reponses: Reponse[]) => {
      res.send(reponses);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

/**
 * @api {get} /reponses/:id Récupérer une réponse
 * @apiVersion 0.1.0
 * @apiName GetReponse
 * @apiGroup Reponses
 * 
 * @apiParam {Number} id l'ID de la réponse.
 * 
 * @apiSuccess {Number} id l'ID de la réponse.
 * @apiSuccess {String} reponse Le contenu de la réponse.
 * @apiSuccess {Boolean} valide Un booléen indiquant si la réponse est valide ou non.
 * @apiSuccess {Question} question La question associé.
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 1,
 *        "valide": false,
 *        "question": {
 *          "id": 1,
 *          "qcm": {
 *            "id": 1,
 *            "slug": "test",
 *            "nom": "Test",
 *            "chapitre": {
 *              "id": 2,
 *              "numero": 2,
 *              "nom": "Chap. 2",
 *              "description": "Chapitre 2 du cours 2",
 *              "cours": {
 *                "id": 2,
 *                "slug": "reseau2",
 *                "nom": "Réseaux2",
 *                "description": "Cours contenant tous ce qui est en lien avec les communications",
 *                "categorie": {
 *                  "id": 1,
 *                  "slug": "informatique",
 *                  "nom": "Informatique",
 *                  "description": "Catégorie parente pour le domaine de l'informatique",
 *                  "parent": null
 *                }
 *              }
 *            }
 *          },
 *          "question": "test ?"
 *        },
 *        "reponse": "42"
 *      }
 *
 * @apiUse ApiErreurs
 */
.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  reponsesDao.get(parseInt(req.params.id)).then(
    (reponse: Reponse) => {
      res.send(reponse);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
});

export { reponsesRouter };