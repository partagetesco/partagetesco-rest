/**
 * Routes /question/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../../model/types/requete.authentifiee";
import { QCMsDao } from "../../../model/dao/cours/qcm/qcms.dao";
import { QuestionsDao } from "../../../model/dao/cours/qcm/questions.dao";
import { Question } from "../../../model/bean/cours/qcm/question";

// Création des instances des DAO
const questionsDao: QuestionsDao = new QuestionsDao();
const qcmsDao: QCMsDao = new QCMsDao();

// Création du routeur
const questionRouter = express.Router();

/**
 * @api {post} /question Insérer une nouvelle question
 * @apiVersion 0.1.0
 * @apiName PostQuestion
 * @apiGroup Questions
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données de la question au format JSON.
 * @apiParam {String} question Le contenu de la question.
 * @apiParam {Number} qcm L'ID du QCM associé.
 * 
 * @apiParamExample {json} Exemple:
 *  {
 *    "question": "test ?",
 *    "qcm": 1
 *  }
 * 
 * @apiSuccess {Number} id l'ID de la question.
 * @apiSuccess {String} question Le contenu de la question.
 * @apiSuccess {QCM} qcm Le QCM.
 *
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 1,
 *        "qcm": {
 *          "id": 1,
 *          "slug": "test",
 *          "nom": "Test",
 *          "chapitre": {
 *            "id": 2,
 *            "numero": 2,
 *            "nom": "Chap. 2",
 *            "description": "Chapitre 2 du cours 2",
 *            "cours": {
 *              "id": 2,
 *              "slug": "reseau2",
 *              "nom": "Réseaux2",
 *              "description": "Cours contenant tous ce qui est en lien avec les communications",
 *              "categorie": {
 *                "id": 1,
 *                "slug": "informatique",
 *                "nom": "Informatique",
 *                "description": "Catégorie parente pour le domaine de l'informatique",
 *                "parent": null
 *              }
 *            }
 *          }
 *        },
 *        "question": "test ?"
 *      }
 *
 * @apiUse ApiErreurs
 */
questionRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer la question
  let question: Question = new Question(-1, req.body.question,
    await qcmsDao.get(parseInt(req.body.qcm)));

  // On enregistre la question dans la base de donnée et on la retourne à l'utilisateur
  questionsDao.insert(question).then(
    (questionCree: Question) => {
      res.send(questionCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /question Mettre à jour une question
   * @apiVersion 0.1.0
   * @apiName PutQuestion
   * @apiGroup Questions
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données de la question au format JSON.
   * @apiParam {Number} id L'ID de la question.
   * @apiParam {String} question Le contenu de la question.
   * @apiParam {Number} qcm L'ID du QCM associé.
   * 
   * @apiParamExample {json} Exemple:
   *  {
   *    "id": 1,
   *    "question": "test ?",
   *    "qcm": 1
   *  }
   * 
   * @apiSuccess {Number} id l'ID de la question.
   * @apiSuccess {String} question Le contenu de la question.
   * @apiSuccess {QCM} qcm Le QCM.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "qcm": {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        },
   *        "question": "test ?"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la question
    let question: Question = new Question(-1, req.body.question,
      await qcmsDao.get(parseInt(req.body.qcm)));

    // On met à jour la question dans la base de donnée et on la retourne à l'utilisateur
    questionsDao.update(question).then(
      (questionModifiee: Question) => {
        res.send(questionModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /question Supprimer une question
   * @apiVersion 0.1.0
   * @apiName DeleteQuestion
   * @apiGroup Questions
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID de la question.
   * 
   * @apiSuccess {Number} id l'ID de la question.
   * @apiSuccess {String} question Le contenu de la question.
   * @apiSuccess {QCM} qcm Le QCM.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "qcm": {
   *          "id": 1,
   *          "slug": "test",
   *          "nom": "Test",
   *          "chapitre": {
   *            "id": 2,
   *            "numero": 2,
   *            "nom": "Chap. 2",
   *            "description": "Chapitre 2 du cours 2",
   *            "cours": {
   *              "id": 2,
   *              "slug": "reseau2",
   *              "nom": "Réseaux2",
   *              "description": "Cours contenant tous ce qui est en lien avec les communications",
   *              "categorie": {
   *                "id": 1,
   *                "slug": "informatique",
   *                "nom": "Informatique",
   *                "description": "Catégorie parente pour le domaine de l'informatique",
   *                "parent": null
   *              }
   *            }
   *          }
   *        },
   *        "question": "test ?"
   *      }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime la question dans la base de donnée et on la retourne à l'utilisateur
    questionsDao.delete(parseInt(req.query.id)).then(
      (questionSupprimee: Question) => {
        res.send(questionSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { questionRouter };