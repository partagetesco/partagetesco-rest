/**
 * Routes /chapitres/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { RequeteAuthentifiee } from '../../model/types/requete.authentifiee';
import { ParametresRechercheChapitres } from "../../model/types/parametres.recherche";
import { ChapitresDao } from "../../model/dao/cours/chapitres.dao";
import { Chapitre } from "../../model/bean/cours/chapitre";

// Création des instances des DAO
const chapitresDao: ChapitresDao = new ChapitresDao();

// Création du routeur
const chapitresRouter = express.Router();

/**
 * @api {get} /chapitres Rechercher des chapitres
 * @apiVersion 0.1.0
 * @apiName GetChapitres
 * @apiGroup Chapitres
 * 
 * @apiParam {Number} id L'ID du chapitre
 * @apiParam {Number} numero Le numéro du chapitre
 * @apiParam {String} nom Le nom du chapitre
 * @apiParam {String} description La description du chapitre
 * @apiParam {Number} cours L'ID du cours
 * 
 * @apiSuccess {Number} id l'ID du chapitre.
 * @apiSuccess {Number} numero Le numéro du chapitre
 * @apiSuccess {String} nom Le nom du chapitre
 * @apiSuccess {String} description La description du chapitre
 * @apiSuccess {Categorie} cours Le cours
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 2,
 *        "numero": 2,
 *        "nom": "Chap. 2",
 *        "description": "Chapitre 2 du cours 2",
 *        "cours": {
 *          "id": 2,
 *          "slug": "reseau2",
 *          "nom": "Réseaux2",
 *          "description": "Cours contenant tous ce qui est en lien avec les communications",
 *          "categorie": {
 *            "id": 1,
 *            "slug": "informatique",
 *            "nom": "Informatique",
 *            "description": "Catégorie parente pour le domaine de l'informatique",
 *            "parent": null
 *          }
 *        }
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
chapitresRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheChapitres = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.numero)
    parametres.numero = parseInt(req.query.numero);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.cours)
    parametres.id_cours = parseInt(req.query.cours);

  // On fait la recherche dans la base de donnée
  chapitresDao.getAll(parametres).then(
    (chapitres: Chapitre[]) => {
      res.send(chapitres);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /chapitres/:id Recupérer un chapitre
   * @apiVersion 0.1.0
   * @apiName GetChapitre
   * @apiGroup Chapitres
   * 
   * @apiParam {Number} id L'ID du chapitre
   * 
   * @apiSuccess {Number} id l'ID du chapitre.
   * @apiSuccess {Number} numero Le numéro du chapitre
   * @apiSuccess {String} nom Le nom du chapitre
   * @apiSuccess {String} description La description du chapitre
   * @apiSuccess {Categorie} cours Le cours
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "numero": 2,
   *        "nom": "Chap. 2",
   *        "description": "Chapitre 2 du cours 2",
   *        "cours": {
   *          "id": 2,
   *          "slug": "reseau2",
   *          "nom": "Réseaux2",
   *          "description": "Cours contenant tous ce qui est en lien avec les communications",
   *          "categorie": {
   *            "id": 1,
   *            "slug": "informatique",
   *            "nom": "Informatique",
   *            "description": "Catégorie parente pour le domaine de l'informatique",
   *            "parent": null
   *          }
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    chapitresDao.get(parseInt(req.params.id)).then(
      (chapitre: Chapitre) => {
        res.send(chapitre);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { chapitresRouter };