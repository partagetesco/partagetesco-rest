/**
 * Routes /categorie/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { CategoriesDao } from "../../model/dao/cours/categories.dao";
import { Categorie } from "../../model/bean/cours/categorie";

// Création des instances des DAO
const categoriesDao: CategoriesDao = new CategoriesDao();

// Création du routeur
const categorieRouter = express.Router();

  /**
   * @api {post} /categorie Insérer une nouvelle catégorie
   * @apiVersion 0.1.0
   * @apiName PostCategorie
   * @apiGroup Categories
   * @apiPermission utilisateurs
   * 
   * @apiParam {json} body Les données de la catégorie au format JSON
   * @apiParam {String} slug Le slug de la catégorie
   * @apiParam {String} nom Le nom de la catégorie
   * @apiParam {String} description La description de la catégorie
   * @apiParam {Number} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiParamExample {json} Exemple-1:
   *      {
   *        "slug": "cat1",
   *        "nom": "Catégorie 1",
   *        "description": "La description"
   *      }
   * 
   * @apiParamExample {json} Exemple-2:
   *      {
   *        "slug": "cat1",
   *        "nom": "Catégorie 1",
   *        "description": "La description",
   *        "parent": 1
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la catégorie.
   * @apiSuccess {String} slug Le slug de la catégorie
   * @apiSuccess {String} nom Le nom de la catégorie
   * @apiSuccess {String} description La description de la catégorie
   * @apiSuccess {Categorie} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "slug": "informatique",
   *        "nom": "Informatique",
   *        "description": "Catégorie parente pour le domaine de l'informatique",
   *      }
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau",
   *        "nom": "Réseau",
   *        "description": "Catégorie le réseau",
   *        "parent": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   *
   * @apiUse ApiErreurs
   */
categorieRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la categorie
    let categorie: Categorie = new Categorie(-1, req.body.slug, req.body.nom,
      req.body.description);
    
    // Si la catégorie parente est défini on la récupère et on l'associe
    if(req.body.parent)
      categorie.$parent = await categoriesDao.get(parseInt(req.body.parent))

    // On enregistre la categorie dans la base de donnée et on la retourne à l'utilisateur
    categoriesDao.insert(categorie).then(
      (categorieCree: Categorie) => {
        res.send(categorieCree);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {put} /categorie Mettre à jour une catégorie
   * @apiVersion 0.1.0
   * @apiName PutCategorie
   * @apiGroup Categories
   * @apiPermission utilisateurs
   * 
   * @apiParam {json} body Les données de la catégorie au format JSON
   * @apiParam {Number} id L'ID de la catégorie
   * @apiParam {String} slug Le slug de la catégorie
   * @apiParam {String} nom Le nom de la catégorie
   * @apiParam {String} description La description de la catégorie
   * @apiParam {Number} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiParamExample {json} Exemple-1:
   *      {
   *        "id": 1,
   *        "slug": "cat1",
   *        "nom": "Catégorie 1",
   *        "description": "La description"
   *      }
   * 
   * @apiParamExample {json} Exemple-2:
   *      {
   *        "id": 2,
   *        "slug": "cat1",
   *        "nom": "Catégorie 1",
   *        "description": "La description",
   *        "parent": 1
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la catégorie.
   * @apiSuccess {String} slug Le slug de la catégorie
   * @apiSuccess {String} nom Le nom de la catégorie
   * @apiSuccess {String} description La description de la catégorie
   * @apiSuccess {Categorie} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "slug": "informatique",
   *        "nom": "Informatique",
   *        "description": "Catégorie parente pour le domaine de l'informatique",
   *      }
   * 
   * @apiSuccessExample Success-Response-1:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau",
   *        "nom": "Réseau",
   *        "description": "Catégorie le réseau",
   *        "parent": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la categorie
    let categorie: Categorie = new Categorie(parseInt(req.body.id), req.body.slug,
    req.body.nom, req.body.description);
    
    // Si la catégorie parente est défini on la récupère et on l'associe
    if(req.body.parent)
      categorie.$parent = await categoriesDao.get(parseInt(req.body.parent))

    // On met à jour le categorie dans la base de donnée et on la retourne à l'utilisateur
    categoriesDao.update(categorie).then(
      (categorieModifiee: Categorie) => {
        res.send(categorieModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {delete} /categorie Supprimer une catégorie
   * @apiVersion 0.1.0
   * @apiName DeleteCategorie
   * @apiGroup Categories
   * @apiPermission utilisateurs
   * 
   * @apiParam {Number} id L'ID de la catégorie
   * 
   * @apiSuccess {Number} id l'ID de la catégorie.
   * @apiSuccess {String} slug Le slug de la catégorie
   * @apiSuccess {String} nom Le nom de la catégorie
   * @apiSuccess {String} description La description de la catégorie
   * @apiSuccess {Categorie} parent La catégorie parente de la catégorie (optionnel)
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "slug": "informatique",
   *        "nom": "Informatique",
   *        "description": "Catégorie parente pour le domaine de l'informatique",
   *      }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le categorie dans la base de donnée et on la retourne à l'utilisateur
    categoriesDao.delete(parseInt(req.query.id)).then(
      (categorieSupprimee: Categorie) => {
        res.send(categorieSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { categorieRouter };