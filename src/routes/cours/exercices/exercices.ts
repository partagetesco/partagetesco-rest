/**
 * Routes /exercices/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response, Request } from "express-serve-static-core";
import { ParametresRechercheExercicesPratique } from "../../../model/types/parametres.recherche";
import { ExercicesPratiqueDao } from "../../../model/dao/cours/exercices/exercices.pratique.dao";
import { ExercicePratique } from "../../../model/bean/cours/exercices/exercice.pratique";
import { ReponseError } from "../../../model/errors/reponse.error";

// Création des instances des DAO
const exercicesDao: ExercicesPratiqueDao = new ExercicesPratiqueDao();

// Création du routeur
const exercicesRouter = express.Router();

/**
 * @api {get} /exercices Rechercher des exercices
 * @apiVersion 0.1.0
 * @apiName GetExercices
 * @apiGroup Exercices
 * 
 * @apiParam {Number} id L'ID de l'exercice.
 * @apiParam {String} enonce Un mot clé recherché dans l'énoncé des exercices.
 * @apiParam {Number} chapitre Le chapitre associé aux exercices.
 * 
 * @apiSuccess {Number} id l'ID de l'exercice.
 * @apiSuccess {String} enonce L'énoncé de l'exercice.
 * @apiSuccess {Chapitre} chapitre Le chapitre de l'exercice.
 * @apiSuccess {Utilisateur[]} correcteurs Les correcteurs de l'exercice, présent si la correction de l'exercice n'est pas automatisé.
 * @apiSuccess {Fichier} generateur Le fichier de générateur des entrées, présent si la correction est automatisé.
 * @apiSuccess {Fichier} correcteur Le fichier du programme qui donne les sorties correctes en fonction des entrées, présent si la correction est automatisé.
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "enonce": "Un énoncé de test",
 *        "chapitre": {
 *          "id": 2,
 *          "numero": 2,
 *          "nom": "Chap. 2",
 *          "description": "Chapitre 2 du cours 2",
 *          "cours": {
 *            "id": 2,
 *            "slug": "reseau2",
 *            "nom": "Réseaux2",
 *            "description": "Cours contenant tous ce qui est en lien avec les communications",
 *            "categorie": {
 *              "id": 1,
 *              "slug": "informatique",
 *              "nom": "Informatique",
 *              "description": "Catégorie parente pour le domaine de l'informatique",
 *              "parent": null
 *            }
 *          }
 *        },
 *        "correcteurs": [
 *          {
 *          "id": 3,
 *          "pseudo": "developpix",
 *          "mail": "developpix@gmail.com",
 *          "biographie": ""
 *          }
 *        ]
 *      }
 *    ]
 *
 * @apiUse ApiErreurs
 */
exercicesRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheExercicesPratique = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.enonce)
    parametres.enonce = req.query.enonce;
  if (req.query.chapitre)
    parametres.id_chapitre = parseInt(req.query.chapitre);

  // On fait la recherche dans la base de donnée
  exercicesDao.getAll(parametres).then(
    (exercices: ExercicePratique[]) => {
      res.send(exercices);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /exercices/:id Recupérer un exercice
   * @apiVersion 0.1.0
   * @apiName GetExercice
   * @apiGroup Exercices
   * 
   * @apiParam {Number} id L'ID de l'exercice.
   * 
   * @apiSuccess {Number} id l'ID de l'exercice.
   * @apiSuccess {String} enonce L'énoncé de l'exercice.
   * @apiSuccess {Chapitre} chapitre Le chapitre de l'exercice.
   * @apiSuccess {Utilisateur[]} correcteurs Les correcteurs de l'exercice, présent si la correction de l'exercice n'est pas automatisé.
   * @apiSuccess {Fichier} generateur Le fichier de générateur des entrées, présent si la correction est automatisé.
   * @apiSuccess {Fichier} correcteur Le fichier du programme qui donne les sorties correctes en fonction des entrées, présent si la correction est automatisé.
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *    [
   *      {
   *        "id": 1,
   *        "enonce": "Un énoncé de test",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "correcteurs": [
   *          {
   *          "id": 3,
   *          "pseudo": "developpix",
   *          "mail": "developpix@gmail.com",
   *          "biographie": ""
   *          }
   *        ]
   *      }
   *    ]
   *
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: Request, res: Response, next: NextFunction) => {
    exercicesDao.get(parseInt(req.params.id)).then(
      (exercice: ExercicePratique) => {
        res.send(exercice);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      }
    )
  });

export { exercicesRouter };