/**
 * Routes /exercice/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { RequeteAuthentifiee } from '../../../model/types/requete.authentifiee';
import { ExercicesPratiqueDao } from "../../../model/dao/cours/exercices/exercices.pratique.dao";
import { ExercicePratiqueAuto } from "../../../model/bean/cours/exercices/exercice.pratique.auto";
import { ChapitresDao } from "../../../model/dao/cours/chapitres.dao";
import { ExercicePratique } from "../../../model/bean/cours/exercices/exercice.pratique";
import { ReponseError } from "../../../model/errors/reponse.error";
import { FichiersDao } from "../../../model/dao/fichiers/fichiers.dao";
import { ExercicePratiqueManuel } from "../../../model/bean/cours/exercices/exercice.pratique.manuel";
import { Utilisateur } from "../../../model/bean/utilisateurs/utilisateur";
import { UtilisateursDao } from "../../../model/dao/utilisateurs/utilisateurs.dao";

// Création des instances des DAO
const exercicesDao: ExercicesPratiqueDao = new ExercicesPratiqueDao();
const chapitresDao: ChapitresDao = new ChapitresDao();
const fichiersDao: FichiersDao = new FichiersDao();
const utilisateursDao: UtilisateursDao = new UtilisateursDao();

// Création du routeur
const exerciceRouter = express.Router();

/**
 * @api {post} /exercice Insérer un nouvel exercice
 * @apiVersion 0.1.0
 * @apiName PostExercice
 * @apiGroup Exercices
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données de l'exercice au format JSON (avec les attributs ci-dessous). 
 * @apiParam {String} enonce L'énoncé de l'exercice.
 * @apiParam {Number} chapitre l'ID du chapitre associé à l'exercice
 * @apiParam {Boolean} auto Un booléen indiquant si l'exercice se corrige automatiquement.
 * @apiParam {Number} genererateur l'ID du programme de générateur des entrées (fichier), obligatoire si auto=true.
 * @apiParam {Number} correcteur l'ID du programme donnant la bonne sortie en fonction des entrées (fichier), obligatoire si auto=true.
 * @apiParam {Number[]} correcteurs Un tableau des ID des utilisateurs en charge de la correction, obligatoire si auto=false.
 * 
 * 
 * @apiParamExample {json} Exemple correction automatique:
 *  {
 *    "enonce": "<h1>Un enoncé de test</h1>\n<p>Un paragraphe de test</p>",
 *    "chapitre": 1,
 *    "auto": true,
 *    "generateur": 1,
 *    "correcteur": 2
 *  }
 * @apiParamExample {json} Exemple correction manuelle:
 *  {
 *    "enonce": "<h1>Un enoncé de test</h1>\n<p>Un paragraphe de test</p>",
 *    "chapitre": 1,
 *    "auto": false,
 *    "correcteurs": [ 1, 2 ]
 *  }
 * 
 * @apiSuccess {Number} id l'ID de l'exercice.
 * @apiSuccess {String} enonce L'énoncé de l'exercice.
 * @apiSuccess {Chapitre} chapitre Le chapitre de l'exercice.
 * @apiSuccess {Utilisateur[]} correcteurs Les correcteurs de l'exercice, présent si la correction de l'exercice n'est pas automatisé.
 * @apiSuccess {Fichier} generateur Le fichier de générateur des entrées, présent si la correction est automatisé.
 * @apiSuccess {Fichier} correcteur Le fichier du programme qui donne les sorties correctes en fonction des entrées, présent si la correction est automatisé.
 *
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 3,
 *        "enonce": "Un énoncé de test",
 *        "chapitre": {
 *          "id": 2,
 *          "numero": 2,
 *          "nom": "Chap. 2",
 *          "description": "Chapitre 2 du cours 2",
 *          "cours": {
 *            "id": 2,
 *            "slug": "reseau2",
 *            "nom": "Réseaux2",
 *            "description": "Cours contenant tous ce qui est en lien avec les communications",
 *            "categorie": {
 *              "id": 1,
 *              "slug": "informatique",
 *              "nom": "Informatique",
 *              "description": "Catégorie parente pour le domaine de l'informatique",
 *              "parent": null
 *            }
 *          }
 *        },
 *        "correcteurs": [
 *          {
 *          "id": 3,
 *          "pseudo": "developpix",
 *          "mail": "developpix@gmail.com",
 *          "biographie": ""
 *          }
 *        ]
 *      }
 *
 * @apiUse ApiErreurs
 */
exerciceRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'exercice
  let exercice: ExercicePratique = await creerExercice(req.body);

  // On enregistre l'exercice dans la base de donnée et on la retourne à l'utilisateur
  exercicesDao.insert(exercice).then(
    (exerciceCree: ExercicePratique) => {
      res.send(exerciceCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /exercice Mettre à jour un exercice
   * @apiVersion 0.1.0
   * @apiName PutExercice
   * @apiGroup Exercices
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données de l'exercice au format JSON (avec les attributs ci-dessous).
   * @apiParam {Number} id L'ID de l'exercice.
   * @apiParam {String} enonce L'énoncé de l'exercice.
   * @apiParam {Number} chapitre l'ID du chapitre associé à l'exercice
   * @apiParam {Boolean} auto Un booléen indiquant si l'exercice se corrige automatiquement.
   * @apiParam {Number} genererateur l'ID du programme de générateur des entrées (fichier), obligatoire si auto=true.
   * @apiParam {Number} correcteur l'ID du programme donnant la bonne sortie en fonction des entrées (fichier), obligatoire si auto=true.
   * @apiParam {Number[]} correcteurs Un tableau des ID des utilisateurs en charge de la correction, obligatoire si auto=false.
   * 
   * 
   * @apiParamExample {json} Exemple correction automatique:
   *  {
   *    "id": 1,
   *    "enonce": "<h1>Un enoncé de test</h1>\n<p>Un paragraphe de test</p>",
   *    "chapitre": 1,
   *    "auto": true,
   *    "generateur": 1,
   *    "correcteur": 2
   *  }
   * @apiParamExample {json} Exemple correction manuelle:
   *  {
   *    "id": 1,
   *    "enonce": "<h1>Un enoncé de test</h1>\n<p>Un paragraphe de test</p>",
   *    "chapitre": 1,
   *    "auto": false,
   *    "correcteurs": [ 1, 2 ]
   *  }
   * 
   * @apiSuccess {Number} id l'ID de l'exercice.
   * @apiSuccess {String} enonce L'énoncé de l'exercice.
   * @apiSuccess {Chapitre} chapitre Le chapitre de l'exercice.
   * @apiSuccess {Utilisateur[]} correcteurs Les correcteurs de l'exercice, présent si la correction de l'exercice n'est pas automatisé.
   * @apiSuccess {Fichier} generateur Le fichier de générateur des entrées, présent si la correction est automatisé.
   * @apiSuccess {Fichier} correcteur Le fichier du programme qui donne les sorties correctes en fonction des entrées, présent si la correction est automatisé.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "enonce": "Un énoncé de test",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "correcteurs": [
   *          {
   *          "id": 3,
   *          "pseudo": "developpix",
   *          "mail": "developpix@gmail.com",
   *          "biographie": ""
   *          }
   *        ]
   *      }
   *
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer l'exercice
    let exercice: ExercicePratique = await creerExercice(req.body);

    // On met à jour l'exercice dans la base de donnée et on la retourne à l'utilisateur
    exercicesDao.update(exercice).then(
      (exerciceModifiee: ExercicePratique) => {
        res.send(exerciceModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /exercice Supprimer un exercice
   * @apiVersion 0.1.0
   * @apiName DeleteExercice
   * @apiGroup Exercices
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id L'ID de l'exercice.
   * 
   * @apiSuccess {Number} id l'ID de l'exercice.
   * @apiSuccess {String} enonce L'énoncé de l'exercice.
   * @apiSuccess {Chapitre} chapitre Le chapitre de l'exercice.
   * @apiSuccess {Utilisateur[]} correcteurs Les correcteurs de l'exercice, présent si la correction de l'exercice n'est pas automatisé.
   * @apiSuccess {Fichier} generateur Le fichier de générateur des entrées, présent si la correction est automatisé.
   * @apiSuccess {Fichier} correcteur Le fichier du programme qui donne les sorties correctes en fonction des entrées, présent si la correction est automatisé.
   *
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "enonce": "Un énoncé de test",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "correcteurs": [
   *          {
   *          "id": 3,
   *          "pseudo": "developpix",
   *          "mail": "developpix@gmail.com",
   *          "biographie": ""
   *          }
   *        ]
   *      }
   *
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime l'exercice dans la base de donnée et on la retourne à l'utilisateur
    exercicesDao.delete(parseInt(req.query.id)).then(
      (exerciceSupprimee: ExercicePratique) => {
        res.send(exerciceSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

/**
 * Méthode permettant de créer un exercice à partir de la requête
 * 
 * @param donnees les données de la requête
 * @returns l'exercice pratique
 */
async function creerExercice(donnees: any): Promise<ExercicePratique> {
  if (donnees.auto) {
    return new ExercicePratiqueAuto(donnees.id ? parseInt(donnees.id) : -1,
      donnees.enonce, await chapitresDao.get(parseInt(donnees.chapitre)), await fichiersDao.get(parseInt(donnees.generateur)), await fichiersDao.get(parseInt(donnees.correcteur)));
  } else {
    // On crée la liste des correcteurs
    let correcteurs: Utilisateur[] = [];
    // On récupère les correcteurs
    for (let i = 0; i < donnees.correcteurs.length; i++) {
      correcteurs.push(await utilisateursDao.get(parseInt(donnees.correcteurs[i])));
    }

    return new ExercicePratiqueManuel(donnees.id ? parseInt(donnees.id) : -1,
      donnees.enonce, await chapitresDao.get(parseInt(donnees.chapitre)), correcteurs);
  }
}

export { exerciceRouter };
