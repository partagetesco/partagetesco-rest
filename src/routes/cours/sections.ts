/**
 * Routes /sections/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { RequeteAuthentifiee } from '../../model/types/requete.authentifiee';
import { ParametresRechercheSections } from "../../model/types/parametres.recherche";
import { Section } from "../../model/bean/cours/section";
import { SectionsDao } from "../../model/dao/cours/sections.dao";

// Création des instances des DAO
const sectionsDao: SectionsDao = new SectionsDao();

// Création du routeur
const sectionsRouter = express.Router();

/**
 * @api {get} /sections Rechercher des sections
 * @apiVersion 0.1.0
 * @apiName GetSections
 * @apiGroup Sections
 * 
 * @apiParam {Number} id L'ID de la section
 * @apiParam {Number} numero Le numéro de la section
 * @apiParam {String} nom Le nom de la section
 * @apiParam {String} contenu Le contenu de la section
 * @apiParam {Number} chapitre L'ID du chapitre
 * 
 * @apiSuccess {Number} id l'ID de la section.
 * @apiSuccess {Number} numero Le numéro de la section
 * @apiSuccess {String} nom Le nom de la section
 * @apiSuccess {String} contenu Le contenu de la section
 * @apiSuccess {Chapitre} chapitre Le chapitre
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "numero": 1,
 *        "nom": "Section 1",
 *        "chapitre": {
 *          "id": 2,
 *          "numero": 2,
 *          "nom": "Chap. 2",
 *          "description": "Chapitre 2 du cours 2",
 *          "cours": {
 *            "id": 2,
 *            "slug": "reseau2",
 *            "nom": "Réseaux2",
 *            "description": "Cours contenant tous ce qui est en lien avec les communications",
 *            "categorie": {
 *              "id": 1,
 *              "slug": "informatique",
 *              "nom": "Informatique",
 *              "description": "Catégorie parente pour le domaine de l'informatique",
 *              "parent": null
 *            }
 *          }
 *        },
 *        "contenu": "<code>&lt;script&gt;Hello world !&lt;script&gt;&lt;code&gt;&lt;/script&gt;</code>"
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
sectionsRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheSections = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.numero)
    parametres.numero = parseInt(req.query.numero);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.contenu)
    parametres.contenu = req.query.contenu;
  if (req.query.chapitre)
    parametres.id_chapitre = parseInt(req.query.chapitre);

  // On fait la recherche dans la base de donnée
  sectionsDao.getAll(parametres).then(
    (sections: Section[]) => {
      res.send(sections);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /sections/:id Recupérer une section
   * @apiVersion 0.1.0
   * @apiName GetSection
   * @apiGroup Sections
   * 
   * @apiParam {Number} id L'ID de la section
   * 
   * @apiSuccess {Number} id l'ID de la section.
   * @apiSuccess {Number} numero Le numéro de la section
   * @apiSuccess {String} nom Le nom de la section
   * @apiSuccess {String} contenu Le contenu de la section
   * @apiSuccess {Chapitre} chapitre Le chapitre
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 1,
   *        "nom": "Section 1",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "contenu": "<code>&lt;script&gt;Hello world !&lt;script&gt;&lt;code&gt;&lt;/script&gt;</code>"
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    sectionsDao.get(parseInt(req.params.id)).then(
      (section: Section) => {
        res.send(section);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { sectionsRouter };