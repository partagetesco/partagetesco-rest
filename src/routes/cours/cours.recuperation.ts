/**
 * Routes /cours/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response, Request } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { ParametresRechercheCours } from "../../model/types/parametres.recherche";
import { CoursDao } from "../../model/dao/cours/cours.dao";
import { Cours } from "../../model/bean/cours/cours";

// Création des instances des DAO
const coursDao: CoursDao = new CoursDao();

// Création du routeur
const coursRecuperationRouter = express.Router();

  /**
   * @api {get} /cours Rechercher des cours
   * @apiVersion 0.1.0
   * @apiName GetCours
   * @apiGroup Cours
   * 
   * @apiParam {Number} id L'ID du cours
   * @apiParam {String} slug Un mot clé recherché dans le slug du cours
   * @apiParam {String} nom Un mot clé recherché dans le nom du cours
   * @apiParam {String} description Un mot clé recherché dans la description du cours
   * @apiParam {Number} categorie L'ID de la catégorie
   * 
   * @apiSuccess {Number} id l'ID du cours.
   * @apiSuccess {Number} numero Le numéro du cours
   * @apiSuccess {String} nom Le nom du cours
   * @apiSuccess {String} description La description du cours
   * @apiSuccess {Categorie} categorie La catégorie
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *    [
   *      {
   *        "id": 2,
   *        "slug": "reseau2",
   *        "nom": "Réseaux2",
   *        "description": "Cours contenant tous ce qui est en lien avec les communications",
   *        "categorie": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   *    ]
   * 
   * @apiUse ApiErreurs
   */
coursRecuperationRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheCours = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.slug)
    parametres.slug = req.query.slug;
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.categorie)
    parametres.id_categorie = parseInt(req.query.categorie);
  if (req.query.icone)
    parametres.id_icone = parseInt(req.query.icone);

  // On fait la recherche dans la base de donnée
  coursDao.getAll(parametres).then(
    (cours: Cours[]) => {
      res.send(cours);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /cours/:id Recupérer un cours
   * @apiVersion 0.1.0
   * @apiName GetUniqueCours
   * @apiGroup Cours
   * 
   * @apiParam {Number} id L'ID du cours
   * 
   * @apiSuccess {Number} id l'ID du cours.
   * @apiSuccess {Number} numero Le numéro du cours
   * @apiSuccess {String} nom Le nom du cours
   * @apiSuccess {String} description La description du cours
   * @apiSuccess {Categorie} categorie La catégorie
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau2",
   *        "nom": "Réseaux2",
   *        "description": "Cours contenant tous ce qui est en lien avec les communications",
   *        "categorie": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: Request, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    coursDao.get(parseInt(req.params.id)).then(
      (cours: Cours) => {
        res.send(cours);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { coursRecuperationRouter };