/**
 * Routes /cours/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { RequeteAuthentifiee } from '../../model/types/requete.authentifiee';
import { CoursDao } from "../../model/dao/cours/cours.dao";
import { Cours } from "../../model/bean/cours/cours";
import { FichiersDao } from '../../model/dao/fichiers/fichiers.dao';
import { CategoriesDao } from '../../model/dao/cours/categories.dao';
import { authRouter } from "../auth.router";

// Création des instances des DAO
const coursDao: CoursDao = new CoursDao();
const categoriesDao: CategoriesDao = new CategoriesDao();
const fichiersDao: FichiersDao = new FichiersDao();

// Création du routeur
const coursManipulationRouter = express.Router();

/**
 * @api {post} /cours Insérer un nouveau cours
 * @apiVersion 0.1.0
 * @apiName PostCours
 * @apiGroup Cours
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données du cours au format JSON
 * @apiParam {String} slug Le slug du cours
 * @apiParam {String} nom Le nom du cours
 * @apiParam {String} description La description du cours
 * @apiParam {Number} categorie L'ID de la catégorie
 * 
 * @apiParamExample {json} Exemple:
 *    HTTP/1.1 200 OK
 *      {
 *        "slug": "reseau2",
 *        "nom": "Réseaux2",
 *        "description": "Cours contenant tous ce qui est en lien avec les communications",
 *        "categorie": 1
 *      }
 * 
 * @apiSuccess {Number} id l'ID du cours.
 * @apiSuccess {Number} numero Le numéro du cours
 * @apiSuccess {String} nom Le nom du cours
 * @apiSuccess {String} description La description du cours
 * @apiSuccess {Categorie} categorie La catégorie
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 2,
 *        "slug": "reseau2",
 *        "nom": "Réseaux2",
 *        "description": "Cours contenant tous ce qui est en lien avec les communications",
 *        "categorie": {
 *          "id": 1,
 *          "slug": "informatique",
 *          "nom": "Informatique",
 *          "description": "Catégorie parente pour le domaine de l'informatique",
 *          "parent": null
 *        }
 *      }
 * 
 * @apiUse ApiErreurs
 */
coursManipulationRouter.post('/', authRouter, async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer le cours
  let cours: Cours = new Cours(-1, req.body.slug, req.body.nom,
    req.body.description, await categoriesDao.get(parseInt(req.body.categorie)));

  // Si l'icône est défini on la récupère et on l'associe
  if (req.body.icone)
    cours.$icone = await fichiersDao.get(parseInt(req.body.icone))

  // On enregistre le cours dans la base de donnée et on la retourne à l'utilisateur
  coursDao.insert(cours).then(
    (coursCree: Cours) => {
      res.send(coursCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /cours Mettre à jour un cours
   * @apiVersion 0.1.0
   * @apiName PutCours
   * @apiGroup Cours
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données du cours au format JSON
   * @apiParam {Number} id L'ID du cours
   * @apiParam {String} slug Le slug du cours
   * @apiParam {String} nom Le nom du cours
   * @apiParam {String} description La description du cours
   * @apiParam {Number} categorie L'ID de la catégorie
   * 
   * @apiParamExample {json} Exemple:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau2",
   *        "nom": "Réseaux2",
   *        "description": "Cours contenant tous ce qui est en lien avec les communications",
   *        "categorie": 1
   *      }
   * 
   * @apiSuccess {Number} id l'ID du cours.
   * @apiSuccess {Number} numero Le numéro du cours
   * @apiSuccess {String} nom Le nom du cours
   * @apiSuccess {String} description La description du cours
   * @apiSuccess {Categorie} categorie La catégorie
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau2",
   *        "nom": "Réseaux2",
   *        "description": "Cours contenant tous ce qui est en lien avec les communications",
   *        "categorie": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', authRouter, async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer le cours
    let cours: Cours = new Cours(parseInt(req.body.id), req.body.slug, req.body.nom,
      req.body.description, await categoriesDao.get(parseInt(req.body.categorie)));

    // Si l'icône est défini on la récupère et on l'associe
    if (req.body.icone)
      cours.$icone = await fichiersDao.get(parseInt(req.body.icone))

    // On met à jour le cours dans la base de donnée et on la retourne à l'utilisateur
    coursDao.update(cours).then(
      (coursModifiee: Cours) => {
        res.send(coursModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /cours Supprimer un cours
   * @apiVersion 0.1.0
   * @apiName DeleteCours
   * @apiGroup Cours
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id L'ID du cours
   * 
   * @apiSuccess {Number} id l'ID du cours.
   * @apiSuccess {Number} numero Le numéro du cours
   * @apiSuccess {String} nom Le nom du cours
   * @apiSuccess {String} description La description du cours
   * @apiSuccess {Categorie} categorie La catégorie
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 2,
   *        "slug": "reseau2",
   *        "nom": "Réseaux2",
   *        "description": "Cours contenant tous ce qui est en lien avec les communications",
   *        "categorie": {
   *          "id": 1,
   *          "slug": "informatique",
   *          "nom": "Informatique",
   *          "description": "Catégorie parente pour le domaine de l'informatique",
   *          "parent": null
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', authRouter, (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le cours dans la base de donnée et on la retourne à l'utilisateur
    coursDao.delete(parseInt(req.query.id)).then(
      (coursSupprimee: Cours) => {
        res.send(coursSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { coursManipulationRouter };