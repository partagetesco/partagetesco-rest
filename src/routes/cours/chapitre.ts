/**
 * Routes /chapitre/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { RequeteAuthentifiee } from '../../model/types/requete.authentifiee';
import { CoursDao } from "../../model/dao/cours/cours.dao";
import { ChapitresDao } from "../../model/dao/cours/chapitres.dao";
import { Chapitre } from "../../model/bean/cours/chapitre";

// Création des instances des DAO
const chapitresDao: ChapitresDao = new ChapitresDao();
const coursDao: CoursDao = new CoursDao();

// Création du routeur
const chapitreRouter = express.Router();

/**
 * @api {post} /chapitre Insérer un nouveau chapitre
 * @apiVersion 0.1.0
 * @apiName PostChapitre
 * @apiGroup Chapitres
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données du chapitre au format JSON
 * @apiParam {Number} numero Le numéro du chapitre
 * @apiParam {String} nom Le nom du chapitre
 * @apiParam {String} description La description du chapitre
 * @apiParam {Number} cours L'ID du cours
 * 
 * @apiParamExample {json} Exemple:
 *    HTTP/1.1 200 OK
 *      {
 *        "numero": 2,
 *        "nom": "Chap. 2",
 *        "description": "Chapitre 2 du cours 2",
 *        "cours": 2
 *      }
 * 
 * @apiSuccess {Number} id l'ID du chapitre.
 * @apiSuccess {Number} numero Le numéro du chapitre
 * @apiSuccess {String} nom Le nom du chapitre
 * @apiSuccess {String} description La description du chapitre
 * @apiSuccess {Categorie} cours Le cours
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 2,
 *        "numero": 2,
 *        "nom": "Chap. 2",
 *        "description": "Chapitre 2 du cours 2",
 *        "cours": {
 *          "id": 2,
 *          "slug": "reseau2",
 *          "nom": "Réseaux2",
 *          "description": "Cours contenant tous ce qui est en lien avec les communications",
 *          "categorie": {
 *            "id": 1,
 *            "slug": "informatique",
 *            "nom": "Informatique",
 *            "description": "Catégorie parente pour le domaine de l'informatique",
 *            "parent": null
 *          }
 *        }
 *      }
 * 
 * @apiUse ApiErreurs
 */
chapitreRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer le chapitre
  let chapitre: Chapitre = new Chapitre(-1, parseInt(req.body.numero), req.body.nom,
    req.body.description, await coursDao.get(parseInt(req.body.cours)));

  // On enregistre le chapitre dans la base de donnée et on la retourne à l'utilisateur
  chapitresDao.insert(chapitre).then(
    (chapitreCree: Chapitre) => {
      res.send(chapitreCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /chapitre Mettre à jour un chapitre
   * @apiVersion 0.1.0
   * @apiName PutChapitre
   * @apiGroup Chapitres
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données du chapitre au format JSON
   * @apiParam {Number} id L'ID du chapitre
   * @apiParam {Number} numero Le numéro du chapitre
   * @apiParam {String} nom Le nom du chapitre
   * @apiParam {String} description La description du chapitre
   * @apiParam {Number} cours L'ID du cours
   * 
   * @apiParamExample {json} Exemple:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 2,
   *        "nom": "Chap. 2",
   *        "description": "Chapitre 2 du cours 2",
   *        "cours": 2
   *      }
   * 
   * @apiSuccess {Number} id l'ID du chapitre.
   * @apiSuccess {Number} numero Le numéro du chapitre
   * @apiSuccess {String} nom Le nom du chapitre
   * @apiSuccess {String} description La description du chapitre
   * @apiSuccess {Categorie} cours Le cours
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 2,
   *        "nom": "Chap. 2",
   *        "description": "Chapitre 2 du cours 2",
   *        "cours": {
   *          "id": 2,
   *          "slug": "reseau2",
   *          "nom": "Réseaux2",
   *          "description": "Cours contenant tous ce qui est en lien avec les communications",
   *          "categorie": {
   *            "id": 1,
   *            "slug": "informatique",
   *            "nom": "Informatique",
   *            "description": "Catégorie parente pour le domaine de l'informatique",
   *            "parent": null
   *          }
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer le chapitre
    let chapitre: Chapitre = new Chapitre(parseInt(req.body.id), parseInt(req.body.numero), req.body.nom,
      req.body.description, await coursDao.get(parseInt(req.body.cours)));

    // On met à jour le chapitre dans la base de donnée et on la retourne à l'utilisateur
    chapitresDao.update(chapitre).then(
      (chapitreModifie: Chapitre) => {
        res.send(chapitreModifie);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /chapitre Supprimer un chapitre
   * @apiVersion 0.1.0
   * @apiName DeleteChapitre
   * @apiGroup Chapitres
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id L'ID du chapitre
   * 
   * @apiSuccess {Number} id l'ID du chapitre.
   * @apiSuccess {Number} numero Le numéro du chapitre
   * @apiSuccess {String} nom Le nom du chapitre
   * @apiSuccess {String} description La description du chapitre
   * @apiSuccess {Categorie} cours Le cours
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 2,
   *        "nom": "Chap. 2",
   *        "description": "Chapitre 2 du cours 2",
   *        "cours": {
   *          "id": 2,
   *          "slug": "reseau2",
   *          "nom": "Réseaux2",
   *          "description": "Cours contenant tous ce qui est en lien avec les communications",
   *          "categorie": {
   *            "id": 1,
   *            "slug": "informatique",
   *            "nom": "Informatique",
   *            "description": "Catégorie parente pour le domaine de l'informatique",
   *            "parent": null
   *          }
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le chapitre dans la base de donnée et on la retourne à l'utilisateur
    chapitresDao.delete(parseInt(req.query.id)).then(
      (chapitreSupprime: Chapitre) => {
        res.send(chapitreSupprime);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { chapitreRouter };