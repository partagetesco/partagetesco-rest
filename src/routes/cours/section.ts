/**
 * Routes /section/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from '../../model/errors/reponse.error';
import { RequeteAuthentifiee } from '../../model/types/requete.authentifiee';
import { Section } from "../../model/bean/cours/section";
import { SectionsDao } from "../../model/dao/cours/sections.dao";
import { ChapitresDao } from "../../model/dao/cours/chapitres.dao";

// Création des instances des DAO
const sectionsDao: SectionsDao = new SectionsDao();
const chapitresDao: ChapitresDao = new ChapitresDao();

// Création du routeur
const sectionRouter = express.Router();

/**
 * @api {post} /section Insérer une nouvelle section
 * @apiVersion 0.1.0
 * @apiName PostSection
 * @apiGroup Sections
 * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données de la section au format JSON
 * @apiParam {Number} numero Le numéro de la section
 * @apiParam {String} nom Le nom de la section
 * @apiParam {String} contenu Le contenu de la section
 * @apiParam {Number} chapitre L'ID du chapitre
 * 
 * @apiParamExample {json} Exemple:
 *    HTTP/1.1 200 OK
 *      {
 *        "numero": 1,
 *        "nom": "Section 1",
 *        "contenu": "<code><script>Hello world !<script><code></script></code>",
 *        "chapitre": 2
 *      }
 * 
 * @apiSuccess {Number} id l'ID de la section.
 * @apiSuccess {Number} numero Le numéro de la section
 * @apiSuccess {String} nom Le nom de la section
 * @apiSuccess {String} contenu Le contenu de la section
 * @apiSuccess {Chapitre} chapitre Le chapitre
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 1,
 *        "numero": 1,
 *        "nom": "Section 1",
 *        "chapitre": {
 *          "id": 2,
 *          "numero": 2,
 *          "nom": "Chap. 2",
 *          "description": "Chapitre 2 du cours 2",
 *          "cours": {
 *            "id": 2,
 *            "slug": "reseau2",
 *            "nom": "Réseaux2",
 *            "description": "Cours contenant tous ce qui est en lien avec les communications",
 *            "categorie": {
 *              "id": 1,
 *              "slug": "informatique",
 *              "nom": "Informatique",
 *              "description": "Catégorie parente pour le domaine de l'informatique",
 *              "parent": null
 *            }
 *          }
 *        },
 *        "contenu": "<code>&lt;script&gt;Hello world !&lt;script&gt;&lt;code&gt;&lt;/script&gt;</code>"
 *      }
 * 
 * @apiUse ApiErreurs
 */
sectionRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer la section
  let section: Section = new Section(-1, parseInt(req.body.numero), req.body.nom,
    req.body.contenu, await chapitresDao.get(parseInt(req.body.chapitre)));

  // On enregistre la section dans la base de donnée et on la retourne à l'utilisateur
  sectionsDao.insert(section).then(
    (sectionCree: Section) => {
      res.send(sectionCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /section Mettre à jour une section
   * @apiVersion 0.1.0
   * @apiName PutSection
   * @apiGroup Sections
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données de la section au format JSON
   * @apiParam {Number} id L'ID de la section
   * @apiParam {Number} numero Le numéro de la section
   * @apiParam {String} nom Le nom de la section
   * @apiParam {String} contenu Le contenu de la section
   * @apiParam {Number} chapitre L'ID du chapitre
   * 
   * @apiParamExample {json} Exemple:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 1,
   *        "nom": "Section 1",
   *        "contenu": "<code><script>Hello world !<script><code></script></code>",
   *        "chapitre": 2
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la section.
   * @apiSuccess {Number} numero Le numéro de la section
   * @apiSuccess {String} nom Le nom de la section
   * @apiSuccess {String} contenu Le contenu de la section
   * @apiSuccess {Chapitre} chapitre Le chapitre
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 1,
   *        "nom": "Section 1",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "contenu": "<code>&lt;script&gt;Hello world !&lt;script&gt;&lt;code&gt;&lt;/script&gt;</code>"
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la section
    let section: Section = new Section(parseInt(req.body.id), parseInt(req.body.numero), req.body.nom,
      req.body.contenu, await chapitresDao.get(parseInt(req.body.chapitre)));

    // On met à jour la section dans la base de donnée et on la retourne à l'utilisateur
    sectionsDao.update(section).then(
      (sectionModifie: Section) => {
        res.send(sectionModifie);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /section Supprimer une section
   * @apiVersion 0.1.0
   * @apiName DeleteSection
   * @apiGroup Sections
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id L'ID de la section
   * 
   * @apiSuccess {Number} id l'ID de la section.
   * @apiSuccess {Number} numero Le numéro de la section
   * @apiSuccess {String} nom Le nom de la section
   * @apiSuccess {String} contenu Le contenu de la section
   * @apiSuccess {Chapitre} chapitre Le chapitre
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "numero": 1,
   *        "nom": "Section 1",
   *        "chapitre": {
   *          "id": 2,
   *          "numero": 2,
   *          "nom": "Chap. 2",
   *          "description": "Chapitre 2 du cours 2",
   *          "cours": {
   *            "id": 2,
   *            "slug": "reseau2",
   *            "nom": "Réseaux2",
   *            "description": "Cours contenant tous ce qui est en lien avec les communications",
   *            "categorie": {
   *              "id": 1,
   *              "slug": "informatique",
   *              "nom": "Informatique",
   *              "description": "Catégorie parente pour le domaine de l'informatique",
   *              "parent": null
   *            }
   *          }
   *        },
   *        "contenu": "<code>&lt;script&gt;Hello world !&lt;script&gt;&lt;code&gt;&lt;/script&gt;</code>"
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime la section dans la base de donnée et on la retourne à l'utilisateur
    sectionsDao.delete(parseInt(req.query.id)).then(
      (sectionSupprime: Section) => {
        res.send(sectionSupprime);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { sectionRouter };