/**
 * Routeurs contrôlant les accès administrateur de niveau 1
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { RequeteAuthentifiee } from "../model/types/requete.authentifiee";
import Fonction from "../model/bean/utilisateurs/fonction";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../model/errors/reponse.error";

// Création des instances des DAO
const adminRouter: express.Router = express.Router();

// Routes d'administration
adminRouter.all('*', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On recupère la fonction de l'utilisateur
    let fonction: Fonction = req.utilisateur.getFonction();

    // Si l'utilisateur est administrateur alors on autorise l'accès
    if (fonction && fonction.getId() == parseInt(process.env.ADMINISTRATEUR_FONCTION_ID))
        next();
    else
        // Sinon on refuse l'accès
        res.status(401).send({ erreur: ReponseError.ERREUR_ACCESS_RESTREINS_ADMIN });
});

export { adminRouter };