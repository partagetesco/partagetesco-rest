/**
 * Routes /types-fichiers/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { TypesFichierDao } from "../../model/dao/fichiers/types.fichier.dao";
import { ParametresRechercheTypesFichier } from "../../model/types/parametres.recherche";
import { TypeFichier } from "../../model/bean/fichiers/type.fichier";

// Création des instances des DAO
const typesFichierDao: TypesFichierDao = new TypesFichierDao();

// Création du routeur
const typesFichierRouter = express.Router();

/**
 * @api {get} /types-fichiers Rechercher des types de fichiers
 * @apiVersion 0.1.0
 * @apiName GetTypesFichiers
 * @apiGroup Types de fichiers
 * 
 * @apiParam {Number} id l'ID du type de fichiers.
 * @apiParam {String} nom Un mot clé recherché dans le nom du type de fichiers
 * @apiParam {String} description Un mot clé recherché dans la description du type de fichiers
 * 
 * @apiSuccess {Number} id l'ID du type de fichiers.
 * @apiSuccess {String} nom Le nom du type de fichiers
 * @apiSuccess {String} description La description du type de fichiers
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "nom": "JPEG",
 *        "description": "Format d'image avec une palette de couleur très vaste."
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
typesFichierRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheTypesFichier = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;

  // On fait la recherche dans la base de donnée
  typesFichierDao.getAll(parametres).then(
    (types: TypeFichier[]) => {
      res.send(types);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /types-fichiers/:id Recupérer un type de fichiers
   * @apiVersion 0.1.0
   * @apiName GetTypeFichiers
   * @apiGroup Types de fichiers
   * 
   * @apiParam {Number} id l'ID du type de fichiers.
   * 
   * @apiSuccess {Number} id l'ID du type de fichiers.
   * @apiSuccess {String} nom Le nom du type de fichiers
   * @apiSuccess {String} description La description du type de fichiers
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *    [
   *      {
   *        "id": 1,
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   *    ]
   * 
   * @apiUse ApiErreurs
   */
  .get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    typesFichierDao.get(parseInt(req.params.id)).then(
      (type: TypeFichier) => {
        res.send(type);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { typesFichierRouter };