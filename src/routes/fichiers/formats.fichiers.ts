/**
 * Routes /formats-fichiers/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { FormatsFichierDao } from "../../model/dao/fichiers/formats.fichier.dao";
import { FormatFichier } from "../../model/bean/fichiers/format.fichier";
import { ParametresRechercheFormatsFichier } from "../../model/types/parametres.recherche";
import { TypesFichierDao } from "../../model/dao/fichiers/types.fichier.dao";

// Création des instances des DAO
const formatsFichierDao: FormatsFichierDao = new FormatsFichierDao();

// Création du routeur
const formatsFichiersRouter = express.Router();

/**
 * @api {get} /formats-fichiers Rechercher des formats de fichiers
 * @apiVersion 0.1.0
 * @apiName GetFormatsFichiers
 * @apiGroup Format de fichiers
 * 
 * @apiParam {Number} id l'ID du format de fichiers.
 * @apiParam {String} nom Un mot clé recherché dans le nom du format de fichiers
 * @apiParam {String} description Un mot clé recherché dans la description du format de fichiers
 * @apiParam {Number} type l'ID du type
 * 
 * @apiSuccess {Number} id l'ID du format de fichiers.
 * @apiSuccess {String} nom Le nom du format de fichiers
 * @apiSuccess {String} description La description du format de fichiers
 * @apiSuccess {Type} type Le type de fichier
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 1,
 *        "type": {
 *          "id": 1,
 *          "nom": "Image",
 *          "description": "Une image."
 *        },
 *        "nom": "JPEG",
 *        "description": "Format d'image avec une palette de couleur très vaste."
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
formatsFichiersRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheFormatsFichier = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.type)
    parametres.id_type_fichier = req.query.type;

  // On fait la recherche dans la base de donnée
  formatsFichierDao.getAll(parametres).then(
    (formats: FormatFichier[]) => {
      res.send(formats);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

  /**
   * @api {get} /formats-fichiers/:id Récupérer un format de fichiers
   * @apiVersion 0.1.0
   * @apiName GetFormatFichiers
   * @apiGroup Format de fichiers
   * 
   * @apiParam {Number} id l'ID du format de fichiers.
   * 
   * @apiSuccess {Number} id l'ID du format de fichiers.
   * @apiSuccess {String} nom Le nom du format de fichiers
   * @apiSuccess {String} description La description du format de fichiers
   * @apiSuccess {Type} type Le type de fichier
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "type": {
   *          "id": 1,
   *          "nom": "Image",
   *          "description": "Une image."
   *        },
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    formatsFichierDao.get(parseInt(req.params.id)).then(
      (format: FormatFichier) => {
        res.send(format);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { formatsFichiersRouter };