/**
 * Routes /fichiers/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { FichiersDao } from "../../model/dao/fichiers/fichiers.dao";
import { Fichier } from "../../model/bean/fichiers/fichier";
import { ParametresRechercheFichiers } from "../../model/types/parametres.recherche";

// Création des instances des DAO
const fichiersDao: FichiersDao = new FichiersDao();

// Création du routeur
const fichiersRouter = express.Router();

/**
 * @api {get} /fichiers Rechercher des fichiers
 * @apiVersion 0.1.0
 * @apiName GetFichiers
 * @apiGroup Fichiers
 * 
 * @apiParam {Number} id l'ID du fichier.
 * @apiParam {String} nom Un mot clé recherché dans le nom du fichier
 * @apiParam {String} description Un mot clé recherché dans la description du fichier
 * @apiParam {Number} format l'ID du format.
 * @apiParam {Number} licence l'ID du fichier.
 * 
 * @apiSuccess {Number} id l'ID du fichier.
 * @apiSuccess {String} nom Le nom du fichier
 * @apiSuccess {String} description La description du fichier
 * @apiSuccess {Format} format Le format.
 * @apiSuccess {Licence} licence Le fichier.
 * @apiSuccess {Buffer} donnees Les données du fichier (Buffer JavaScript au format JSON).
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 11,
 *        "format": {
 *          "id": 1,
 *          "type": {
 *            "id": 1,
 *            "nom": "Image",
 *            "description": "Une image."
 *          },
 *          "nom": "JPEG",
 *          "description": "Format d'image avec une palette de couleur trÃ¨s vaste."
 *        },
 *        "licence": {
 *          "id": 3,
 *          "url": "https://opensource.org/licenses/mit-license.php",
 *          "nom": "MIT",
 *          "version": "v1.0",
 *          "description": "Licence MIT."
 *        },
 *        "propietaire": {
 *          "id": 3,
 *          "pseudo": "developpix",
 *          "mail": "developpix@gmail.com",
 *          "biographie": ""
 *        },
 *        "nom": "nom.txt",
 *        "description": "Une description",
 *        "donnees": {
 *          "type": "Buffer",
 *          "data": [ 80, 75, 42, 110 ],
 *        }
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
fichiersRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheFichiers = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.format)
    parametres.id_format = req.query.format;
  if (req.query.licence)
    parametres.id_licence = req.query.licence;
  if (req.query.utilisateur)
    parametres.id_utilisateur = req.query.utilisateur;

  // On fait la recherche dans la base de donnée
  fichiersDao.getAll(parametres).then(
    (fichiers: Fichier[]) => {
      // On charge le contenu des fichiers
      fichiers.forEach(fichier => fichier.lire());

      res.send(fichiers);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})

/**
 * @api {get} /fichiers/:id Recupérer un fichier
 * @apiVersion 0.1.0
 * @apiName GetFichier
 * @apiGroup Fichiers
 * 
 * @apiParam {Number} id l'ID du fichier.
 * 
 * @apiSuccess {Number} id l'ID du fichier.
 * @apiSuccess {String} nom Le nom du fichier
 * @apiSuccess {String} description La description du fichier
 * @apiSuccess {Format} format Le format.
 * @apiSuccess {Licence} licence Le fichier.
 * @apiSuccess {Buffer} donnees Les données du fichier (Buffer JavaScript au format JSON).
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 11,
 *        "format": {
 *          "id": 1,
 *          "type": {
 *            "id": 1,
 *            "nom": "Image",
 *            "description": "Une image."
 *          },
 *          "nom": "JPEG",
 *          "description": "Format d'image avec une palette de couleur trÃ¨s vaste."
 *        },
 *        "licence": {
 *          "id": 3,
 *          "url": "https://opensource.org/licenses/mit-license.php",
 *          "nom": "MIT",
 *          "version": "v1.0",
 *          "description": "Licence MIT."
 *        },
 *        "propietaire": {
 *          "id": 3,
 *          "pseudo": "developpix",
 *          "mail": "developpix@gmail.com",
 *          "biographie": ""
 *        },
 *        "nom": "nom.txt",
 *        "description": "Une description",
 *        "donnees": {
 *          "type": "Buffer",
 *          "data": [ 80, 75, 42, 110 ],
 *        }
 *      }
 * 
 * @apiUse ApiErreurs
 */
fichiersRouter.get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On fait la recherche dans la base de donnée
  fichiersDao.get(parseInt(req.params.id)).then(
    (fichier: Fichier) => {
      // On charge le contenu du fichier
      fichier.lire();

      res.send(fichier);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
});

export { fichiersRouter };