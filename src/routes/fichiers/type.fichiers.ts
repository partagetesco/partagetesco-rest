/**
 * Routes /type-fichier/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { TypesFichierDao } from "../../model/dao/fichiers/types.fichier.dao";
import { TypeFichier } from "../../model/bean/fichiers/type.fichier";

// Création des instances des DAO
const typesFichierDao: TypesFichierDao = new TypesFichierDao();

// Création du routeur
const typeFichierRouter = express.Router();

/**
 * @api {post} /type-fichiers Insérer un type de fichiers
 * @apiVersion 0.1.0
 * @apiName PostTypeFichiers
 * @apiGroup Types de fichiers
   * @apiPermission utilisateurs
 * 
 * @apiParam {json} body Les données du type de fichiers au format JSON.
 * @apiParam {String} nom Le nom du type de fichiers
 * @apiParam {String} description La description du type de fichiers
 * 
 * @apiParamExample {json} Exemple:
 *    HTTP/1.1 200 OK
 *      {
 *        "nom": "JPEG",
 *        "description": "Format d'image avec une palette de couleur très vaste."
 *      }
 * 
 * @apiSuccess {Number} id l'ID du type de fichiers.
 * @apiSuccess {String} nom Le nom du type de fichiers
 * @apiSuccess {String} description La description du type de fichiers
 * 
 * @apiSuccessExample Success-Response:
 *      {
 *        "id": 1,
 *        "nom": "JPEG",
 *        "description": "Format d'image avec une palette de couleur très vaste."
 *      }
 * 
 * @apiUse ApiErreurs
 */
typeFichierRouter
  .post('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la type
    let type: TypeFichier = new TypeFichier(-1, req.body.nom, req.body.description);

    // On enregistre la type dans la base de donnée et on la retourne à l'utilisateur
    typesFichierDao.insert(type).then(
      (typeCree: TypeFichier) => {
        res.send(typeCree);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {put} /type-fichiers Mettre à jour un type de fichiers
   * @apiVersion 0.1.0
   * @apiName PutTypeFichiers
   * @apiGroup Types de fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données du type de fichiers au format JSON.
   * @apiParam {Number} id l'ID du type de fichiers.
   * @apiParam {String} nom Le nom du type de fichiers
   * @apiParam {String} description La description du type de fichiers
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "id": 1,
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiSuccess {Number} id l'ID du type de fichiers.
   * @apiSuccess {String} nom Le nom du type de fichiers
   * @apiSuccess {String} description La description du type de fichiers
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la type
    let type: TypeFichier = new TypeFichier(parseInt(req.body.id), req.body.nom, req.body.description);

    // On met à jour la type dans la base de donnée et on la retourne à l'utilisateur
    typesFichierDao.update(type).then(
      (typeModifiee: TypeFichier) => {
        res.send(typeModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /type-fichiers Supprimer un type de fichiers
   * @apiVersion 0.1.0
   * @apiName DeleteTypeFichiers
   * @apiGroup Types de fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID du type de fichiers.
   * 
   * @apiSuccess {Number} id l'ID du type de fichiers.
   * @apiSuccess {String} nom Le nom du type de fichiers
   * @apiSuccess {String} description La description du type de fichiers
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime la type dans la base de donnée et on la retourne à l'utilisateur
    typesFichierDao.delete(parseInt(req.query.id)).then(
      (typeSupprimee: TypeFichier) => {
        res.send(typeSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { typeFichierRouter };