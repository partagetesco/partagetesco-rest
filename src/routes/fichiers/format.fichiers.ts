/**
 * Routes /format-fichiers/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { FormatsFichierDao } from "../../model/dao/fichiers/formats.fichier.dao";
import { FormatFichier } from "../../model/bean/fichiers/format.fichier";
import { TypesFichierDao } from "../../model/dao/fichiers/types.fichier.dao";

// Création des instances des DAO
const formatsFichierDao: FormatsFichierDao = new FormatsFichierDao();
const typesFichierDao: TypesFichierDao = new TypesFichierDao();

// Création du routeur
const formatFichiersRouter = express.Router();

  /**
   * @api {post} /format-fichiers Rechercher un format de fichiers
   * @apiVersion 0.1.0
   * @apiName PostFormatFichiers
   * @apiGroup Format de fichiers
   * @apiPermission utilisateurs
   * 
   * @apiParam {json} body Les données du format de fichiers au format JSON
   * @apiParam {String} nom Le nom du format de fichiers
   * @apiParam {String} description La description du format de fichiers
   * @apiParam {Number} type l'ID du type
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste.",
   *        "type": 1
   *      }
   * 
   * @apiSuccess {Number} id l'ID du format de fichiers.
   * @apiSuccess {String} nom Le nom du format de fichiers
   * @apiSuccess {String} description La description du format de fichiers
   * @apiSuccess {Type} type Le type de fichier
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "type": {
   *          "id": 1,
   *          "nom": "Image",
   *          "description": "Une image."
   *        },
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
formatFichiersRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la format
    let format: FormatFichier = new FormatFichier(-1, req.body.nom,
      req.body.description, await typesFichierDao.get(parseInt(req.body.type)));

    // On enregistre la format dans la base de donnée et on la retourne à l'utilisateur
    formatsFichierDao.insert(format).then(
      (formatCree: FormatFichier) => {
        res.send(formatCree);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {put} /format-fichiers Mettre à jour un format de fichiers
   * @apiVersion 0.1.0
   * @apiName PutFormatFichiers
   * @apiGroup Format de fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données du format de fichiers au format JSON
   * @apiParam {Number} id l'ID du format de fichiers.
   * @apiParam {String} nom Le nom du format de fichiers
   * @apiParam {String} description La description du format de fichiers
   * @apiParam {Number} type l'ID du type
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "id": 1,
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste.",
   *        "type": 1
   *      }
   * 
   * @apiSuccess {Number} id l'ID du format de fichiers.
   * @apiSuccess {String} nom Le nom du format de fichiers
   * @apiSuccess {String} description La description du format de fichiers
   * @apiSuccess {Type} type Le type de fichier
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "type": {
   *          "id": 1,
   *          "nom": "Image",
   *          "description": "Une image."
   *        },
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la format
    let format: FormatFichier = new FormatFichier(parseInt(req.body.id), req.body.nom,
    req.body.description, await typesFichierDao.get(parseInt(req.body.type)));

    // On met à jour le format dans la base de donnée et on la retourne à l'utilisateur
    formatsFichierDao.update(format).then(
      (formatModifiee: FormatFichier) => {
        res.send(formatModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {delete} /format-fichiers Supprimer un format de fichiers
   * @apiVersion 0.1.0
   * @apiName DeleteFormatFichiers
   * @apiGroup Format de fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID du format de fichiers.
   * 
   * @apiSuccess {Number} id l'ID du format de fichiers.
   * @apiSuccess {String} nom Le nom du format de fichiers
   * @apiSuccess {String} description La description du format de fichiers
   * @apiSuccess {Type} type Le type de fichier
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 1,
   *        "type": {
   *          "id": 1,
   *          "nom": "Image",
   *          "description": "Une image."
   *        },
   *        "nom": "JPEG",
   *        "description": "Format d'image avec une palette de couleur très vaste."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le format dans la base de donnée et on la retourne à l'utilisateur
    formatsFichierDao.delete(parseInt(req.query.id)).then(
      (formatSupprimee: FormatFichier) => {
        res.send(formatSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { formatFichiersRouter };