/**
 * Routes /licences/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { LicencesDao } from "../../model/dao/fichiers/licences.dao";
import { Licence } from "../../model/bean/fichiers/licence";
import { ParametresRechercheLicences } from "../../model/types/parametres.recherche";

// Création des instances des DAO
const licencesDao: LicencesDao = new LicencesDao();

// Création du routeur
const licencesRouter = express.Router();

/**
 * @api {get} /licences Rechercher des licences
 * @apiVersion 0.1.0
 * @apiName GetLicences
 * @apiGroup Licences
 * 
 * @apiParam {Number} id l'ID de la licence.
 * @apiParam {String} nom Un mot clé recherché dans le nom de la licence
 * @apiParam {String} version Un mot clé recherché dans la version de la licence
 * @apiParam {String} description Un mot clé recherché dans la description de la licence
 * @apiParam {String} url L'URL vers le texte de la licence
 * 
 * @apiSuccess {Number} id l'ID de la licence.
 * @apiSuccess {String} nom Le nom de la licence
 * @apiSuccess {String} version La version de la licence
 * @apiSuccess {String} description La description de la licence
 * @apiSuccess {String} url L'URL vers le texte de la licence
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "id": 3,
 *        "url": "https://opensource.org/licenses/mit-license.php",
 *        "nom": "MIT",
 *        "version": "v1.0",
 *        "description": "Licence MIT."
 *      }
 *    ]
 * 
 * @apiUse ApiErreurs
 */
licencesRouter.get('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer l'objet qui contient les paramètres
  let parametres: ParametresRechercheLicences = {};

  // On défini les paramètres de recherche
  if (req.query.id)
    parametres.id = parseInt(req.query.id);
  if (req.query.nom)
    parametres.nom = req.query.nom;
  if (req.query.version)
    parametres.version = req.query.version;
  if (req.query.description)
    parametres.description = req.query.description;
  if (req.query.url)
    parametres.url = req.query.url;

  // On fait la recherche dans la base de donnée
  licencesDao.getAll(parametres).then(
    (licences: Licence[]) => {
      res.send(licences);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
    });
})
  /**
   * @api {get} /licences/:id Recupérer une licence
   * @apiVersion 0.1.0
   * @apiName GetLicence
   * @apiGroup Licences
   * 
   * @apiParam {Number} id l'ID de la licence.
   * 
   * @apiSuccess {Number} id l'ID de la licence.
   * @apiSuccess {String} nom Le nom de la licence
   * @apiSuccess {String} version La version de la licence
   * @apiSuccess {String} description La description de la licence
   * @apiSuccess {String} url L'URL vers le texte de la licence
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 3,
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .get('/:id', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On fait la recherche dans la base de donnée
    licencesDao.get(parseInt(req.params.id)).then(
      (licence: Licence) => {
        res.send(licence);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE });
      });
  });

export { licencesRouter };