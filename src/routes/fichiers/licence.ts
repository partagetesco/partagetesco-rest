/**
 * Routes /licence/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { LicencesDao } from "../../model/dao/fichiers/licences.dao";
import { Licence } from "../../model/bean/fichiers/licence";

// Création des instances des DAO
const licencesDao: LicencesDao = new LicencesDao();

// Création du routeur
const licenceRouter = express.Router();

  /**
   * @api {post} /licence Insérer une licence
   * @apiVersion 0.1.0
   * @apiName PostLicence
   * @apiGroup Licences
   * @apiPermission utilisateurs
   * 
   * @apiParam {json} body Les données de la licence au format JSON.
   * @apiParam {String} nom Le nom de la licence
   * @apiParam {String} version La version de la licence
   * @apiParam {String} description La description de la licence
   * @apiParam {String} url L'URL vers le texte de la licence
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la licence.
   * @apiSuccess {String} nom Le nom de la licence
   * @apiSuccess {String} version La version de la licence
   * @apiSuccess {String} description La description de la licence
   * @apiSuccess {String} url L'URL vers le texte de la licence
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 3,
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiUse ApiErreurs
   */
licenceRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la licence
    let licence: Licence = new Licence(-1, req.body.nom, req.body.version,
      req.body.description, new URL(req.body.url));

    // On enregistre la licence dans la base de donnée et on la retourne à l'utilisateur
    licencesDao.insert(licence).then(
      (licenceCree: Licence) => {
        res.send(licenceCree);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {put} /licence Mettre à jour une licence
   * @apiVersion 0.1.0
   * @apiName PutLicence
   * @apiGroup Licences
   * @apiPermission proprietaire
   * 
   * @apiParam {json} body Les données de la licence au format JSON.
   * @apiParam {Number} id l'ID de la licence.
   * @apiParam {String} nom Le nom de la licence
   * @apiParam {String} version La version de la licence
   * @apiParam {String} description La description de la licence
   * @apiParam {String} url L'URL vers le texte de la licence
   * 
   * @apiParamExample {json} Exemple:
   *      {
   *        "id": 3,
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiSuccess {Number} id l'ID de la licence.
   * @apiSuccess {String} nom Le nom de la licence
   * @apiSuccess {String} version La version de la licence
   * @apiSuccess {String} description La description de la licence
   * @apiSuccess {String} url L'URL vers le texte de la licence
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 3,
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer la licence
    let licence: Licence = new Licence(parseInt(req.body.id), req.body.nom,
      req.body.version, req.body.description, new URL(req.body.url));

    // On met à jour le licence dans la base de donnée et on la retourne à l'utilisateur
    licencesDao.update(licence).then(
      (licenceModifiee: Licence) => {
        res.send(licenceModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })
  
  /**
   * @api {delete} /licence Supprimer une licence
   * @apiVersion 0.1.0
   * @apiName DeleteLicence
   * @apiGroup Licences
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID de la licence.
   * 
   * @apiSuccess {Number} id l'ID de la licence.
   * @apiSuccess {String} nom Le nom de la licence
   * @apiSuccess {String} version La version de la licence
   * @apiSuccess {String} description La description de la licence
   * @apiSuccess {String} url L'URL vers le texte de la licence
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 3,
   *        "url": "https://opensource.org/licenses/mit-license.php",
   *        "nom": "MIT",
   *        "version": "v1.0",
   *        "description": "Licence MIT."
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le licence dans la base de donnée et on la retourne à l'utilisateur
    licencesDao.delete(parseInt(req.query.id)).then(
      (licenceSupprimee: Licence) => {
        res.send(licenceSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { licenceRouter };