/**
 * Routes /fichier/*
 * 
 * @author Developpix
 * @version 0.1
 */
import express, { NextFunction } from "express";
import { Response } from "express-serve-static-core";
import { ReponseError } from "../../model/errors/reponse.error";
import { RequeteAuthentifiee } from "../../model/types/requete.authentifiee";
import { FichiersDao } from "../../model/dao/fichiers/fichiers.dao";
import { FormatsFichierDao } from "../../model/dao/fichiers/formats.fichier.dao";
import { Fichier } from "../../model/bean/fichiers/fichier";
import { LicencesDao } from "../../model/dao/fichiers/licences.dao";

// Création des instances des DAO
const fichiersDao: FichiersDao = new FichiersDao();
const formatsFichierDao: FormatsFichierDao = new FormatsFichierDao();
const licencesDao: LicencesDao = new LicencesDao();

// Création du routeur
const fichierRouter = express.Router();

/**
 * @api {post} /fichier Insérer un fichier
 * @apiVersion 0.1.0
 * @apiName PostFichier
 * @apiGroup Fichiers
 * @apiPermission utilisateurs
 * 
 * @apiParam {multipart/form-data} body Les données du fichier au format multipart/form-data (contenant les données ci-dessous).
 * @apiParam {String} nom Le nom du fichier
 * @apiParam {File} fichier Le fichier
 * @apiParam {String} description La description du fichier
 * @apiParam {Number} format l'ID du format.
 * @apiParam {Number} licence l'ID du fichier.
 * 
 * @apiSuccess {Number} id l'ID du fichier.
 * @apiSuccess {String} nom Le nom du fichier
 * @apiSuccess {String} description La description du fichier
 * @apiSuccess {Format} format Le format.
 * @apiSuccess {Licence} licence Le fichier.
 * @apiSuccess {Buffer} donnees Les données du fichier (Buffer JavaScript au format JSON).
 * 
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *      {
 *        "id": 11,
 *        "format": {
 *          "id": 1,
 *          "type": {
 *            "id": 1,
 *            "nom": "Image",
 *            "description": "Une image."
 *          },
 *          "nom": "JPEG",
 *          "description": "Format d'image avec une palette de couleur trÃ¨s vaste."
 *        },
 *        "licence": {
 *          "id": 3,
 *          "url": "https://opensource.org/licenses/mit-license.php",
 *          "nom": "MIT",
 *          "version": "v1.0",
 *          "description": "Licence MIT."
 *        },
 *        "propietaire": {
 *          "id": 3,
 *          "pseudo": "developpix",
 *          "mail": "developpix@gmail.com",
 *          "biographie": ""
 *        },
 *        "nom": "nom.txt",
 *        "description": "Une description",
 *        "donnees": {
 *          "type": "Buffer",
 *          "data": [ 80, 75, 42, 110 ],
 *        }
 *      }
 * 
 * @apiUse ApiErreurs
 */
fichierRouter.post('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
  // On créer le fichier
  let fichier: Fichier = new Fichier(-1, req.body.nom, req.body.description,
    await formatsFichierDao.get(parseInt(req.body.format)),
    await licencesDao.get(parseInt(req.body.licence)),
    req.utilisateur);

  // On enregistre le fichier dans la base de donnée et on la retourne à l'utilisateur
  fichiersDao.insert(fichier).then(
    (fichierCree: Fichier) => {
      // On enregistre uniquement le premier fichier reçu
      fichierCree.enregistrer(req.files[0].buffer);

      // On retourne le fichier
      res.send(fichierCree);
    },
    (err: any) => {
      // TODO
      console.log(err);

      res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
    })
})

  /**
   * @api {put} /fichier Mettre à jour un fichier
   * @apiVersion 0.1.0
   * @apiName PutFichier
   * @apiGroup Fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {multipart/form-data} body Les données du fichier au format multipart/form-data (contenant les données ci-dessous).
   * @apiParam {Number} id l'ID du fichier.
   * @apiParam {String} nom Le nom du fichier
   * @apiParam {File} fichier Le fichier
   * @apiParam {String} description La description du fichier
   * @apiParam {Number} format l'ID du format.
   * @apiParam {Number} licence l'ID du fichier.
   * 
   * @apiSuccess {Number} id l'ID du fichier.
   * @apiSuccess {String} nom Le nom du fichier
   * @apiSuccess {String} description La description du fichier
   * @apiSuccess {Format} format Le format.
   * @apiSuccess {Licence} licence Le fichier.
   * @apiSuccess {Buffer} donnees Les données du fichier (Buffer JavaScript au format JSON).
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 11,
   *        "format": {
   *          "id": 1,
   *          "type": {
   *            "id": 1,
   *            "nom": "Image",
   *            "description": "Une image."
   *          },
   *          "nom": "JPEG",
   *          "description": "Format d'image avec une palette de couleur trÃ¨s vaste."
   *        },
   *        "licence": {
   *          "id": 3,
   *          "url": "https://opensource.org/licenses/mit-license.php",
   *          "nom": "MIT",
   *          "version": "v1.0",
   *          "description": "Licence MIT."
   *        },
   *        "propietaire": {
   *          "id": 3,
   *          "pseudo": "developpix",
   *          "mail": "developpix@gmail.com",
   *          "biographie": ""
   *        },
   *        "nom": "nom.txt",
   *        "description": "Une description",
   *        "donnees": {
   *          "type": "Buffer",
   *          "data": [ 80, 75, 42, 110 ],
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .put('/', async (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On créer le fichier
    let fichier: Fichier = new Fichier(parseInt(req.body.id), req.body.nom, req.body.description,
      await formatsFichierDao.get(parseInt(req.body.format)),
      await licencesDao.get(parseInt(req.body.licence)),
      req.utilisateur);

    // On met à jour le fichier dans la base de donnée et on la retourne à l'utilisateur
    fichiersDao.update(fichier).then(
      (fichierModifiee: Fichier) => {
        // Si la requête contient un fichier on met à jour le contenu en mémoire
        if (req.files[0])
          fichier.enregistrer(req.files[0].buffer);

        // On retourne le fichier
        res.send(fichierModifiee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  })

  /**
   * @api {delete} /fichier Supprimer un fichier
   * @apiVersion 0.1.0
   * @apiName DeleteFichier
   * @apiGroup Fichiers
   * @apiPermission proprietaire
   * 
   * @apiParam {Number} id l'ID du fichier.
   * 
   * @apiSuccess {Number} id l'ID du fichier.
   * @apiSuccess {String} nom Le nom du fichier
   * @apiSuccess {String} description La description du fichier
   * @apiSuccess {Format} format Le format.
   * @apiSuccess {Licence} licence Le fichier.
   * @apiSuccess {Buffer} donnees Les données du fichier (Buffer JavaScript au format JSON).
   * 
   * @apiSuccessExample Success-Response:
   *    HTTP/1.1 200 OK
   *      {
   *        "id": 11,
   *        "format": {
   *          "id": 1,
   *          "type": {
   *            "id": 1,
   *            "nom": "Image",
   *            "description": "Une image."
   *          },
   *          "nom": "JPEG",
   *          "description": "Format d'image avec une palette de couleur trÃ¨s vaste."
   *        },
   *        "licence": {
   *          "id": 3,
   *          "url": "https://opensource.org/licenses/mit-license.php",
   *          "nom": "MIT",
   *          "version": "v1.0",
   *          "description": "Licence MIT."
   *        },
   *        "propietaire": {
   *          "id": 3,
   *          "pseudo": "developpix",
   *          "mail": "developpix@gmail.com",
   *          "biographie": ""
   *        },
   *        "nom": "nom.txt",
   *        "description": "Une description",
   *        "donnees": {
   *          "type": "Buffer",
   *          "data": [ 80, 75, 42, 110 ],
   *        }
   *      }
   * 
   * @apiUse ApiErreurs
   */
  .delete('/', (req: RequeteAuthentifiee, res: Response, next: NextFunction) => {
    // On supprime le fichier dans la base de donnée et on la retourne à l'utilisateur
    fichiersDao.delete(parseInt(req.query.id)).then(
      (fichierSupprimee: Fichier) => {
        // On charge le contenu du fichier
        fichierSupprimee.lire();

        // On supprime le fichier en mémoire
        fichierSupprimee.supprimer();

        res.send(fichierSupprimee);
      },
      (err: any) => {
        // TODO
        console.log(err);

        res.send({ erreur: ReponseError.ERREUR_INATTENDUE })
      })
  });

export { fichierRouter };