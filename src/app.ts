import express from "express";
import path from "path";
import logger from "morgan";
import { Request, Response } from "express-serve-static-core";
import timeout from "connect-timeout";
import multer from "multer";
import cookieParser from "cookie-parser";
import { rootRouter } from "./routes/rootRouter";

// On créer une instance de ExpressJS
const app = express();
// On créer une instance de Multer
const upload = multer();

// Estimation de l'URL de base de l'API
let baseUrl: string = (process.env.BASE_URL || '/api');
if (baseUrl.charAt(baseUrl.length - 1) === '/')
    baseUrl = baseUrl.slice(0, baseUrl.length - 1);

app.use(timeout('5s'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// Utilisation des cookies avec signature
app.use(cookieParser(process.env.COOKIE_SECRET));
// Utilisation du Middleware pour la récupération des fichiers
app.use(upload.any());

// On rend accessible les fichiers de ApiDoc
app.use(`${baseUrl}/api-documentation`, express.static(path.join(`${__dirname}/../../apidoc`)))
    // On rend accessible les fichiers de TypeDoc
    .use(`${baseUrl}/documentation`, express.static(path.join(`${__dirname}/../../docs`)))

    // On créer un handler pour définir les réponses en tant que JSON encodé en UTF-8
    .use((req: Request, res: Response, next: () => void) => {
        // Toutes les routes retournes des objets JSON encodé en UTF-8
        res.setHeader('Content-Type', 'application/json; charset=utf-8');
        next();
    })

    // Handler des erreurs
    .use((err: any, req: Request, res: Response, next: () => void) => {
        if (err.code === 'ETIMEDOUT') {
            res.status(524).send();
        } else if (err) {
            res.send(err);
        } else {
            next();
        }
    })

    // On défini les routes
    .use(`${baseUrl}`, rootRouter)

    // Si aucune routes on redirige vers l'index
    .use('/', (req: Request, res: Response) => {
        res.redirect(`${baseUrl}/api-documentation`);
    });

/**
 * @apiDefine utilisateurs Les utilisateurs
 *  Les utilisateurs authentifiés à l'aide d'un token.
 */
/**
 * @apiDefine proprietaire Le propriétaire
 *  L'utilisateur qui l'a créé.
 */
/**
 * @apiDefine admin Administrateur
 *  Un utilisateur en charge de l'administration de l'application.
 */
/**
 * @apiDefine modo Modérateur
 *  Un utilisateur en charge de l'administration d'une partie l'application.
 */
export { app };
