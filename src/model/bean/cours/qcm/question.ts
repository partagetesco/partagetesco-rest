import EmptyStringError from "../../../errors/empty.string.error";
import { Chapitre } from "../chapitre";
import { QCM } from "./qcm";

/**
 * Classe représentant une question
 * 
 * @author Developpix
 * @version 0.1
 */
export class Question {
    /**
     * l'id de la question
     */
    private id: number;
    /**
     * La question
     */
    private question: string;
    /**
     * Le QCM associé
     */
    private qcm: QCM;

    /**
     * Constructeur de la classe
     * @param $id l'ID
     * @param $question la question
     * @param $qcm le qcm associé
     */
    constructor($id: number, $question: string, $qcm: QCM) {
        this.id = $id;
        this.qcm = $qcm;

        // Si la question est vide ou null on lève une exception
        if ($question === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE);
        else
            // Sinon on met à jour la valeur
            this.question = $question;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $question
     * @return {string}
     */
    public get $question(): string {
        return this.question;
    }

    /**
     * Getter $qcm
     * @return {QCM}
     */
    public get $qcm(): QCM {
        return this.qcm;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $question
     * @param {string} value
     */
    public set $question(value: string) {
        // Si la question est vide ou null on lève une exception
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE);
        else
            // Sinon on met à jour la valeur
            this.question = value;
    }

    /**
     * Setter $qcm
     * @param {QCM} value
     */
    public set $qcm(value: QCM) {
        this.qcm = value;
    }
}