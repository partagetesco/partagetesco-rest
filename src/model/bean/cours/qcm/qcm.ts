import EmptyStringError from "../../../errors/empty.string.error";
import { Chapitre } from "../chapitre";

/**
 * Classe représentant un QCM
 * 
 * @author Developpix
 * @version 0.1
 */
export class QCM {
    /**
     * l'id du QCM
     */
    private id: number;
    /**
     * Le slug du QCM
     */
    private slug: string;
    /**
     * Le nom du QCM
     */
    private nom: string;
    /**
     * Le chapitre associé
     */
    private chapitre: Chapitre;

    /**
     * Constructeur de la classe
     * @param id l'ID
     * @param slug le slug
     * @param nom le nom
     * @param chapitre le chapitre associé
     */
    constructor(id: number, slug: string, nom: string, chapitre: Chapitre) {
        this.id = id;
        this.slug = slug;
        this.nom = nom;
        this.chapitre = chapitre;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $slug
     * @return {string}
     */
    public get $slug(): string {
        return this.slug;
    }

    /**
     * Getter $nom
     * @return {string}
     */
    public get $nom(): string {
        return this.nom;
    }
    
    /**
     * Getter $chapitre
     * @return {Chapitre}
     */
    public get $chapitre(): Chapitre {
        return this.chapitre;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $slug
     * @param {string} value
     */
    public set $slug(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_SLUG_VIDE)
        else
            this.slug = value;
    }

    /**
     * Setter $nom
     * @param {string} value
     */
    public set $nom(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE)
        else
            this.nom = value;
    }

    /**
     * Setter $chapitre
     * @param {Chapitre} value
     */
    public set $chapitre(value: Chapitre) {
        this.chapitre = value;
    }

}