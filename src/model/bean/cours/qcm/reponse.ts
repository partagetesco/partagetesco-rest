import EmptyStringError from "../../../errors/empty.string.error";
import { Question } from "./question";

/**
 * Classe représentant une réponse
 * 
 * @author Developpix
 * @version 0.1
 */
export class Reponse {
    /**
     * l'id de la réponse
     */
    private id: number;
    /**
     * La réponse
     */
    private reponse: string;
    /**
     * Un booléen indiquant si la réponse est valide ou non
     */
    private valide: boolean;
    /**
     * La question associée
     */
    private question: Question;

    /**
     * Constructeur de la classe
     * @param id l'ID
     * @param reponse la réponse
     * @param valide booléen indiquant si la réponse est valide ou non
     * @param question la question associée
     */
    constructor(id: number, reponse: string, valide: boolean, question: Question) {
        this.id = id;
        this.valide = valide;
        this.question = question;

        // Si la réponse est vide ou null on lève une exception
        if (reponse === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE);
        else
            // Sinon on met à jour la valeur
            this.reponse = reponse;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $reponse
     * @return {string}
     */
    public get $reponse(): string {
        return this.reponse;
    }

    /**
     * Getter $valide
     * @return {boolean}
     */
    public get $valide(): boolean {
        return this.valide;
    }

    /**
     * Getter $question
     * @return {Question}
     */
    public get $question(): Question {
        return this.question;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $reponse
     * @param {string} value
     */
    public set $reponse(value: string) {
        // Si la réponse est vide ou null on lève une exception
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE);
        else
            // Sinon on met à jour la valeur
            this.reponse = value;
    }

    /**
     * Setter $valide
     * @param {boolean} value
     */
    public set $valide(value: boolean) {
        this.valide = value;
    }

    /**
     * Setter $question
     * @param {Question} value
     */
    public set $question(value: Question) {
        this.question = value;
    }
}