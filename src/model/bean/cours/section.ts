import sanitizeHtml, { Transformer, Tag, Attributes, IFrame } from "sanitize-html";
import EmptyStringError from "../../errors/empty.string.error";
import { Chapitre } from './chapitre';
import { JSDOM } from "jsdom";
import { HTMLAnalyseur } from "../../utils/html.analyseur";

/**
 * Classe représentant une section
 * 
 * @author Developpix
 * @version 0.1
 */
export class Section {
    /**
     * l'id de la section
     */
    private id: number;
    /**
     * Le numéro de la section
     */
    private numero: number;
    /**
     * Le nom de la section
     */
    private nom: string;
    /**
     * Le contenu de la section
     */
    private contenu: string;
    /**
     * Le chapitre associé
     */
    private chapitre: Chapitre;

    /**
     * Constructeur de la classe
     * @param $id l'ID
     * @param $numero le numéro
     * @param $nom le nom
     * @param $contenu le contenu 
     * @param $chapitre le chapitre associé
     */
    constructor($id: number, $numero: number, $nom: string, $contenu: string, $chapitre: Chapitre) {
        this.id = $id;
        this.numero = $numero;
        this.nom = $nom;
        this.chapitre = $chapitre;

        if ($contenu === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE)
        else
            this.contenu = HTMLAnalyseur.securiser($contenu);
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $nom
     * @return {string}
     */
    public get $nom(): string {
        return this.nom;
    }

    /**
     * Getter $contenu
     * @return {string}
     */
    public get $contenu(): string {
        return this.contenu;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $nom
     * @param {string} value
     */
    public set $nom(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE)
        else
            this.nom = value;
    }

    /**
     * Setter $contenu
     * @param {string} value
     */
    public set $contenu(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_CONTENU_VIDE)
        else
            this.contenu = HTMLAnalyseur.securiser(value);
    }

    /**
     * Getter $numero
     * @return {number}
     */
    public get $numero(): number {
        return this.numero;
    }

    /**
     * Setter $numero
     * @param {number} value
     */
    public set $numero(value: number) {
        this.numero = value;
    }

    /**
     * Getter $chapitre
     * @return {Chapitre}
     */
    public get $chapitre(): Chapitre {
        return this.chapitre;
    }

    /**
     * Setter $chapitre
     * @param {Chapitre} value
     */
    public set $chapitre(value: Chapitre) {
        this.chapitre = value;
    }
}