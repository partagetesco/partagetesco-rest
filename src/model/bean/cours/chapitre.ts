import EmptyStringError from "../../errors/empty.string.error";
import { Cours } from "./cours";

/**
 * Classe représentant un chapitre
 * 
 * @author Developpix
 * @version 0.1
 */
export class Chapitre {
    /**
     * l'id du chapitre
     */
    private id: number;
    /**
     * Le numéro du chapitre
     */
    private numero: number;
    /**
     * Le nom du chapitre
     */
    private nom: string;
    /**
     * La description du chapitre
     */
    private description: string;
    /**
     * Le cours
     */
    private cours: Cours;

    /**
     * Constructeur de la classe
     * @param id l'ID
     * @param numero le numéro
     * @param nom le nom
     * @param description la description 
     * @param cours le cours associé
     */
    constructor(id: number, numero: number, nom: string, description: string, cours: Cours) {
        this.id = id;
        this.numero = numero;
        this.nom = nom;
        this.description = description;
        this.cours = cours;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $nom
     * @return {string}
     */
    public get $nom(): string {
        return this.nom;
    }

    /**
     * Getter $description
     * @return {string}
     */
    public get $description(): string {
        return this.description;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $nom
     * @param {string} value
     */
    public set $nom(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE)
        else
            this.nom = value;
    }

    /**
     * Setter $description
     * @param {string} value
     */
    public set $description(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE)
        else
            this.description = value;
    }

    /**
     * Getter $numero
     * @return {number}
     */
	public get $numero(): number {
		return this.numero;
	}

    /**
     * Getter $cours
     * @return {Cours}
     */
	public get $cours(): Cours {
		return this.cours;
	}

    /**
     * Setter $numero
     * @param {number} value
     */
	public set $numero(value: number) {
		this.numero = value;
	}

    /**
     * Setter $cours
     * @param {Cours} value
     */
	public set $cours(value: Cours) {
		this.cours = value;
	}
}