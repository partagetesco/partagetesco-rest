import { Fichier } from "../../fichiers/fichier";
import { Chapitre } from "../chapitre";
import { ExercicePratique } from "./exercice.pratique";

/**
 * Classe représentant un exercice pratique avec une correction automatique
 * 
 * @author Developpix
 * @version 0.1
 */
export class ExercicePratiqueAuto extends ExercicePratique {
    /**
     * Générateur pour les entrées
     */
    private generateurEntrees: Fichier;
    /**
     * Correcteur
     */
    private correcteur: Fichier;

    /**
     * Constructeur de la classe
     * 
     * @param $id l'ID
     * @param $enonce l'énoncé
     * @param $chapitre le chapitre associé
     * @param $generateurEntrees le générateur des entrées
     * @param $correcteur le correcteur
     */
    constructor($id: number, $enonce: string, $chapitre: Chapitre, $generateurEntrees: Fichier, $correcteur: Fichier) {
        super($id, $enonce, $chapitre);

        this.generateurEntrees = $generateurEntrees;
        this.correcteur = $correcteur;
    }

    /**
     * Getter $generateurEntrees
     * @return {Fichier}
     */
	public get $generateurEntrees(): Fichier {
		return this.generateurEntrees;
	}

    /**
     * Getter $correcteur
     * @return {Fichier}
     */
	public get $correcteur(): Fichier {
		return this.correcteur;
	}

    /**
     * Setter $generateurEntrees
     * @param {Fichier} value
     */
	public set $generateurEntrees(value: Fichier) {
		this.generateurEntrees = value;
	}

    /**
     * Setter $correcteur
     * @param {Fichier} value
     */
	public set $correcteur(value: Fichier) {
		this.correcteur = value;
	}
}