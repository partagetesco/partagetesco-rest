import { ExercicePratique } from "./exercice.pratique";
import { Utilisateur } from "../../utilisateurs/utilisateur";
import { Chapitre } from "../chapitre";

/**
 * Classe représentant un exercice pratique avec une correction manuelle
 * 
 * @author Developpix
 * @version 0.1
 */
export class ExercicePratiqueManuel extends ExercicePratique {
    /**
     * Les utilisateurs en charge de corriger les exercices
     */
    private correcteurs: Utilisateur[];

    /**
     * Constructeur de la classe
     * 
     * @param $id l'ID
     * @param $enonce l'énoncé
     * @param $chapitre le chapitre associé
     * @param $correcteurs les correcteurs
     */
    constructor($id: number, $enonce: string, $chapitre: Chapitre, $correcteurs: Utilisateur[]) {
        super($id, $enonce, $chapitre);
        this.correcteurs = $correcteurs;
    }

    /**
     * Getter $correcteurs
     * @return {Utilisateur[]}
     */
    public get $correcteurs(): Utilisateur[] {
        return this.correcteurs;
    }

    /**
     * Setter $correcteurs
     * @param {Utilisateur[]} value
     */
    public set $correcteurs(value: Utilisateur[]) {
        this.correcteurs = value;
    }
}