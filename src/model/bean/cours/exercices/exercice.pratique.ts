import { Chapitre } from "../chapitre";
import { HTMLAnalyseur } from "../../../utils/html.analyseur";

/**
 * Exercice pratique
 * 
 * @author Developpix
 * @version 0.1
 */
export abstract class ExercicePratique {
    /**
     * ID de l'exercice pratique
     */
    private id: number;
    /**
     * L'énoncé de l'exercice
     */
    private enonce: string;
    /**
     * Le chapitre associé
     */
    private chapitre: Chapitre;

    /**
     * Constructeur de la classe
     * 
     * @param $id l'ID
     * @param $enonce l'énoncé
     * @param $chapitre le chapitre associé
     */
    constructor($id: number, $enonce: string, $chapitre: Chapitre) {
        this.id = $id;
        this.enonce = HTMLAnalyseur.securiser($enonce);
        this.chapitre = $chapitre;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $enonce
     * @return {string}
     */
    public get $enonce(): string {
        return this.enonce;
    }

    /**
     * Getter $chapitre
     * @return {Chapitre}
     */
    public get $chapitre(): Chapitre {
        return this.chapitre;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $enonce
     * @param {string} value
     */
    public set $enonce(value: string) {
        this.enonce = HTMLAnalyseur.securiser(value);
    }

    /**
     * Setter $chapitre
     * @param {Chapitre} value
     */
    public set $chapitre(value: Chapitre) {
        this.chapitre = value;
    }

}