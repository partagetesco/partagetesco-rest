import EmptyStringError from "../../errors/empty.string.error";
import { Fichier } from "../fichiers/fichier";
import { Categorie } from "./categorie";

/**
 * Classe représentant un cours
 * 
 * @author Developpix
 * @version 0.1
 */
export class Cours {
    /**
     * l'id du cours
     */
    private id: number;
    /**
     * Le slug du cours
     */
    private slug: string;
    /**
     * Le nom du cours
     */
    private nom: string;
    /**
     * La description du cours
     */
    private description: string;
    /**
     * La catégorie
     */
    private categorie: Categorie;
    /**
     * L'icône (image représentant le cours)
     */
    private icone: Fichier;

    /**
     * Constructeur de la classe
     * @param id l'ID
     * @param slug le slug
     * @param nom le nom
     * @param description la description 
     * @param categorie la catégorie associé
     * @param icone l'icône
     */
    constructor(id: number, slug: string, nom: string, description: string, categorie: Categorie, icone?: Fichier) {
        this.id = id;
        this.slug = slug;
        this.nom = nom;
        this.description = description;
        this.categorie = categorie;
        
        // Si une icône est défini on l'associe
        if (icone)
            this.icone = icone;
    }

    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $slug
     * @return {string}
     */
    public get $slug(): string {
        return this.slug;
    }

    /**
     * Getter $nom
     * @return {string}
     */
    public get $nom(): string {
        return this.nom;
    }

    /**
     * Getter $description
     * @return {string}
     */
    public get $description(): string {
        return this.description;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $slug
     * @param {string} value
     */
    public set $slug(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_SLUG_VIDE)
        else
            this.slug = value;
    }

    /**
     * Setter $nom
     * @param {string} value
     */
    public set $nom(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE)
        else
            this.nom = value;
    }

    /**
     * Setter $description
     * @param {string} value
     */
    public set $description(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE)
        else
            this.description = value;
    }

    /**
     * Getter $categorie
     * @return {Categorie}
     */
    public get $categorie(): Categorie {
        return this.categorie;
    }

    /**
     * Getter $icone
     * @return {Fichier}
     */
    public get $icone(): Fichier {
        return this.icone;
    }

    /**
     * Setter $categorie
     * @param {Categorie} value
     */
    public set $categorie(value: Categorie) {
        this.categorie = value;
    }

    /**
     * Setter $icone
     * @param {Fichier} value
     */
    public set $icone(value: Fichier) {
        this.icone = value;
    }
}