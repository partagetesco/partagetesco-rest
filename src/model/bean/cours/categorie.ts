import EmptyStringError from "../../errors/empty.string.error";

/**
 * Classe représentant une catégorie
 * 
 * @author Developpix
 * @version 0.1
 */
export class Categorie {
    /**
     * l'id de la catégorie
     */
    private id: number;
    /**
     * Le slug de la catégorie
     */
    private slug: string;
    /**
     * Le nom de la catégorie
     */
    private nom: string;
    /**
     * La description de la catégorie
     */
    private description: string;
    /**
     * La catégorie parente si il s'agit d'une sous-catégorie
     */
    private parent: Categorie;

    /**
     * Constructeur de la classe
     * @param id l'ID
     * @param slug le slug
     * @param nom le nom
     * @param description la description 
     * @param parent la catégorie parente (optionnel)
     */
    constructor(id: number, slug: string, nom: string, description: string, parent?: Categorie) {
        this.id = id;
        this.slug = slug;
        this.nom = nom;
        this.description = description;
        this.parent = parent;
    }
    
    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $slug
     * @return {string}
     */
    public get $slug(): string {
        return this.slug;
    }

    /**
     * Getter $nom
     * @return {string}
     */
    public get $nom(): string {
        return this.nom;
    }

    /**
     * Getter $description
     * @return {string}
     */
    public get $description(): string {
        return this.description;
    }

    /**
     * Getter $parent
     * @return {Categorie}
     */
    public get $parent(): Categorie {
        return this.parent;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $slug
     * @param {string} value
     */
    public set $slug(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_SLUG_VIDE)
        else
        this.slug = value;
    }

    /**
     * Setter $nom
     * @param {string} value
     */
    public set $nom(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE)
        else
        this.nom = value;
    }

    /**
     * Setter $description
     * @param {string} value
     */
    public set $description(value: string) {
        if (value === '')
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE)
        else
            this.description = value;
    }

    /**
     * Setter $parent
     * @param {Categorie} value
     */
    public set $parent(value: Categorie) {
        this.parent = value;
    }

}