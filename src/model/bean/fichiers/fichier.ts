import EmptyStringError from "../../errors/empty.string.error";
import { FormatFichier } from "./format.fichier";
import { Licence } from "./licence";
import { Utilisateur } from "../utilisateurs/utilisateur";
import { writeFileSync, readFileSync, unlinkSync, existsSync, mkdirSync } from "fs";

/**
 * Classe représentant un fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export class Fichier {
    /**
     * ID du fichier
     */
    private id: number;
    /**
     * Nom du fichier
     */
    private nom: string;
    /**
     * Description du fichier
     */
    private description: string;
    /**
     * Format du fichier
     */
    private format: FormatFichier;
    /**
     * Licence du fichier
     */
    private licence: Licence;
    /**
     * Propriétaire du fichier
     */
    private propietaire: Utilisateur;
    /**
     * Les données du fichier (contenu)
     */
    private donnees: Buffer;

    /**
     * Constructeur de la classe
     * @param id l'id
     * @param nom le nom
     * @param description la description
     * @param format le format
     * @param licence la licence
     * @param proprietaire le propriétaire du fichier
     */
    constructor(id: number, nom: string, description: string, format: FormatFichier, licence: Licence, proprietaire: Utilisateur) {
        this.id = id;
        this.format = format;
        this.licence = licence;
        this.propietaire = proprietaire;

        // Si le nom est vide on lève une exception
        if (nom === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        } else {
            // Sinon on associe le nom
            this.nom = nom;
        }

        // Si la description est vide on lève une exception
        if (description === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        } else {
            // Sinon on associe la description
            this.description = description;
        }
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID du fichier
     */
    public getId(): number { return this.id; }

    /**
     * Accesseur pour l'ID
     * 
     * @param id le nouvel id du fichier
     */
    public setId(id: number) { this.id = id; }

    /**
     * Accesseur pour le nom
     * 
     * @returns le nom du fichier
     */
    public getNom(): string { return this.nom; }

    /**
     * Modifieur pour le nom
     * 
     * @param nom le nouveau nom du fichier
     */
    public setNom(nom: string) { this.nom = nom; }

    /**
     * Accesseur pour la description
     * 
     * @returns la description
     */
    public getDescription(): string { return this.description; }

    /**
     * Modifieur pour la description
     * 
     * @param description la nouvelle description
     */
    public setDescription(description: string) { this.description = description; }

    /**
     * Accesseur pour l'URI
     * 
     * @returns l'URI vers le fichier
     */
    public getUri(): string { return `${process.env.PATH_STOCKAGE_FICHIERS}/${this.propietaire.getPseudo()}/${this.nom}`; }

    /**
     * Accesseur pour le format
     * 
     * @returns le format
     */
    public getFormat(): FormatFichier { return this.format; }

    /**
     * Modifieur pour le format
     * 
     * @param format le nouveau format
     */
    public setFormat(format: FormatFichier) { this.format = format; }

    /**
     * Accesseur pour la licence
     * 
     * @returns la licence
     */
    public getLicence(): Licence { return this.licence; }

    /**
     * Modifieur pour la licence
     * 
     * @param licence la nouvelle licence 
     */
    public setLicence(licence: Licence) { this.licence = licence; }

    /**
     * Accesseur pour le propriétaire
     * 
     * @returns le propriétaire
     */
    public getProprietaire(): Licence { return this.licence; }

    /**
     * Modifieur pour le propriétaire
     * 
     * @param proprietaire le nouveau propriétaire 
     */
    public setProprietaire(proprietaire: Utilisateur) { this.propietaire = proprietaire; }

    /**
     * Méthode permettant d'enregistrer le fichier en mémoire
     */
    public enregistrer(donnees: Buffer) {
        // On enregistre le contenu du fichier
        this.donnees = donnees;

        // Si le dossier de l'utilisateur n'existe pas on le créé
        if (!existsSync(this.getUri().replace(this.nom, '')))
            mkdirSync(this.getUri().replace(this.nom, ''));

        // On écrit les données en mémoire
        writeFileSync(this.getUri(), donnees);
    }

    /**
     * Méthode permettant de lire le fichier depuis la mémoire
     * 
     * @returns un Buffer contenant le contenu du fichier
     */
    public lire(): Buffer {
        // On enregistre le contenu du fichier
        this.donnees = readFileSync(this.getUri());

        // On retourne le contenu lus
        return this.donnees;
    }

    /**
     * Méthode permettant de supprimer le fichier en mémoire
     */
    public supprimer() {
        // Si le fichier existe on le supprime
        if (existsSync(this.getUri()))
            unlinkSync(this.getUri());
    }
}