import EmptyStringError from "../../errors/empty.string.error";
import { TypeFichier } from "./type.fichier";

/**
 * Classe représentant un format de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export class FormatFichier {
    /**
     * ID du format de fichier
     */
    private id: number;
    /**
     * Nom du format de fichier
     */
    private nom: string;
    /**
     * Description du format de fichier
     */
    private description: string;
    /**
     * Le type de fichier associé à ce format
     */
    private type: TypeFichier;

    /**
     * Constructeur de la classe
     * @param id l'id
     * @param nom le nom
     * @param description la description
     * @param type le type de fichier associé
     */
    constructor(id: number, nom: string, description: string, type: TypeFichier) {
        this.id = id;
        this.type = type;

        // Si le nom est vide on lève une exception
        if (nom === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        } else {
            // Sinon on associe le nom
            this.nom = nom;
        }

        // Si la description est vide on lève une exception
        if (description === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        } else {
            // Sinon on associe la description
            this.description = description;
        }
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID du format de fichier
     */
    public getId(): number { return this.id; }

    /**
     * Modifieur pour l'ID
     * 
     * @param id le nouvel id du format de fichier
     */
    public setId(id: number) { this.id = id; }

    /**
     * Accesseur pour le nom
     * 
     * @returns le nom du format de fichier
     */
    public getNom(): string { return this.nom; }

    /**
     * Modifieur pour le nom
     * 
     * @param nom le nouveau nom du format
     */
    public setNom(nom: string) {
        this.nom = nom;
        // Si le nom est vide on lève une exception
        if (nom === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        } else {
            // Sinon on associe le nom
            this.nom = nom;
        }
    }

    /**
     * Accesseur pour la description
     * 
     * @returns la description
     */
    public getDescription(): string { return this.description; }

    /**
     * Modifieur pour la description
     * 
     * @param description la nouvelle description
     */
    public setDescription(description: string) {
        // Si la description est vide on lève une exception
        if (description === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        } else {
            // Sinon on associe la description
            this.description = description;
        }
    }

    /**
     * Accesseur pour le type de fichier associé
     * 
     * @returns le type de fichier
     */
    public getType(): TypeFichier { return this.type; }

    /**
     * Modifieur pour le type de fichier associé
     * 
     * @param type le nouveau type de fichier associé 
     */
    public setType(type: TypeFichier) { this.type = type; }
}