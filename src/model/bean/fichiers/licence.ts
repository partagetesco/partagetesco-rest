import EmptyStringError from "../../errors/empty.string.error";

/**
 * Classe représentant une licence
 * 
 * @author Developpix
 * @version 0.1
 */
export class Licence {
    /**
     * ID de la licence
     */
    private id: number;
    /**
     * Nom de la licence
     */
    private nom: string;
    /**
     * La version de la licence
     */
    private version: string;
    /**
     * Description de la licence
     */
    private description: string;
    /**
     * URL de la licence
     */
    private url: URL;

    /**
     * Constructeur de la classe
     * @param id l'id
     * @param nom le nom
     * @param version la version
     * @param description la description
     * @param url l'URL vers le texte de la licence
     */
    constructor(id: number, nom: string, version: string, description: string, url: URL) {
        this.id = id;
        this.url = url;

        // Si le nom est vide on lève une exception
        if (nom === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        } else {
            // Sinon on associe le nom
            this.nom = nom;
        }

        // Si la version est vide on lève une exception
        if (version === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_VERSION_VIDE);
        } else {
            // Sinon on associe le nom
            this.version = version;
        }

        // Si la description est vide on lève une exception
        if (description === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        } else {
            // Sinon on associe la description
            this.description = description;
        }
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID de la licence
     */
    public getId(): number { return this.id; }

    /**
     * Modifieur pour l'ID
     * 
     * @param id le nouvel id de la licence
     */
    public setId(id: number) { this.id = id; }

    /**
     * Accesseur pour le nom
     * 
     * @returns le nom de la licence
     */
    public getNom(): string { return this.nom; }

    /**
     * Modifieur pour le nom
     * 
     * @param nom le nouveau nom de la licence
     */
    public setNom(nom: string) { this.nom = nom; }

    /**
     * Accesseur pour la version
     * 
     * @returns la version
     */
    public getVersion(): string { return this.version; }

    /**
     * Modifieur pour la version
     * 
     * @param version la nouvelle version 
     */
    public setVersion(version: string) { this.version = version };

    /**
     * Accesseur pour la description
     * 
     * @returns la description
     */
    public getDescription(): string { return this.description; }

    /**
     * Modifieur pour la description
     * 
     * @param description la nouvelle description
     */
    public setDescription(description: string) { this.description = description; }

    /**
     * Accesseur pour l'URL
     * 
     * @returns l'URL vers le texte de la licence
     */
    public getUrl(): URL { return this.url; }

    /**
     * Modifieur pour l'URL
     * 
     * @param url la nouvelle URL vers le texte de la licence 
     */
    public setUrl(url: URL) { this.url = url; }
}