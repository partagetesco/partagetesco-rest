import EmptyStringError from "../../errors/empty.string.error";

/**
 * Classe représentant un type de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export class TypeFichier {
    /**
     * ID du type de fichier
     */
    private id: number;
    /**
     * Nom du type de fichier
     */
    private nom: string;
    /**
     * Description du type de fichier
     */
    private description: string;

    /**
     * Constructeur de la classe
     * @param id l'id
     * @param nom le nom
     * @param description la description
     */
    constructor(id: number, nom: string, description: string) {
        this.id = id;

        // Si le nom est vide on lève une exception
        if (nom === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        } else {
            // Sinon on associe le nom
            this.nom = nom;
        }

        // Si la description est vide on lève une exception
        if (description === '') {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        } else {
            // Sinon on associe la description
            this.description = description;
        }
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID du type de fichier
     */
    public getId(): number { return this.id; }

    /**
     * Modifieur pour l'ID
     * 
     * @param id le nouvelle ID
     */
    public setId(id: number) { this.id = id; }

    /**
     * Accesseur pour le nom
     * 
     * @returns le nom du type de fichier
     */
    public getNom(): string { return this.nom; }

    /**
     * Modifieur pour le nom
     * 
     * @param nom le nouveau nom du type
     */
    public setNom(nom: string) { this.nom = nom; }

    /**
     * Accesseur pour la description
     * 
     * @returns la description
     */
    public getDescription(): string { return this.description; }

    /**
     * Modifieur pour la description
     * 
     * @param description la nouvelle description
     */
    public setDescription(description: string) { this.description = description; }
}