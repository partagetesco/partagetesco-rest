import EmptyStringError from "../../errors/empty.string.error";
import MailInvalideError from "../../errors/mail.invalide.error";
import Fonction from "./fonction";

/**
 * Classe permettant de créer un utilisateur
 * 
 * @author Developpix
 * @version 0.1
 */
export class Utilisateur {
    /**
     * Regex des adresses mail valides
     */
    private static readonly REGEXP_CARACTERE_MOT_MAIL: string = "[a-zA-Z0-9_-]";

    /**
     * Regex des adresses mail valides
     */
    private static readonly REGEXP_MAIL: RegExp = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    /**
     * L'ID de l'utilisateur
     */
    private id: number;
    /**
     * Le pseudonyme de l'utilisateur
     */
    private pseudo: string;
    /**
     * L'adresse mail de l'utilisateur
     */
    private mail: string;
    /**
     * La biographie de l'utilisateur
     */
    private biographie: string;
    /**
     * La fonction de l'utilisateur
     */
    private fonction: Fonction;

    /**
     * Constructeur de la classe
     * 
     * @param id l'ID de l'utilisateur
     * @param pseudo le pseudo de l'utilisateur
     * @param mail l'adresse mail de l'utilisateur
     * @param biographie la biographie de l'utilisateur (optionnel)
     * @param fonction la fonction de l'utilisateur (optionnel)
     */
    constructor(id: number, pseudo: string, mail: string, biographie?: string, fonction?: Fonction) {
        this.id = id;
        this.setPseudo(pseudo);
        this.setMail(mail);
        this.setBiographie(biographie);
        this.setFonction(fonction);
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID de l'utilisateur
     */
    public getId(): number {
        return this.id;
    }

    /**
     * Modifieur pour l'ID
     * 
     * @param id le nouvel ID de l'utilisateur
     */
    public setId(id: number) {
        this.id = id;
    }

    /**
     * Accesseur pour le pseudo
     * 
     * @returns le pseudo de l'utilisateur
     */
    public getPseudo(): string {
        return this.pseudo;
    }

    /**
     * Modofieur pour le pseudo
     * 
     * @param pseudo le nouveau pseudo du joueur
     */
    public setPseudo(pseudo: string) {
        // Si le pseudo existe et n'est pas vide on le met à jour
        if (pseudo && pseudo !== "") {
            this.pseudo = pseudo;
        } else {
            throw new EmptyStringError(EmptyStringError.MESSAGE_PSEUDO_VIDE);
        }
    }

    /**
     * Accesseur pour l'adresse mail
     * 
     * @returns l'adresse mail de l'utilisateur
     */
    public getMail(): string {
        return this.mail;
    }

    /**
     * Modifieur pour l'adresse mail
     * 
     * @param mail l'adresse mail
     */
    public setMail(mail: string) {
        if (mail && mail !== "") {
            // Si la chaîne saisie est une adresse mail, on met à jour l'adresse mail
            if (Utilisateur.REGEXP_MAIL.test(mail)) {
                this.mail = mail;
            } else {
                // Sinon on lève une exception
                throw new MailInvalideError();
            }
        } else {
            throw new EmptyStringError(EmptyStringError.MESSAGE_MAIL_VIDE);
        }
    }

    /**
     * Accesseur pour la fonction
     * 
     * @returns la fonction de l'utilisateur
     */
    public getFonction(): Fonction { return this.fonction; }

    /**
     * Modifieur pour la fonction
     * 
     * @param fonction la nouvelle fonction de l'utilisateur
     */
    public setFonction(fonction: Fonction) { this.fonction = fonction; }

    /**
     * Accesseur pour la biographie de l'utilisateur
     * 
     * @returns la biographie de l'utilisateur
     */
    public getBiographie(): string {
        return this.biographie;
    }

    /**
     * Modifieur pour la biographie de l'utilisateur
     * 
     * @param biographie la biographie de l'utilisateur 
     */
    public setBiographie(biographie = "") {
        this.biographie = biographie;
    }
}
