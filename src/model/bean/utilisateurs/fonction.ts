import EmptyStringError from "../../errors/empty.string.error";

/**
 * La fonction d'un compte
 * 
 * @author Developpix
 * @version 0.1
 */
export default class Fonction {
    /**
     * L'ID de la fonction
     */
    private id: number;
    /**
     * Le nom de la fonction
     */
    private nom: string;
    /**
     * La description de la fonction
     */
    private description: string;

    /**
     * Constructeur de la classe
     * 
     * @param id l'ID de la fonction
     * @param nom le pseudo de la fonction
     * @param description la description de la fonction
     */
    constructor(id: number, nom: string, description: string) {
        this.id = id;
        this.setNom(nom);
        this.setMail(description);
    }

    /**
     * Accesseur pour l'ID
     * 
     * @returns l'ID de la fonction
     */
    public getId(): number {
        return this.id;
    }

    /**
     * Modifieur pour l'ID
     * 
     * @param id le nouvel ID de la fonction
     */
    public setId(id: number) {
        this.id = id;
    }

    /**
     * Accesseur pour le nom
     * 
     * @returns le nom de la fonction
     */
    public getNom(): string {
        return this.nom;
    }

    /**
     * Modofieur pour le nom
     * 
     * @param nom le nouveau nom de la fonction
     */
    public setNom(nom: string) {
        // Si le nom existe et n'est pas vide on le met à jour
        if (nom !== "") {
            this.nom = nom;
        } else {
            throw new EmptyStringError(EmptyStringError.MESSAGE_NOM_VIDE);
        }
    }

    /**
     * Accesseur pour la description
     * 
     * @returns la description de la fonction
     */
    public getDescription(): string {
        return this.description;
    }

    /**
     * Modifieur pour la description
     * 
     * @param description la description
     */
    public setMail(description: string) {
        // On vérifie que la description n'est pas vide ou nulle sinon lève une exception
        if (description !== "") {
                this.description = description;
        } else {
            throw new EmptyStringError(EmptyStringError.MESSAGE_DESCRIPTION_VIDE);
        }
    }
}
