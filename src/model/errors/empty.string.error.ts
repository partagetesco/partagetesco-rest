/**
 * Erreur de chaîne de caractères vide
 * 
 * @author Developpix
 * @version 0.2
 */
export default class EmptyStringError extends Error {
    /**
     * Message pour indiquer que l'adresse mail est vide ou null 
     */
    public static readonly MESSAGE_MAIL_VIDE: string = "Adresse mail vide ou null";
    /**
     * Message pour indiquer que le pseudo est vide ou null 
     */
    public static readonly MESSAGE_PSEUDO_VIDE: string = "Pseudo vide ou null";
    /**
     * Message pour indiquer que le mot de passe est vide ou null 
     */
    public static readonly MESSAGE_MOT_DE_PASSE_VIDE: string = "Mot de passe vide ou null";
    /**
     * Message pour indiquer que le slug est vide ou null 
     */
    public static readonly MESSAGE_SLUG_VIDE: string = "Slug vide ou null";
    /**
     * Message pour indiquer que le nom est vide ou null 
     */
    public static readonly MESSAGE_NOM_VIDE: string = "Nom vide ou null";
    /**
     * Message pour indiquer que la description est vide ou null 
     */
    public static readonly MESSAGE_DESCRIPTION_VIDE: string = "Description vide ou null";
    /**
     * Message pour indiquer que le contenu est vide ou null 
     */
    public static readonly MESSAGE_CONTENU_VIDE: string = "Contenu vide ou null";
    /**
     * Message pour indiquer que la version est vide ou null 
     */
    public static readonly MESSAGE_VERSION_VIDE: string = "Version vide ou null";
    /**
     * Message pour indiquer que l'URI est vide ou null 
     */
    public static readonly MESSAGE_URI_VIDE: string = "URI vide ou null";

    /**
     * Constructeur de la classe
     * 
     * @param message 
     */
    constructor(message: string) {
        super(message);
    }
}
