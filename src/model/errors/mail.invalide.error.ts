/**
 * Erreur d'adresse mail invalide
 * 
 * @author Developpix
 * @version 0.1
 */
export default class MailInvalideError extends Error {
    public static readonly DEFAULT_MESSAGE: string = "Adresse mail invalide";

    /**
     * Constructeur de la classe 
     */
    constructor() {
        //On créer l'erreur avec le message par défaut
        super(MailInvalideError.DEFAULT_MESSAGE);
    }
}
