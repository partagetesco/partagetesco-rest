/**
 * Classe permetttant de créer une erreur contenue dans la réponse HTTP
 */
export class ReponseError {
    /**
     * Erreur inattendue
     */
    public static readonly ERREUR_INATTENDUE = new ReponseError(0, "inconnue", "Érreur inattendue. Veuillez contacter l'administrateur.");
    /**
     * Erreur d'échec d'authentification
     */
    public static readonly ERREUR_ECHEC_AUTHENTIFICATION = new ReponseError(401000, "Échec de l'authentification", "Les informations saisies sont erronées.");
    /**
     * Erreur de token absent
     */
    public static readonly ERREUR_TOKEN_ABSENT = new ReponseError(401001, "Token absent", "Impossible de vous identifier. Veuillez définir le paramètre 'token'.");
    /**
     * Erreur de token invalide
     */
    public static readonly ERREUR_TOKEN_INVALIDE = new ReponseError(401002, "Token  invalide", "Le token envoyé est invalide.");
    /**
     * Erreur de compte supprimé
     */
    public static readonly ERREUR_COMPTE_SUPPRIME = new ReponseError(401003, "Compte supprimé", "Le compte a été supprimé. Veuillez contacter l'administrateur.");
    /**
     * Erreur accès restreins aux administrateurs
     */
    public static readonly ERREUR_ACCESS_RESTREINS_ADMIN = new ReponseError(401004, "Accès restreins (Niveau 1)", "L'accès est restreins aux administrateurs.");
    /**
     * Erreur accès restreins aux modérateurs
     */
    public static readonly ERREUR_ACCESS_RESTREINS_MODO = new ReponseError(401004, "Accès restreins (Niveau 2)", "L'accès est restreins aux modérateurs.");
    /**
     * Erreur de pseudo déjà pris
     */
    public static readonly ERREUR_PSEUDO_PRIS = new ReponseError(23505, "Pseudo pris", "Le pseudo est déjà utilisé");
    /**
     * Erreur de mail déjà utilisée
     */
    public static readonly ERREUR_MAIL_PRIS = new ReponseError(23505, "Mail pris", "L'adresse mail a déjà été utilisée");

    /**
     * Constructeur de la classe
     * 
     * @param code le code d'erreur 
     * @param type le type d'erreur
     * @param message le message d'erreur
     */
    constructor(private code:number, private type:string, private message:string) { }
}