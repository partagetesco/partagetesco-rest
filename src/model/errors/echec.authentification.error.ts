/**
 * Erreur signalant l'échec de l'authentification
 * 
 * @author Developpix
 * @version 0.1
 */
export default class EchecAuthentificationError extends Error {
    /**
     * Message pour indiquer que l'authentification à échoué
     */
    public static readonly MESSAGE_ECHEC_AUTHENTIFICATION: string = "Échec de l'authentification";
    /**
     * Constructeur de la classe
     * 
     * @param message 
     */
    constructor() {
        super(EchecAuthentificationError.MESSAGE_ECHEC_AUTHENTIFICATION);
    }
}
