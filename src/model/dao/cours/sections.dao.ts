import DatabaseSession from '../../database.session';
import { QueryResult } from 'pg';
import { ParametresRechercheSections } from '../../types/parametres.recherche';
import { Section } from '../../bean/cours/section';
import { ChapitresDao } from './chapitres.dao';

// Création des instances des DAO
const chapitresDao: ChapitresDao = new ChapitresDao();

/**
 * DAO pour les sections
 * 
 * @author Developpix
 * @version 0.1
 */
export class SectionsDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une section dans la base de donnée
     * 
     * @param section la section
     * @returns une promesse contenant la section enregistré
     */
    public async insert(section: Section): Promise<Section> {
        return new Promise<Section>(
            (resolve: (value: Section) => void, reject: (reason: any) => void) => {
                this.session.executerRequete(`INSERT INTO SECTIONS
                (numero, nom, contenu, id_chapitre) VALUES($1, $2, $3, $4)`,
                    [section.$numero, section.$nom, section.$contenu, section.$chapitre.$id]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM SECTIONS WHERE numero = $1 AND id_chapitre = $2",
                                [section.$numero, section.$chapitre.$id]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        section.$id = res.rows[0].id;

                                        // On retourne la section à jour
                                        resolve(section);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une section dans la base de donnée
     * 
     * @param section la section à mettre à jour
     * @returns une promesse contenant la section à jour
     */
    public async update(section: Section): Promise<Section> {
        return new Promise<Section>(
            (resolve: (value: Section) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE SECTIONS
                SET numero = $1, nom = $2, contenu = $3, id_chapitre = $4 WHERE id = $5`,
                    [section.$numero, section.$nom, section.$contenu, section.$chapitre.$id, section.$id])
                    .then(
                        (res: QueryResult) => {
                            // On retourne la section à jour
                            resolve(section);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une section dans la base de donnée
     * 
     * @param id l'id de la section à supprimer
     * @returns une promesse contenant la section supprimée
     */
    public async delete(id: number): Promise<Section> {
        return new Promise<Section>(
            async (resolve: (value: Section) => void, reject: (reason: any) => void) => {
                // On récupère la section avant suppression
                let sections: Section = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM SECTIONS WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la section supprimé
                            resolve(sections);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une section dans la base de donnée
     * 
     * @param id l'ID de la section a récupérer
     * @returns une promesse contenant la section
     */
    public async get(id: number): Promise<Section> {
        return new Promise<Section>(
            (resolve: (value: Section) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM SECTIONS WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let sections = res.rows[0];

                            // Si aucune section trouvé on retourne null
                            if (sections == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la section
                                resolve(new Section(sections.id, sections.numero,
                                    sections.nom, sections.contenu,
                                    await chapitresDao.get(sections.id_chapitre)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les sections dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les sections
     */
    public async getAll(parametres: ParametresRechercheSections): Promise<Section[]> {
        return new Promise<Section[]>(
            (resolve: (value: Section[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM SECTIONS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les sections
                        let sections: Section[] = []

                        // On crée les sections à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const section = res.rows[i];
                            sections.push(new Section(section.id, section.numero,
                                section.nom, section.contenu,
                                await chapitresDao.get(section.id_chapitre)));
                        }

                        // On retourne les sections
                        resolve(sections);
                    },
                    (err: Error) => reject(err));
            });
    }
}