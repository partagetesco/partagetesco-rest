import {Cours} from '../../bean/cours/cours';
import {CategoriesDao} from './categories.dao';
import DatabaseSession from '../../database.session';
import { QueryResult } from 'pg';
import {FichiersDao} from '../fichiers/fichiers.dao';
import { ParametresRechercheCours } from '../../types/parametres.recherche';

// Création des instances des DAO
const categoriesDao:CategoriesDao = new CategoriesDao();
const fichiersDao:FichiersDao = new FichiersDao();

/**
 * DAO pour les cours
 * 
 * @author Developpix
 * @version 0.1
 */
export class CoursDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un cours dans la base de donnée
     * 
     * @param cours le cours de cours
     * @returns une promesse contenant le cours enregistré
     */
    public async insert(cours: Cours): Promise<Cours> {
        return new Promise<Cours>(
            (resolve: (value: Cours) => void, reject: (reason: any) => void) => {
                let params: any[] = [cours.$slug, cours.$nom, cours.$description, cours.$categorie.$id];

                // Si il y a une icône associée on ajoute son id aux paramètres
                if (cours.$icone)
                    params.push(cours.$icone.getId());

                this.session.executerRequete(`INSERT INTO COURS
                (slug, nom, description, id_categorie${cours.$icone ? ', id_icone' : ''})
                VALUES($1, $2, $3, $4${cours.$icone ? ', $5' : ''})`, params).then(
                    (res: QueryResult) => {
                        this.session.executerRequete("SELECT id FROM COURS WHERE slug = $1",
                            [cours.$slug]).then(
                                (res: QueryResult) => {
                                    // Mise à jour de l'ID
                                    cours.$id = res.rows[0].id;

                                    // On retourne le cours à jour
                                    resolve(cours);
                                },
                                (err: Error) => reject(err));
                    },
                    (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un cours dans la base de donnée
     * 
     * @param cours le cours à mettre à jour
     * @returns une promesse contenant le cours à jour
     */
    public async update(cours: Cours): Promise<Cours> {
        return new Promise<Cours>(
            (resolve: (value: Cours) => void, reject: (reason: any) => void) => {
                let params: any[] = [cours.$slug, cours.$nom, cours.$description, cours.$categorie.$id, cours.$id];

                // Si il y a une icône associée on ajoute son id aux paramètres
                if (cours.$icone)
                    params.push(cours.$icone.getId());

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE COURS
                SET slug = $1, nom = $2, description = $3, id_categorie = $4${cours.$icone ? ', id_icone = $6' : ''}
                WHERE id = $5`, params).then(
                        (res: QueryResult) => {
                            // On retourne le cours à jour
                            resolve(cours);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un cours dans la base de donnée
     * 
     * @param id l'id du cours à supprimer
     * @returns une promesse contenant le cours supprimée
     */
    public async delete(id: number): Promise<Cours> {
        return new Promise<Cours>(
            async (resolve: (value: Cours) => void, reject: (reason: any) => void) => {
                // On récupère le cours avant suppression
                let cours: Cours = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM COURS WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le cours supprimé
                            resolve(cours);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un cours dans la base de donnée
     * 
     * @param id l'ID du cours a récupérer
     * @returns une promesse contenant le cours
     */
    public async get(id: number): Promise<Cours> {
        return new Promise<Cours>(
            (resolve: (value: Cours) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM COURS WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let cours = res.rows[0];

                            // Si aucun cours trouvé on retourne null
                            if (cours == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le cours
                                resolve(new Cours(cours.id, cours.slug,
                                    cours.nom, cours.description,
                                    await categoriesDao.get(cours.id_categorie),
                                    await fichiersDao.get(cours.id_icone)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les cours dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les cours de cours
     */
    public async getAll(parametres: ParametresRechercheCours): Promise<Cours[]> {
        return new Promise<Cours[]>(
            (resolve: (value: Cours[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM COURS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les cours de cours
                        let coursTab: Cours[] = []

                        // On crée les cours à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const cours = res.rows[i];
                            coursTab.push(new Cours(cours.id, cours.slug,
                                cours.nom, cours.description,
                                await categoriesDao.get(cours.id_categorie),
                                await fichiersDao.get(cours.id_icone)));
                        }

                        // On retourne les cours
                        resolve(coursTab);
                    },
                    (err: Error) => reject(err));
            });
    }
}