import { QueryResult } from "pg";
import DatabaseSession from "../../../database.session";
import { ParametresRechercheQCM } from '../../../types/parametres.recherche';
import { ChapitresDao } from "../chapitres.dao";
import { QCM } from "../../../bean/cours/qcm/qcm";

// Création des instances des DAO
const chapitresDao: ChapitresDao = new ChapitresDao();

/**
 * DAO pour les QCM
 * 
 * @author Developpix
 * @version 0.1
 */
export class QCMsDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un qcm dans la base de donnée
     * 
     * @param qcm le qcm
     * @returns une promesse contenant le qcm enregistré
     */
    public async insert(qcm: QCM): Promise<QCM> {
        return new Promise<QCM>(
            (resolve: (value: QCM) => void, reject: (reason: any) => void) => {
                this.session.executerRequete(`INSERT INTO QCMS
                (slug, nom, id_chapitre) VALUES($1, $2, $3)`,
                    [qcm.$slug, qcm.$nom, qcm.$chapitre.$id]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM QCMS WHERE slug = $1",
                                [qcm.$slug]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        qcm.$id = res.rows[0].id;

                                        // On retourne le qcm à jour
                                        resolve(qcm);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un qcm dans la base de donnée
     * 
     * @param qcm le qcm à mettre à jour
     * @returns une promesse contenant le qcm à jour
     */
    public async update(qcm: QCM): Promise<QCM> {
        return new Promise<QCM>(
            (resolve: (value: QCM) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE QCMS
                SET slug = $1, nom = $2, id_chapitre = $3 WHERE id = $4`,
                    [qcm.$slug, qcm.$nom, qcm.$chapitre.$id, qcm.$id]).then(
                        (res: QueryResult) => {
                            // On retourne le qcm à jour
                            resolve(qcm);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un qcm dans la base de donnée
     * 
     * @param id l'id du qcm à supprimer
     * @returns une promesse contenant le qcm supprimée
     */
    public async delete(id: number): Promise<QCM> {
        return new Promise<QCM>(
            async (resolve: (value: QCM) => void, reject: (reason: any) => void) => {
                // On récupère le qcm avant suppression
                let qcm: QCM = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM QCMS WHERE id = $1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le qcm supprimé
                            resolve(qcm);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un qcm dans la base de donnée
     * 
     * @param id l'ID du qcm a récupérer
     * @returns une promesse contenant le qcm
     */
    public async get(id: number): Promise<QCM> {
        return new Promise<QCM>(
            (resolve: (value: QCM) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM QCMS WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let qcm = res.rows[0];

                            // Si aucun qcm trouvé on retourne null
                            if (qcm == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le qcm
                                resolve(new QCM(qcm.id, qcm.slug, qcm.nom,
                                    await chapitresDao.get(qcm.id_chapitre)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les qcms dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les qcms de qcm
     */
    public async getAll(parametres: ParametresRechercheQCM): Promise<QCM[]> {
        return new Promise<QCM[]>(
            (resolve: (value: QCM[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM QCMS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les qcms de qcm
                        let qcms: QCM[] = []

                        // On crée les qcms à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const qcm = res.rows[i];
                            qcms.push(new QCM(qcm.id, qcm.slug, qcm.nom,
                                await chapitresDao.get(qcm.id_chapitre)));
                        }

                        // On retourne les qcms
                        resolve(qcms);
                    },
                    (err: Error) => reject(err));
            });
    }
}