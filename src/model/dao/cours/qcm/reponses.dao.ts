import { QueryResult } from "pg";
import DatabaseSession from "../../../database.session";
import { ParametresRechercheReponses } from '../../../types/parametres.recherche';
import { Reponse } from "../../../bean/cours/qcm/reponse";
import { QuestionsDao } from "./questions.dao";

// Création des instances des DAO
const questionsDao: QuestionsDao = new QuestionsDao();

/**
 * DAO pour les reponses
 * 
 * @author Developpix
 * @version 0.1
 */
export class ReponsesDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une reponse dans la base de donnée
     * 
     * @param reponse la reponse
     * @returns une promesse contenant la reponse enregistrée
     */
    public async insert(reponse: Reponse): Promise<Reponse> {
        return new Promise<Reponse>(
            (resolve: (value: Reponse) => void, reject: (reason: any) => void) => {
                this.session.executerRequete(`INSERT INTO REPONSES
                (reponse, valide, id_question) VALUES($1, $2, $3)`,
                    [reponse.$reponse, reponse.$valide, reponse.$question.$id]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM REPONSES WHERE reponse = $1 AND id_question = $2",
                                [reponse.$reponse, reponse.$question.$id]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        reponse.$id = res.rows[0].id;

                                        // On retourne la reponse à jour
                                        resolve(reponse);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une reponse dans la base de donnée
     * 
     * @param reponse la reponse à mettre à jour
     * @returns une promesse contenant la reponse à jour
     */
    public async update(reponse: Reponse): Promise<Reponse> {
        return new Promise<Reponse>(
            (resolve: (value: Reponse) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE REPONSES
                SET reponse = $1, valide = $2, id_question = $3 WHERE id = $4`,
                    [reponse.$reponse, reponse.$valide, reponse.$question.$id, reponse.$id]).then(
                        (res: QueryResult) => {
                            // On retourne la reponse à jour
                            resolve(reponse);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une reponse dans la base de donnée
     * 
     * @param id l'id de la reponse à supprimer
     * @returns une promesse contenant la reponse supprimée
     */
    public async delete(id: number): Promise<Reponse> {
        return new Promise<Reponse>(
            async (resolve: (value: Reponse) => void, reject: (reason: any) => void) => {
                // On récupère la reponse avant suppression
                let reponse: Reponse = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM REPONSES WHERE id = $1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la reponse supprimé
                            resolve(reponse);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une reponse dans la base de donnée
     * 
     * @param id l'ID de la reponse a récupérer
     * @returns une promesse contenant la reponse
     */
    public async get(id: number): Promise<Reponse> {
        return new Promise<Reponse>(
            (resolve: (value: Reponse) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM REPONSES WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let reponse = res.rows[0];

                            // Si aucune reponse trouvé on retourne null
                            if (reponse == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la reponse
                                resolve(new Reponse(reponse.id, reponse.reponse,
                                    reponse.valide, await questionsDao.get(reponse.id_question)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les reponses dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les reponses de reponse
     */
    public async getAll(parametres: ParametresRechercheReponses): Promise<Reponse[]> {
        return new Promise<Reponse[]>(
            (resolve: (value: Reponse[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM REPONSES ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les reponses de reponse
                        let reponses: Reponse[] = []

                        // On crée les reponses à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const reponse = res.rows[i];
                            reponses.push(new Reponse(reponse.id, reponse.reponse,
                                reponse.valide, await questionsDao.get(reponse.id_question)));
                        }

                        // On retourne les reponses
                        resolve(reponses);
                    },
                    (err: Error) => reject(err));
            });
    }
}