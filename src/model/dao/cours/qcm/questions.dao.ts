import { QueryResult } from "pg";
import DatabaseSession from "../../../database.session";
import { ParametresRechercheQuestions } from '../../../types/parametres.recherche';
import { QCMsDao } from "./qcms.dao";
import { Question } from "../../../bean/cours/qcm/question";

// Création des instances des DAO
const qcmsDao: QCMsDao = new QCMsDao();

/**
 * DAO pour les questions
 * 
 * @author Developpix
 * @version 0.1
 */
export class QuestionsDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une question dans la base de donnée
     * 
     * @param question la question
     * @returns une promesse contenant la question enregistré
     */
    public async insert(question: Question): Promise<Question> {
        return new Promise<Question>(
            (resolve: (value: Question) => void, reject: (reason: any) => void) => {
                this.session.executerRequete(`INSERT INTO QUESTIONS
                (question, id_qcm) VALUES($1, $2)`,
                    [question.$question, question.$qcm.$id]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM QUESTIONS WHERE question = $1 AND id_qcm = $2",
                                [question.$question, question.$qcm.$id]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        question.$id = res.rows[0].id;

                                        // On retourne la question à jour
                                        resolve(question);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une question dans la base de donnée
     * 
     * @param question la question à mettre à jour
     * @returns une promesse contenant la question à jour
     */
    public async update(question: Question): Promise<Question> {
        return new Promise<Question>(
            (resolve: (value: Question) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE QUESTIONS
                SET question = $1, id_qcm = $2 WHERE id = $3`,
                    [question.$question, question.$qcm.$id, question.$id]).then(
                        (res: QueryResult) => {
                            // On retourne la question à jour
                            resolve(question);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une question dans la base de donnée
     * 
     * @param id l'id de la question à supprimer
     * @returns une promesse contenant la question supprimée
     */
    public async delete(id: number): Promise<Question> {
        return new Promise<Question>(
            async (resolve: (value: Question) => void, reject: (reason: any) => void) => {
                // On récupère la question avant suppression
                let question: Question = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM QUESTIONS WHERE id = $1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la question supprimé
                            resolve(question);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une question dans la base de donnée
     * 
     * @param id l'ID de la question a récupérer
     * @returns une promesse contenant la question
     */
    public async get(id: number): Promise<Question> {
        return new Promise<Question>(
            (resolve: (value: Question) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM QUESTIONS WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let question = res.rows[0];

                            // Si aucune question trouvé on retourne null
                            if (question == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la question
                                resolve(new Question(question.id, question.question,
                                    await qcmsDao.get(question.id_qcm)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les questions dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les questions de question
     */
    public async getAll(parametres: ParametresRechercheQuestions): Promise<Question[]> {
        return new Promise<Question[]>(
            (resolve: (value: Question[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM QUESTIONS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les questions de question
                        let questions: Question[] = []

                        // On crée les questions à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const question = res.rows[i];
                            questions.push(new Question(question.id, question.question,
                                await qcmsDao.get(question.id_qcm)));
                        }

                        // On retourne les questions
                        resolve(questions);
                    },
                    (err: Error) => reject(err));
            });
    }
}