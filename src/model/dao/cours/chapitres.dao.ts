import DatabaseSession from '../../database.session';
import { QueryResult } from 'pg';
import { ParametresRechercheChapitres } from '../../types/parametres.recherche';
import { CoursDao } from './cours.dao';
import {Chapitre} from '../../bean/cours/chapitre';

// Création des instances des DAO
const coursDao: CoursDao = new CoursDao();

/**
 * DAO pour les chapitres
 * 
 * @author Developpix
 * @version 0.1
 */
export class ChapitresDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un chapitre dans la base de donnée
     * 
     * @param chapitre le chapitre
     * @returns une promesse contenant le chapitre enregistré
     */
    public async insert(chapitre: Chapitre): Promise<Chapitre> {
        return new Promise<Chapitre>(
            (resolve: (value: Chapitre) => void, reject: (reason: any) => void) => {
                this.session.executerRequete(`INSERT INTO CHAPITRES
                (numero, nom, description, id_cours) VALUES($1, $2, $3, $4)`,
                    [chapitre.$numero, chapitre.$nom, chapitre.$description, chapitre.$cours.$id]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM CHAPITRES WHERE numero = $1 AND id_cours = $2",
                                [chapitre.$numero, chapitre.$cours.$id]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        chapitre.$id = res.rows[0].id;

                                        // On retourne le chapitre à jour
                                        resolve(chapitre);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un chapitre dans la base de donnée
     * 
     * @param chapitre le chapitre à mettre à jour
     * @returns une promesse contenant le chapitre à jour
     */
    public async update(chapitre: Chapitre): Promise<Chapitre> {
        return new Promise<Chapitre>(
            (resolve: (value: Chapitre) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE CHAPITRES
                SET numero = $1, nom = $2, description = $3, id_cours = $4 WHERE id = $5`,
                    [chapitre.$numero, chapitre.$nom, chapitre.$description, chapitre.$cours.$id, chapitre.$id])
                    .then(
                        (res: QueryResult) => {
                            // On retourne le chapitre à jour
                            resolve(chapitre);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un chapitre dans la base de donnée
     * 
     * @param id l'id du chapitre à supprimer
     * @returns une promesse contenant le chapitre supprimée
     */
    public async delete(id: number): Promise<Chapitre> {
        return new Promise<Chapitre>(
            async (resolve: (value: Chapitre) => void, reject: (reason: any) => void) => {
                // On récupère le chapitre avant suppression
                let chapitres: Chapitre = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM CHAPITRES WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le chapitre supprimé
                            resolve(chapitres);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un chapitre dans la base de donnée
     * 
     * @param id l'ID du chapitre a récupérer
     * @returns une promesse contenant le chapitre
     */
    public async get(id: number): Promise<Chapitre> {
        return new Promise<Chapitre>(
            (resolve: (value: Chapitre) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM CHAPITRES WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let chapitres = res.rows[0];

                            // Si aucun chapitre trouvé on retourne null
                            if (chapitres == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le chapitre
                                resolve(new Chapitre(chapitres.id, chapitres.numero,
                                    chapitres.nom, chapitres.description,
                                    await coursDao.get(chapitres.id_cours)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les chapitres dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les chapitres
     */
    public async getAll(parametres: ParametresRechercheChapitres): Promise<Chapitre[]> {
        return new Promise<Chapitre[]>(
            (resolve: (value: Chapitre[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM CHAPITRES ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les chapitres
                        let chapitres: Chapitre[] = []

                        // On crée les chapitres à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const chapitre = res.rows[i];
                            chapitres.push(new Chapitre(chapitre.id, chapitre.numero,
                                chapitre.nom, chapitre.description,
                                await coursDao.get(chapitre.id_cours)));
                        }

                        // On retourne les chapitres
                        resolve(chapitres);
                    },
                    (err: Error) => reject(err));
            });
    }
}