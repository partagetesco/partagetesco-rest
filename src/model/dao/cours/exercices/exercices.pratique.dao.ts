import { QueryResult } from "pg";
import DatabaseSession from "../../../database.session";
import { UtilisateursDao } from "../../utilisateurs/utilisateurs.dao";
import { FichiersDao } from "../../fichiers/fichiers.dao";
import { ExercicePratique } from "../../../bean/cours/exercices/exercice.pratique";
import { ExercicePratiqueAuto } from "../../../bean/cours/exercices/exercice.pratique.auto";
import { ExercicePratiqueManuel } from "../../../bean/cours/exercices/exercice.pratique.manuel";
import { Utilisateur } from "../../../bean/utilisateurs/utilisateur";
import { ChapitresDao } from "../chapitres.dao";
import { ParametresRechercheExercicesPratique } from "../../../types/parametres.recherche";

// Création des instances des DAO
const utilisateursDao: UtilisateursDao = new UtilisateursDao();
const fichiersDao: FichiersDao = new FichiersDao();
const chapitresDao: ChapitresDao = new ChapitresDao();

/**
 * DAO pour les exercices pratique
 * 
 * @author Developpix
 * @version 0.1
 */
export class ExercicesPratiqueDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un exercice pratique dans la base de donnée
     * 
     * @param exercice l'exercice pratique
     * @returns une promesse contenant l'exercice pratique enregistré
     */
    public async insert(exercice: ExercicePratique): Promise<ExercicePratique> {
        return new Promise<ExercicePratique>(
            (resolve: (value: ExercicePratique) => void, reject: (reason: any) => void) => {
                if (exercice instanceof ExercicePratiqueAuto) {
                    let exo: ExercicePratiqueAuto = <ExercicePratiqueAuto>exercice;

                    this.session.executerRequete(`INSERT INTO EXERCICES_PRATIQUE
                    (enonce, correction_auto, id_script_entrees, id_script_verification, id_chapitre)
                    VALUES($1, $2, $3, $4, $5)`,
                        [exo.$enonce, true, exo.$generateurEntrees.getId(), exo.$correcteur.getId(), exo.$chapitre.$id]).then(
                            (res: QueryResult) => {
                                this.session.executerRequete("SELECT id FROM EXERCICES_PRATIQUE WHERE enonce = $1 AND id_chapitre = $2",
                                    [exo.$enonce, exo.$chapitre.$id]).then((res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        exo.$id = res.rows[0].id;

                                        // On retourne l'exercice à jour
                                        resolve(exo);
                                    }, (err: Error) => reject(err));
                            },
                            (err: Error) => reject(err));
                } else {
                    let exo: ExercicePratiqueManuel = <ExercicePratiqueManuel>exercice;

                    this.session.executerRequete(`INSERT INTO EXERCICES_PRATIQUE
                    (enonce, correction_auto, id_chapitre) VALUES($1, $2, $3)`,
                        [exo.$enonce, false, exo.$chapitre.$id]).then(
                            (res: QueryResult) => {
                                this.session.executerRequete("SELECT id FROM EXERCICES_PRATIQUE WHERE enonce = $1 AND id_chapitre = $2",
                                [exo.$enonce, exo.$chapitre.$id]).then((res: QueryResult) => {
                                    // Mise à jour de l'ID
                                    exo.$id = res.rows[0].id;

                                    // On associe les correcteurs à l'exercice
                                    for (let i = 0; i < exo.$correcteurs.length; i++) {
                                        // On récupère le correcteur
                                        const correcteur: Utilisateur = exo.$correcteurs[i];

                                        // On associe le correcteur à l'exercice
                                        this.session.executerRequete(
                                            "INSERT INTO CORRECTEURS (id_exercice, id_compte) VALUES($1, $2)",
                                            [exo.$id, correcteur.getId()]).then(
                                                (res: QueryResult) => { },
                                                (err: any) => reject(err));
                                    }

                                    // On retourne l'exercice à jour
                                    resolve(exo);
                                }, (err: Error) => reject(err));
                            },
                            (err: Error) => reject(err));
                }
            });
    }

    /**
     * Méthode permettant de mettre à jour un exercice pratique dans la base de donnée
     * 
     * @param exercice l'exercice pratique à mettre à jour
     * @returns une promesse contenant l'exercice pratique à jour
     */
    public async update(exercice: ExercicePratique): Promise<ExercicePratique> {
        return new Promise<ExercicePratique>(
            async (resolve: (value: ExercicePratique) => void, reject: (reason: any) => void) => {
                // On supprime les anciens correcteurs
                await this.session.executerRequete("DELETE FROM EXERCICES_PRATIQUE WHERE id_exercice = $1",
                    [exercice.$id]);

                if (exercice instanceof ExercicePratiqueAuto) {
                    let exo: ExercicePratiqueAuto = <ExercicePratiqueAuto>exercice;

                    this.session.executerRequete(`UPDATE EXERCICES_PRATIQUE
                    SET enonce = $1, correction_auto = $2, id_script_entrees = $3,
                    id_script_verification =$4, id_chapitre = $5 WHERE id = $5`,
                        [exo.$enonce, true, exo.$generateurEntrees.getId(), exo.$correcteur.getId(), exo.$chapitre.$id]).then(
                            (res: QueryResult) => {
                                this.session.executerRequete("SELECT id FROM EXERCICES_PRATIQUE WHERE enonce = $1 AND id_chapitre = $2", [exo.$enonce]).then((res: QueryResult) => {
                                    // Mise à jour de l'ID
                                    exo.$id = res.rows[0].id;

                                    // On retourne l'exercice à jour
                                    resolve(exo);
                                }, (err: Error) => reject(err));
                            },
                            (err: Error) => reject(err));
                } else {
                    let exo: ExercicePratiqueManuel = <ExercicePratiqueManuel>exercice;

                    this.session.executerRequete(`UPDATE EXERCICES_PRATIQUE
                    SET enonce = $1, correction_auto = $2, id_chapitre = $5 WHERE id = $5`,
                        [exo.$enonce, false, exo.$chapitre.$id]).then(
                            (res: QueryResult) => {
                                // On associe les correcteurs à l'exercice
                                for (let i = 0; i < exo.$correcteurs.length; i++) {
                                    // On récupère le correcteur
                                    const correcteur: Utilisateur = exo.$correcteurs[i];

                                    // On associe le correcteur à l'exercice
                                    this.session.executerRequete(
                                        "INSERT INTO CORRECTEURS (id_exercice, id_compte) VALUES($1, $2)",
                                        [exo.$id, correcteur.getId()]).then(
                                            (res: QueryResult) => { },
                                            (err: any) => reject(err));
                                }

                                // On retourne l'exercice à jour
                                resolve(exo);
                            },
                            (err: Error) => reject(err));
                }
            });
    }

    /**
     * Méthode permettant de supprimer une exercice dans la base de donnée
     * 
     * @param id l'id de la exercice à supprimer
     * @returns une promesse contenant la exercice supprimée
     */
    public async delete(id: number): Promise<ExercicePratique> {
        return new Promise<ExercicePratique>(
            async (resolve: (value: ExercicePratique) => void, reject: (reason: any) => void) => {
                // On récupère la exercice avant suppression
                let exercice: ExercicePratique = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM EXERCICES_PRATIQUE WHERE id = $1",
                    [id]).then((res: QueryResult) => {
                        //  On supprime les correcteurs associés
                        return this.session.executerRequete("DELETE FROM EXERCICES_PRATIQUE WHERE id_exercice = $1",
                            [exercice.$id]);
                    },
                        (err: Error) => reject(err)).then(
                            (res: QueryResult) => {
                                // On retourne la exercice supprimé
                                resolve(exercice);
                            },
                            (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une exercice dans la base de donnée
     * 
     * @param id l'ID de la exercice a récupérer
     * @returns une promesse contenant la exercice
     */
    public async get(id: number): Promise<ExercicePratique> {
        return new Promise<ExercicePratique>(
            (resolve: (value: ExercicePratique) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM EXERCICES_PRATIQUE WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let exercice = res.rows[0];

                            // Si aucune exercice trouvé on retourne null
                            if (exercice == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le exercice
                                resolve(await this.creerExercice(exercice));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les exercices dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les exercices de exercice
     */
    public async getAll(parametres: ParametresRechercheExercicesPratique): Promise<ExercicePratique[]> {
        return new Promise<ExercicePratique[]>(
            (resolve: (value: ExercicePratique[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM EXERCICES_PRATIQUE ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les exercices de exercice
                        let exercices: ExercicePratique[] = []

                        // On crée les exercices à partir des données récupérées
                        for (let i = 0; i < res.rowCount; i++) {
                            const exercice = res.rows[i];

                            exercices.push(await this.creerExercice(exercice));
                        }

                        // On retourne les exercices
                        resolve(exercices);
                    },
                    (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer l'exercice à partir de ses données
     * 
     * @param donnees les données de l'exercice 
     * @returns une promesse contenant l'exercice pratique
     */
    private async creerExercice(donnees: any): Promise<ExercicePratique> {
        return new Promise<ExercicePratique>(
            async (resolve: (value: ExercicePratique) => void, reject: (reason: any) => void) => {
                if (donnees.correction_auto) {
                    resolve(new ExercicePratiqueAuto(donnees.id, donnees.enonce,
                        await chapitresDao.get(donnees.id_chapitre),
                        await fichiersDao.get(donnees.id_script_entrees),
                        await fichiersDao.get(donnees.id_script_verification)));
                }
                else {
                    this.session.executerRequete("SELECT * FROM CORRECTEURS WHERE id_exercice = $1",
                        [donnees.id]).then(async (res: QueryResult) => {
                            // On créer une liste de correcteurs
                            let correcteurs: Utilisateur[] = [];
                            // On récupères les correcteurs
                            for (let i = 0; i < res.rows.length; i++) {
                                const correcteur = res.rows[i];
                                correcteurs.push(await utilisateursDao.get(correcteur.id_compte));
                            }
                            // On retourne l'exercice
                            resolve(new ExercicePratiqueManuel(donnees.id, donnees.enonce,
                                await chapitresDao.get(donnees.id_chapitre), correcteurs));
                        }, (err: any) => reject(err));
                }
            });
    }
}