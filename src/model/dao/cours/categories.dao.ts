import { QueryResult } from "pg";
import DatabaseSession from "../../database.session";
import { ParametresRechercheCategories } from '../../types/parametres.recherche';
import {Categorie} from '../../bean/cours/categorie';

/**
 * DAO pour les catégories
 * 
 * @author Developpix
 * @version 0.1
 */
export class CategoriesDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une categorie dans la base de donnée
     * 
     * @param categorie la categorie de categorie
     * @returns une promesse contenant la categorie enregistré
     */
    public async insert(categorie: Categorie): Promise<Categorie> {
        return new Promise<Categorie>(
            (resolve: (value: Categorie) => void, reject: (reason: any) => void) => {
                let params: any[] = [categorie.$slug, categorie.$nom, categorie.$description];

                // Si il y a une catégorie parente on ajoute son id aux paramètres
                if (categorie.$parent)
                    params.push(categorie.$parent.$id);

                this.session.executerRequete(`INSERT INTO CATEGORIES
                (slug, nom, description${categorie.$parent ? ', id_parent' : ''})
                VALUES($1, $2, $3${categorie.$parent ? ', $4' : ''})`, params).then(
                    (res: QueryResult) => {
                        this.session.executerRequete("SELECT id FROM CATEGORIES WHERE slug = $1",
                            [categorie.$slug]).then(
                                (res: QueryResult) => {
                                    // Mise à jour de l'ID
                                    categorie.$id = res.rows[0].id;

                                    // On retourne la categorie à jour
                                    resolve(categorie);
                                },
                                (err: Error) => reject(err));
                    },
                    (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une categorie dans la base de donnée
     * 
     * @param categorie la categorie à mettre à jour
     * @returns une promesse contenant la categorie à jour
     */
    public async update(categorie: Categorie): Promise<Categorie> {
        return new Promise<Categorie>(
            (resolve: (value: Categorie) => void, reject: (reason: any) => void) => {
                let params: any[] = [categorie.$slug, categorie.$nom, categorie.$description, categorie.$id];

                // Si il y a une catégorie parente on ajoute son id aux paramètres
                if (categorie.$parent)
                    params.push(categorie.$parent.$id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(`UPDATE CATEGORIES
                SET slug = $1, nom = $2, description = $3${categorie.$parent ? ', id_parent = $5' : ''}
                WHERE id = $4`, params).then(
                        (res: QueryResult) => {
                            // On retourne la categorie à jour
                            resolve(categorie);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une categorie dans la base de donnée
     * 
     * @param id l'id de la categorie à supprimer
     * @returns une promesse contenant la categorie supprimée
     */
    public async delete(id: number): Promise<Categorie> {
        return new Promise<Categorie>(
            async (resolve: (value: Categorie) => void, reject: (reason: any) => void) => {
                // On récupère la categorie avant suppression
                let categorie: Categorie = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM CATEGORIES WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la categorie supprimé
                            resolve(categorie);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une categorie dans la base de donnée
     * 
     * @param id l'ID de la categorie a récupérer
     * @returns une promesse contenant la categorie
     */
    public async get(id: number): Promise<Categorie> {
        return new Promise<Categorie>(
            (resolve: (value: Categorie) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM CATEGORIES WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let categorie = res.rows[0];

                            // Si aucune categorie trouvé on retourne null
                            if (categorie == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la categorie
                                resolve(new Categorie(categorie.id, categorie.slug,
                                    categorie.nom, categorie.description,
                                    await this.get(categorie.id_parent)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les categories dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les categories de categorie
     */
    public async getAll(parametres: ParametresRechercheCategories): Promise<Categorie[]> {
        return new Promise<Categorie[]>(
            (resolve: (value: Categorie[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM CATEGORIES ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les categories de categorie
                        let categories: Categorie[] = []

                        // On crée les categories à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const categorie = res.rows[i];
                            categories.push(new Categorie(categorie.id, categorie.slug,
                                categorie.nom, categorie.description,
                                await this.get(categorie.id_parent)));
                        }

                        // On retourne les categories
                        resolve(categories);
                    },
                    (err: Error) => reject(err));
            });
    }
}