import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import Fonction from "../../bean/utilisateurs/fonction";
import { ParametresRechercheFonctions } from "../../types/parametres.recherche";

/**
 * DAO pour les fonctions
 * 
 * @author Developpix
 * @version 0.1
 */
export class FonctionsDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une fonction dans la base de donnée
     * 
     * @param fonction la fonction à enregistrer
     * @returns une promesse contenant la fonction enregistré
     */
    public async insert(fonction: Fonction): Promise<Fonction> {
        return new Promise<Fonction>(
            (resolve: (value: Fonction) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO FONCTIONS (nom, description) VALUES($1, $2)",
                    [fonction.getNom(), fonction.getDescription()]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM FONCTIONS WHERE nom = $1",
                                [fonction.getNom()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        fonction.setId(res.rows[0].id);

                                        // On retourne la fonction à jour
                                        resolve(fonction);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une fonction dans la base de donnée
     * 
     * @param fonction la fonction à mettre à jour
     * @returns une promesse contenant la fonction à jour
     */
    public async update(fonction: Fonction): Promise<Fonction> {
        return new Promise<Fonction>(
            (resolve: (value: Fonction) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("UPDATE FONCTIONS SET nom = $1, description = $2 WHERE id = $3",
                    [fonction.getNom(), fonction.getDescription(), fonction.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne la fonction à jour
                            resolve(fonction);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une fonction dans la base de donnée
     * 
     * @param id l'id de la fonction à supprimer
     * @returns une promesse contenant la fonction supprimée
     */
    public async delete(id: number): Promise<Fonction> {
        return new Promise<Fonction>(
            async (resolve: (value: Fonction) => void, reject: (reason: any) => void) => {
                // On récupère la fonction avant suppression
                let fonction:Fonction = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM FONCTIONS WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la fonction supprimée
                            resolve(fonction);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une fonction dans la base de donnée
     * 
     * @param id l'ID de la fonction a récupérer
     * @returns une promesse contenant la fonction
     */
    public async get(id: number): Promise<Fonction> {
        return new Promise<Fonction>(
            (resolve: (value: Fonction) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM FONCTIONS WHERE id = $1",
                    [id]).then(
                        (res: QueryResult) => {
                            let fonction = res.rows[0];

                            // Si aucune fonction trouvé on retourne null
                            if (fonction == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la fonction
                                resolve(new Fonction(fonction.id, fonction.nom, fonction.description));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les fonction dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les fonctions
     */
    public async getAll(parametres: ParametresRechercheFonctions): Promise<Fonction[]> {
        return new Promise<Fonction[]>(
            (resolve: (value: Fonction[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM FONCTIONS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    (res: QueryResult) => {
                        // On créer un tableau qui contiendra les fonctions
                        let fonctions: Fonction[] = []

                        // On crée les fonctions à partir des données récupérées
                        res.rows.forEach(fonction => {
                            fonctions.push(new Fonction(fonction.id, fonction.nom, fonction.description));
                        })

                        // On retourne la fonction
                        resolve(fonctions);
                    },
                    (err: Error) => reject(err));
            });
    }
}