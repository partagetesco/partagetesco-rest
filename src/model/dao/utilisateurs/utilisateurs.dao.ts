import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import { Utilisateur } from "../../bean/utilisateurs/utilisateur";
import EchecAuthentificationError from "../../errors/echec.authentification.error";
import { Fichier } from "../../bean/fichiers/fichier";
import Fonction from "../../bean/utilisateurs/fonction";
import { FonctionsDao } from "./fonctions.dao";
import { ParametresRechercheUtilisateurs } from "../../types/parametres.recherche";
import { createHmac } from "crypto";

// On instancie les DAO
const fonctionsDao: FonctionsDao = new FonctionsDao();

/**
 * DAO pour les utilisateurs
 * 
 * @author Developpix
 * @version 0.2
 */
export class UtilisateursDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer l'utilisateur dans la base de donnée
     * 
     * @param utilisateur les données sur l'utilisateur à enregistrer
     * @param motDePasse le mot de passe du utilisateur
     * @returns une promesse contenant l'utilisateur
     */
    public async insert(utilisateur: Utilisateur, motDePasse: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On créer une instance de la méthode de hash
                const hash = createHmac('sha512', process.env.MOT_DE_PASSE_SECRET);

                // On met à jour la fonction de hashage avec le mot de passe
                hash.update(motDePasse);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO UTILISATEURS (pseudo, mot_de_passe, mail, biographie) VALUES($1, $2, $3, $4)",
                    [utilisateur.getPseudo(), hash.digest('hex'), utilisateur.getMail(), utilisateur.getBiographie()])
                    .then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM UTILISATEURS WHERE pseudo = $1",
                                [utilisateur.getPseudo()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        utilisateur.setId(res.rows[0].id);

                                        // On retourne l'utilisateur à jour
                                        resolve(utilisateur);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour le pseudo d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur lutilisateur à mettre à jour
     * @param pseudo son nouveau pseudo
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updatePseudo(utilisateur: Utilisateur, pseudo: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET pseudo = $1
                    WHERE id = $2`,
                    [pseudo, utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour l'adresse mail d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur lutilisateur à mettre à jour
     * @param mail sa nouvelle adresse mail
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updateMail(utilisateur: Utilisateur, mail: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET mail = $1
                    WHERE id = $2`,
                    [mail, utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour la biographie d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur lutilisateur à mettre à jour
     * @param biographie sa nouvelle biographie
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updateBiographie(utilisateur: Utilisateur, biographie: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET biographie = $1
                    WHERE id = $2`,
                    [biographie, utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour le mot de passe d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur l'utilisateur à mettre à jour
     * @param motDePasse son nouveau mot de passe
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updateMotDePasse(utilisateur: Utilisateur, motDePasse: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On créer une instance de la méthode de hash
                const hash = createHmac('sha512', process.env.MOT_DE_PASSE_SECRET);

                // On met à jour la fonction de hashage avec le mot de passe
                hash.update(motDePasse);

                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET mot_de_passe = $1
                    WHERE id = $2`,
                    [hash.digest('hex'), utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour l'avatar d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur l'utilisateur à mettre à jour
     * @param avatar son nouvelle avatar
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updateAvatar(utilisateur: Utilisateur, avatar: Fichier): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET id_avatar = $1
                    WHERE id = $2`,
                    [avatar.getId(), utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour la fonction d'un utilisateur dans la base de donnée
     * 
     * @param utilisateur l'utilisateur à mettre à jour
     * @param fonction sa nouvelle fonction
     * @returns une promesse contenant l'utilisateur à jour
     */
    public async updateFonction(utilisateur: Utilisateur, fonction: Fonction): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on réalise la mise à jour
                this.session.executerRequete(`UPDATE UTILISATEURS
                    SET id_fonction = $1
                    WHERE id = $2`,
                    [fonction.getId(), utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne l'a 'utilisateur à jour
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une utilisateur dans la base de donnée
     * 
     * @param utilisateur la utilisateur à supprimer
     * @returns une promesse contenant la utilisateur supprimée
     */
    public async delete(utilisateur: Utilisateur): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM UTILISATEURS WHERE id = $1",
                    [utilisateur.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne la utilisateur à supprimer
                            resolve(utilisateur);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant d'authentifier l'utilisateur 
     * 
     * @param pseudo le pseudo du utilisateur
     * @param motDePasse le mot de passe du utilisateur
     * @returns une promesse contenant l'utilisateur
     */
    public async authentifier(pseudo: string, motDePasse: string): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On créer une instance de la méthode de hash
                const hash = createHmac('sha512', process.env.MOT_DE_PASSE_SECRET);

                // On met à jour la fonction de hashage avec le mot de passe
                hash.update(motDePasse);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM UTILISATEURS WHERE pseudo = $1 AND mot_de_passe = $2",
                    [pseudo, hash.digest('hex')]).then(
                        async (res: QueryResult) => {
                            let utilisateur = res.rows[0];

                            // Si aucun utilisateur trouvé on lève une exception d'échec d'authentification
                            if (utilisateur == null) { reject(new EchecAuthentificationError()); }
                            else {
                                // Sinon on retourne l'utilisateur
                                resolve(await this.creerUtilisateur(utilisateur));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un utilisateur dans la base de donnée
     * 
     * @param id l'ID de l'utilisateur
     * @returns une promesse contenant l'utilisateur
     */
    public async get(id: number): Promise<Utilisateur> {
        return new Promise<Utilisateur>(
            (resolve: (value: Utilisateur) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM UTILISATEURS WHERE id = $1", [id])
                    .then(
                        async (res: QueryResult) => {
                            // On récupère les données sur l'utilisateur
                            let utilisateur = res.rows[0];

                            if (utilisateur == null) { resolve(null); }
                            else {
                                // Sinon on retourne l'utilisateur
                                resolve(await this.creerUtilisateur(utilisateur));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer tous les utilisateurs
     * 
     * @param parametres les paramètres de recherches (un objet)
     * @returns une promesse contenant les utilisateurs recherchés
     */
    public async getAll(parametres: ParametresRechercheUtilisateurs): Promise<Utilisateur[]> {
        return new Promise<Utilisateur[]>(
            (resolve: (value: Utilisateur[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM UTILISATEURS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params)
                    .then(
                        async (res: QueryResult) => resolve(await this.creerUtilisateurs(res.rows)),
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de créer l'utilisateur à partir des données
     * 
     * @param donnees les données
     * @returns la promesse contenant l'utilisateur 
     */
    private async creerUtilisateur(donnees: any): Promise<Utilisateur> {
        let fonction: Fonction;

        // Si il y a une fonction on la récupère
        if (donnees.id_fonction != null)
            fonction = await fonctionsDao.get(donnees.id_fonction);

        // On retourne l'utilisateur
        return new Utilisateur(donnees.id, donnees.pseudo, donnees.mail, donnees.biographie, fonction);
    }

    /**
     * Méthode permettant de créer les utilisateurs à partir des données
     * 
     * @param donnees les données
     * @returns la promesse contenant les utilisateurs 
     */
    private async creerUtilisateurs(donnees: any[]): Promise<Utilisateur[]> {
        let utilisateurs: Utilisateur[] = [];

        for (let i = 0; i < donnees.length; i++) {
            utilisateurs.push(await this.creerUtilisateur(donnees[i]));
        }

        return utilisateurs;
    }
}