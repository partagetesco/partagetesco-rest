import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import { Fichier } from "../../bean/fichiers/fichier";
import { ParametresRechercheLicences, ParametresRechercheTypesFichier, ParametresRechercheFichiers } from "../../types/parametres.recherche";
import { LicencesDao } from "./licences.dao";
import { FormatsFichierDao } from "./formats.fichier.dao";
import { UtilisateursDao } from "../utilisateurs/utilisateurs.dao";

// Création des instances des DAO
const formatsFichierDao: FormatsFichierDao = new FormatsFichierDao();
const licencesDao: LicencesDao = new LicencesDao();
const utilisateursDao: UtilisateursDao = new UtilisateursDao();

/**
 * DAO pour les fichiers
 * 
 * @author Developpix
 * @version 0.1
 */
export class FichiersDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un fichier dans la base de donnée
     * 
     * @param fichier le fichier de fichier
     * @returns une promesse contenant le fichier enregistré
     */
    public async insert(fichier: Fichier): Promise<Fichier> {
        return new Promise<Fichier>(
            (resolve: (value: Fichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO FICHIERS (nom, description, id_format, id_licence, id_utilisateur) VALUES($1, $2, $3, $4, $5)",
                    [fichier.getNom(), fichier.getDescription(), fichier.getFormat().getId(), fichier.getLicence().getId(), fichier.getProprietaire().getId()]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM FICHIERS WHERE nom = $1",
                                [fichier.getNom()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        fichier.setId(res.rows[0].id);

                                        // On retourne le fichier à jour
                                        resolve(fichier);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un fichier dans la base de donnée
     * 
     * @param fichier le fichier à mettre à jour
     * @returns une promesse contenant le fichier à jour
     */
    public async update(fichier: Fichier): Promise<Fichier> {
        return new Promise<Fichier>(
            (resolve: (value: Fichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("UPDATE FICHIERS SET nom = $1, description = $2, id_format = $3, id_licence = $4, id_utilisateur = $5 WHERE id = $6",
                    [fichier.getNom(), fichier.getDescription(), fichier.getFormat().getId(), fichier.getLicence().getId(), fichier.getProprietaire().getId(), fichier.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne le fichier à jour
                            resolve(fichier);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un fichier dans la base de donnée
     * 
     * @param id l'id du fichier à supprimer
     * @returns une promesse contenant le fichier supprimée
     */
    public async delete(id: number): Promise<Fichier> {
        return new Promise<Fichier>(
            async (resolve: (value: Fichier) => void, reject: (reason: any) => void) => {
                // On récupère le fichier avant suppression
                let fichier: Fichier = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM FICHIERS WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le fichier supprimé
                            resolve(fichier);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un fichier dans la base de donnée
     * 
     * @param id l'ID du fichier a récupérer
     * @returns une promesse contenant le fichier
     */
    public async get(id: number): Promise<Fichier> {
        return new Promise<Fichier>(
            (resolve: (value: Fichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM FICHIERS WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let fichier = res.rows[0];

                            // Si aucun fichier trouvé on retourne null
                            if (fichier == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le fichier
                                resolve(new Fichier(fichier.id, fichier.nom, fichier.description,
                                    await formatsFichierDao.get(fichier.id_format),
                                    await licencesDao.get(fichier.id_licence),
                                    await utilisateursDao.get(fichier.id_utilisateur)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les fichiers dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les fichiers de fichier
     */
    public async getAll(parametres: ParametresRechercheFichiers): Promise<Fichier[]> {
        return new Promise<Fichier[]>(
            (resolve: (value: Fichier[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM FICHIERS ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les fichiers de fichier
                        let fichiers: Fichier[] = []

                        // On crée les fichiers à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const fichier = res.rows[i];
                            fichiers.push(new Fichier(fichier.id, fichier.nom, fichier.description,
                                await formatsFichierDao.get(fichier.id_format),
                                await licencesDao.get(fichier.id_licence),
                                await utilisateursDao.get(fichier.id_utilisateur)));

                        }

                        // On retourne les fichiers
                        resolve(fichiers);
                    },
                    (err: Error) => reject(err));
            });
    }
}