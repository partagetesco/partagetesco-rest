import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import { ParametresRechercheLicences } from "../../types/parametres.recherche";
import { Licence } from "../../bean/fichiers/licence";

/**
 * DAO pour les licences
 * 
 * @author Developpix
 * @version 0.1
 */
export class LicencesDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer une licence dans la base de donnée
     * 
     * @param licence la licence
     * @returns une promesse contenant la licence enregistré
     */
    public async insert(licence: Licence): Promise<Licence> {
        return new Promise<Licence>(
            (resolve: (value: Licence) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO LICENCES (nom, version, description, url) VALUES($1, $2, $3, $4)",
                    [licence.getNom(), licence.getVersion(), licence.getDescription(), licence.getUrl().href]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM LICENCES WHERE nom = $1",
                                [licence.getNom()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        licence.setId(res.rows[0].id);

                                        // On retourne la licence à jour
                                        resolve(licence);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour une licence dans la base de donnée
     * 
     * @param licence la licence à mettre à jour
     * @returns une promesse contenant la licence à jour
     */
    public async update(licence: Licence): Promise<Licence> {
        return new Promise<Licence>(
            (resolve: (value: Licence) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("UPDATE LICENCES SET nom = $1, version = $2, description = $3, url = $4 WHERE id = $5",
                    [licence.getNom(), licence.getVersion(), licence.getDescription(), licence.getUrl(), licence.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne la licence à jour
                            resolve(licence);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer une licence dans la base de donnée
     * 
     * @param id l'id du licence à supprimer
     * @returns une promesse contenant la licence supprimée
     */
    public async delete(id: number): Promise<Licence> {
        return new Promise<Licence>(
            async (resolve: (value: Licence) => void, reject: (reason: any) => void) => {
                // On récupère la licence avant suppression
                let licence:Licence = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM LICENCES WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne la licence supprimée
                            resolve(licence);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer une licence dans la base de donnée
     * 
     * @param id l'ID du licence a récupérer
     * @returns une promesse contenant la licence de fichier
     */
    public async get(id: number): Promise<Licence> {
        return new Promise<Licence>(
            (resolve: (value: Licence) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM LICENCES WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let licence = res.rows[0];

                            // Si aucune licence trouvée on retourne null
                            if (licence == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne la licence
                                resolve(new Licence(licence.id, licence.nom, licence.version, licence.description,
                                    new URL(licence.url)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les licences dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les licences de fichier
     */
    public async getAll(parametres: ParametresRechercheLicences): Promise<Licence[]> {
        return new Promise<Licence[]>(
            (resolve: (value: Licence[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM LICENCES ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les licences
                        let licences: Licence[] = []

                        // On crée les licences à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const licence = res.rows[i];
                            licences.push(new Licence(licence.id, licence.nom, licence.version, licence.description,
                                new URL(licence.url)));
                            
                        }
                        
                        // On retourne les licences
                        resolve(licences);
                    },
                    (err: Error) => reject(err));
            });
    }
}