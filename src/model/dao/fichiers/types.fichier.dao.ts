import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import { TypeFichier } from "../../bean/fichiers/type.fichier";
import { ParametresRechercheTypesFichier } from "../../types/parametres.recherche";

/**
 * DAO pour les types de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export class TypesFichierDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un type de fichier dans la base de donnée
     * 
     * @param type le type de fichier
     * @returns une promesse contenant le type de fichier enregistré
     */
    public async insert(type: TypeFichier): Promise<TypeFichier> {
        return new Promise<TypeFichier>(
            (resolve: (value: TypeFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO TYPES_FICHIER (nom, description) VALUES($1, $2)",
                    [type.getNom(), type.getDescription()]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM TYPES_FICHIER WHERE nom = $1",
                                [type.getNom()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        type.setId(res.rows[0].id);

                                        // On retourne le type de fichier à jour
                                        resolve(type);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un type de fichier dans la base de donnée
     * 
     * @param type le type de fichier à mettre à jour
     * @returns une promesse contenant le type de fichier à jour
     */
    public async update(type: TypeFichier): Promise<TypeFichier> {
        return new Promise<TypeFichier>(
            (resolve: (value: TypeFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("UPDATE TYPES_FICHIER SET nom = $1, description = $2 WHERE id = $3",
                    [type.getNom(), type.getDescription(), type.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne le type de fichier à jour
                            resolve(type);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un type de fichier dans la base de donnée
     * 
     * @param id l'id du type de fichier à supprimer
     * @returns une promesse contenant le type de fichier supprimée
     */
    public async delete(id: number): Promise<TypeFichier> {
        return new Promise<TypeFichier>(
            async (resolve: (value: TypeFichier) => void, reject: (reason: any) => void) => {
                // On récupère le type de fichier avant suppression
                let type:TypeFichier = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM TYPES_FICHIER WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le type de fichier supprimé
                            resolve(type);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un type de fichier dans la base de donnée
     * 
     * @param id l'ID du type de fichier a récupérer
     * @returns une promesse contenant le type de fichier
     */
    public async get(id: number): Promise<TypeFichier> {
        return new Promise<TypeFichier>(
            (resolve: (value: TypeFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM TYPES_FICHIER WHERE id = $1",
                    [id]).then(
                        (res: QueryResult) => {
                            let type = res.rows[0];

                            // Si aucun type trouvé on retourne null
                            if (type == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le type
                                resolve(new TypeFichier(type.id, type.nom, type.description));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les types de fichier dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les types de fichier
     */
    public async getAll(parametres: ParametresRechercheTypesFichier): Promise<TypeFichier[]> {
        return new Promise<TypeFichier[]>(
            (resolve: (value: TypeFichier[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM TYPES_FICHIER ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    (res: QueryResult) => {
                        // On créer un tableau qui contiendra les types de fichier
                        let types: TypeFichier[] = []

                        // On crée les types à partir des données récupérées
                        res.rows.forEach(type => {
                            types.push(new TypeFichier(type.id, type.nom, type.description));
                        })

                        // On retourne les types
                        resolve(types);
                    },
                    (err: Error) => reject(err));
            });
    }
}