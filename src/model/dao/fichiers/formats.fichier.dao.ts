import DatabaseSession from "../../database.session";
import { QueryResult } from "pg";
import { FormatFichier } from "../../bean/fichiers/format.fichier";
import { ParametresRechercheLicences, ParametresRechercheTypesFichier } from "../../types/parametres.recherche";
import { TypesFichierDao } from "./types.fichier.dao";

// Création des instances des DAO
const typesFichierDao:TypesFichierDao = new TypesFichierDao();

/**
 * DAO pour les formats de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export class FormatsFichierDao {
    /**
     * Session avec la base de donnée
     */
    private session: DatabaseSession;

    /**
     * Constructeur
     */
    constructor() {
        // Récupération d'une session avec la base de donnée
        this.session = DatabaseSession.getInstance();
    }

    /**
     * Méthode permettant d'enregistrer un format de fichier dans la base de donnée
     * 
     * @param format le format de fichier
     * @returns une promesse contenant le format de fichier enregistré
     */
    public async insert(format: FormatFichier): Promise<FormatFichier> {
        return new Promise<FormatFichier>(
            (resolve: (value: FormatFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("INSERT INTO FORMATS_FICHIER (nom, description, id_type_fichier) VALUES($1, $2, $3)",
                    [format.getNom(), format.getDescription(), format.getType().getId()]).then(
                        (res: QueryResult) => {
                            this.session.executerRequete("SELECT id FROM FORMATS_FICHIER WHERE nom = $1",
                                [format.getNom()]).then(
                                    (res: QueryResult) => {
                                        // Mise à jour de l'ID
                                        format.setId(res.rows[0].id);

                                        // On retourne le format de fichier à jour
                                        resolve(format);
                                    },
                                    (err: Error) => reject(err));
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de mettre à jour un format de fichier dans la base de donnée
     * 
     * @param format le format de fichier à mettre à jour
     * @returns une promesse contenant le format de fichier à jour
     */
    public async update(format: FormatFichier): Promise<FormatFichier> {
        return new Promise<FormatFichier>(
            (resolve: (value: FormatFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("UPDATE FORMATS_FICHIER SET nom = $1, description = $2, id_type_fichier WHERE id = $4",
                    [format.getNom(), format.getDescription(), format.getType().getId(), format.getId()]).then(
                        (res: QueryResult) => {
                            // On retourne le format de fichier à jour
                            resolve(format);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de supprimer un format de fichier dans la base de donnée
     * 
     * @param id l'id du format de fichier à supprimer
     * @returns une promesse contenant le format de fichier supprimée
     */
    public async delete(id: number): Promise<FormatFichier> {
        return new Promise<FormatFichier>(
            async (resolve: (value: FormatFichier) => void, reject: (reason: any) => void) => {
                // On récupère le format de fichier avant suppression
                let format:FormatFichier = await this.get(id);

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("DELETE FROM FORMATS_FICHIER WHERE id=$1",
                    [id]).then(
                        (res: QueryResult) => {
                            // On retourne le format de fichier supprimé
                            resolve(format);
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer un format de fichier dans la base de donnée
     * 
     * @param id l'ID du format de fichier a récupérer
     * @returns une promesse contenant le format de fichier
     */
    public async get(id: number): Promise<FormatFichier> {
        return new Promise<FormatFichier>(
            (resolve: (value: FormatFichier) => void, reject: (reason: any) => void) => {
                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete("SELECT * FROM FORMATS_FICHIER WHERE id = $1",
                    [id]).then(
                        async (res: QueryResult) => {
                            let format = res.rows[0];

                            // Si aucun format trouvé on retourne null
                            if (format == null) {
                                resolve(null);
                            } else {
                                // Sinon on retourne le format
                                resolve(new FormatFichier(format.id, format.nom, format.description,
                                    await typesFichierDao.get(format.id_type_fichier)));
                            }
                        },
                        (err: Error) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer les formats de fichier dans la base de donnée
     * 
     * @param parametres les paramètres de recherches
     * @returns une promesse contenant les formats de fichier
     */
    public async getAll(parametres: ParametresRechercheTypesFichier): Promise<FormatFichier[]> {
        return new Promise<FormatFichier[]>(
            (resolve: (value: FormatFichier[]) => void, reject: (reason: any) => void) => {
                let requete = `SELECT * FROM FORMATS_FICHIER ${Object.values(parametres).length > 0 ? 'WHERE ' : ''}`;

                // On créer le tableau des paramètres et la suite de la requête
                let params: any[] = [];
                for (let prop in parametres) {
                    requete += `${prop} ${typeof (parametres[prop]) == 'string' ? 'LIKE' : '='} $${params.length + 1}
                    ${params.length < Object.values(parametres).length - 1 ? 'AND ' : ''}`;

                    if (typeof (parametres[prop]) == 'string') {
                        params.push(`%${parametres[prop]}%`);
                    } else {
                        params.push(parametres[prop]);
                    }
                }

                // On se connecte à la base de donnée puis on exécute la requête
                this.session.executerRequete(requete, params).then(
                    async (res: QueryResult) => {
                        // On créer un tableau qui contiendra les formats de fichier
                        let formats: FormatFichier[] = []

                        // On crée les formats à partir des données récupérées
                        for (let i = 0; i < res.rows.length; i++) {
                            const format = res.rows[i];
                            formats.push(new FormatFichier(format.id, format.nom, format.description,
                                await typesFichierDao.get(format.id_type_fichier)));
                            
                        }
                        
                        // On retourne les formats
                        resolve(formats);
                    },
                    (err: Error) => reject(err));
            });
    }
}