import sanitizeHtml from "sanitize-html";
import { JSDOM } from "jsdom";

/**
 * Analyseur de code HTML
 * 
 * @author Developpix
 * @version 0.1
 */
export class HTMLAnalyseur {
    /**
     * Méthode permettant de sécuriser le code HTML (retirer/échapper le code indésirable )
     * @param contenuHtml le contenu HTML
     * @return {string}
     * @returns le code HTML sécurisé
     */
    public static securiser(contenuHtml: string): string {
        // On convertit le contenu en HTML DOM
        let dom: JSDOM = new JSDOM(contenuHtml);

        // On parcours les balises code jusqu'à la dernière
        let i = 0;
        while (i < dom.window.document.querySelectorAll('code').length) {
            // On récupère la balise code courante
            let element: HTMLElement = dom.window.document.querySelectorAll('code')[i];

            // On échappe les crochets des balises contenu dans les balises code
            element.innerHTML = element.innerHTML
                .replace(/\</g, '&lt;')
                .replace(/\>/g, '&gt;');

            // On incrémente l'index de la balise code
            i++;
        }

        // On retire les balises interdite
        return sanitizeHtml(dom.window.document.body.innerHTML, {
            allowedTags: sanitizeHtml.defaults.allowedTags.concat('cite'),
            allowedAttributes: sanitizeHtml.defaults.allowedAttributes,
            allowedSchemes: sanitizeHtml.defaults.allowedSchemes,
            allowedSchemesByTag: sanitizeHtml.defaults.allowedSchemesByTag
        });
    }
}