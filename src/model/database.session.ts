import { Pool, PoolClient, PoolConfig, QueryResult, Query } from 'pg';

/**
 * Session établie avec la base de donnée
 * 
 * @author Developpix
 * @version 0.1
 */
export default class DatabaseSession {
    /**
     * Instance de la session
     */
    private static instance: DatabaseSession;
    /**
     * Connexion avec la base de donnée
     */
    private connexion: Pool;

    /**
     * Constructeur
     */
    private constructor() {
        this.connexion = new Pool({
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT),
            database: process.env.DATABASE_DB,
            user: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD
        })
    }
    
    /**
     * Méthode permettant d'exécuter une requête SQL
     * 
     * @param requete la requête SQL
     * @param params les paramètres de la requête (optionnel)
     * 
     * @returns le résultat de l'exécution de la requête
     */
    public executerRequete(requete: string, params: any[] = []): Promise<QueryResult> {
        // On créer une promesse qui contiendra le résultat de la requête
        return new Promise<QueryResult>(
            (resolve: (value: QueryResult) => void, reject: (reason: any) => void) => {
                this.connexion.connect()
                    .then(
                        (client: PoolClient) => {
                            client.query(requete, params)
                                .then((res: QueryResult) => {
                                    // On ferme la connexion avec la base de donnée
                                    client.release();

                                    // On retourne le résultat
                                    resolve(res);
                                },
                                    (err: any) => {
                                        // On ferme la connexion avec la base de donnée
                                        client.release();

                                        // On remonte l'exception
                                        reject(err);
                                    });
                        },
                        (err: any) => reject(err));
            });
    }

    /**
     * Méthode permettant de récupérer l'instance de la session avec la base de donnée
     * 
     * @returns la session établie avec la base de donnée
     */
    public static getInstance(): DatabaseSession {
        // Si aucune session établie, on en établie une
        if (DatabaseSession.instance == null)
            DatabaseSession.instance = new DatabaseSession();

        // On retourne l'instance de la session
        return DatabaseSession.instance;
    }
}