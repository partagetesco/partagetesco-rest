/**
 * Les paramètres de recherches des fonctions
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheFonctions {
    /**
     * Id de la fonction
     */
    id?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string
}

/**
 * Les paramètres de recherches des utilisateurs
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheUtilisateurs {
    /**
     * L'id de l'utilisateur
     */
    id?: number,
    /**
     * Un mot clé recherché dans le pseudo
     */
    pseudo?: string,
    /**
     * Un mot clé recherché dans l'adresse mail
     */
    mail?: string,
    /**
     * Un mot clé recherché dans la biographie
     */
    biographie?: string,
    /**
     * L'id de la fonction associé à l'utilisateur
     */
    id_fonction?: number
}

/**
 * Les paramètres de recherches des types de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheTypesFichier {
    /**
     * Id du type de fichier
     */
    id?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string
}

/**
 * Les paramètres de recherches des formats de fichier
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheFormatsFichier {
    /**
     * Id du format de fichier
     */
    id?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * L'id du type de fichier associé
     */
    id_type_fichier?: number
}

/**
 * Les paramètres de recherches des licences
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheLicences {
    /**
     * Id du format de fichier
     */
    id?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la version
     */
    version?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * Un mot clé recherché dans l'URL
     */
    url?: string
}

/**
 * Les paramètres de recherches des fichiers
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheFichiers {
    /**
     * Id du fichier
     */
    id?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * L'id du format de fichier associé
     */
    id_format?: number,
    /**
     * L'id de la licence associée
     */
    id_licence?: number,
    /**
     * L'id de l'utilisateur associé
     */
    id_utilisateur?: number
}

/**
 * Les paramètres de recherches des catégories
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheCategories {
    /**
     * Id de la categorie
     */
    id?: number,
    /**
     * Un mot clé recherché dans le slug
     */
    slug?: string,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * L'id de la catégorie parente associée
     */
    id_parent?: number
}

/**
 * Les paramètres de recherches des cours
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheCours {
    /**
     * Id du cours
     */
    id?: number,
    /**
     * Un mot clé recherché dans le slug
     */
    slug?: string,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * L'id de la catégorie associée
     */
    id_categorie?: number,
    /**
     * L'id de l'icône associée
     */
    id_icone?: number
}

/**
 * Les paramètres de recherches des chapitres
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheChapitres {
    /**
     * Id du cours
     */
    id?: number,
    /**
     * Le numéro du chapitre recherché
     */
    numero?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans la description
     */
    description?: string,
    /**
     * L'id du cours associée
     */
    id_cours?: number
}

/**
 * Les paramètres de recherches des sections
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheSections {
    /**
     * Id de la section
     */
    id?: number,
    /**
     * Le numéro de la section recherché
     */
    numero?: number,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * Un mot clé recherché dans le contenu
     */
    contenu?: string,
    /**
     * L'id du chapitre associée
     */
    id_chapitre?: number
}

/**
 * Les paramètres de recherches des QCM
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheQCM {
    /**
     * Id du QCM
     */
    id?: number,
    /**
     * Un mot clé recherché dans le slug
     */
    slug?: string,
    /**
     * Un mot clé recherché dans le nom
     */
    nom?: string,
    /**
     * L'id du chapitre associée
     */
    id_chapitre?: number
}

/**
 * Les paramètres de recherches des questions
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheQuestions {
    /**
     * Id de la question
     */
    id?: number,
    /**
     * Un mot clé recherché dans la question
     */
    question?: string,
    /**
     * L'id du QCM associée
     */
    id_qcm?: number
}

/**
 * Les paramètres de recherches des réponses
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheReponses {
    /**
     * Id de la réponse
     */
    id?: number,
    /**
     * Un mot clé recherché dans la réponse
     */
    reponse?: string,
    /**
     * Un booléen indiquand si les réponses recherchés sont valides ou non
     */
    valide?: boolean,
    /**
     * L'id de la question associée
     */
    id_question?: number
}

/**
 * Les paramètres de recherches des exercices
 * 
 * @author Developpix
 * @version 0.1
 */
export interface ParametresRechercheExercicesPratique {
    /**
     * Id de l'exerccie
     */
    id?: number,
    /**
     * Un mot clé recherché dans l'énoncé
     */
    enonce?: string,
    /**
     * Un booléen indiquan si l'exercice se corrige automatiquement
     */
    correction_auto?: boolean,
    /**
     * L'id du chapitre associé
     */
    id_chapitre?: number
}