import { Request } from "express-serve-static-core";
import { Utilisateur } from "../bean/utilisateurs/utilisateur";

/**
 * Requête d'un utilisateur authentifié
 * 
 * @author Developpix
 * @version 0.1
 */
export interface RequeteAuthentifiee extends Request {
    utilisateur: Utilisateur
}